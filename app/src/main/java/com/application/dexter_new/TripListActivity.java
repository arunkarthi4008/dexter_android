package com.application.dexter_new;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class TripListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    String Username, userId, DesingId;
    SessionManager session;
    DrawerLayout drawer;
    private ProgressDialog pDialog;
    Dialog dialog;
    final Context context = this;
    ListView lv;
    SwipeRefreshLayout mSwipeRefreshLayout;
    BottomNavigationView bottomNavigationView;
    LinearLayout linlaHeaderProgress;


    TripListAdapter runSheetAdapter;

    String exceptionMsg=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_list_activity);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId);
        linlaHeaderProgress = findViewById(R.id.linlaHeaderProgress);
        new getTripList().execute();


        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Pilot");
        Switch aswitch = headerView.findViewById(R.id.aswitch);

        if (session.getState() == true) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        if (session.getState() == true) {
            aswitch.setChecked(true);
        }
        aswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
                if (ischecked == true) {
                    session.setState(true);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                } else {
                    session.setState(false);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
            }
        });

        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
        MenuItem view_orders = nav_Menu.findItem(R.id.view_orders);
        nav_dashboard.setVisible(false);
        view_orders.setVisible(false);
        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code


        lv = (ListView) findViewById(R.id.homeListView);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);



        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        new getTripList().execute();
//                        Intent mainIntent = new Intent(TripListActivity.this, TripListActivity.class);
//                        startActivity(mainIntent);
//                        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                        mSwipeRefreshLayout.setRefreshing(false);
                        runSheetAdapter.notifyDataSetChanged();

                    }
                }, 2000);
            }
        });

        //bottom Navigation
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);



    }
    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                    switch (item.getItemId()) {
                        case R.id.delivery:
                            Intent intObj1 = new Intent(TripListActivity.this, RunSheetListActivity.class);
                            startActivity(intObj1);
                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                            return true;
                        case R.id.orderlst:
                            Intent intObj2 = new Intent(TripListActivity.this, DashBoard.class);
                            startActivity(intObj2);
                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                            return true;
                        case R.id.line_haul:
                            Intent intObj3 = new Intent(TripListActivity.this, TripListActivity.class);
                            startActivity(intObj3);
                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                            return true;
                    }
                    return false;
                }
            };
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        System.out.println("Menu items show == >" + menuItem.getItemId());
        switch (menuItem.getItemId()) {
            case R.id.delivery:
                Intent intObj1 = new Intent(this, RunSheetListActivity.class);
                startActivity(intObj1);
                return true;
            case R.id.orderlst:
                Intent intObj3 = new Intent(this, DashBoard.class);
                startActivity(intObj3);
                return true;
            case R.id.logout:
                session.logoutUser();
                return true;
        }
        return false;
    }






    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }



    @SuppressLint("StaticFieldLeak")
    public class getTripList extends AsyncTask<String, String, String> {


        String tripDetails = null;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(TripListActivity.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            tripDetails = "";
            tripDetails = webService.getTripList(userId,DesingId);
            System.out.println("tripDetails=="+tripDetails);
            return tripDetails;
        }


        @Override
        protected void onPostExecute(String result) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            System.out.println("tripDetails=="+tripDetails);
            if (tripDetails != null) {
                ArrayList<TripListTO> runSheetList = new ArrayList<TripListTO>();

                try {
                    JSONArray array = new JSONArray(tripDetails);
                    JSONObject jsonObject = array.getJSONObject(0);
                    exceptionMsg = jsonObject.getString("expOccur");
                    if(exceptionMsg.equals("N")){

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObj = array.getJSONObject(i);
                        TripListTO runSheet = new TripListTO(
                                jsonObj.getString("trip_id"),
                                jsonObj.getString("trip_code"),
                                jsonObj.getString("substatus"),
                                jsonObj.getString("customerName"),
                                jsonObj.getString("Origin"),
                                jsonObj.getString("destination"),
                                jsonObj.getString("ewaybill_no"),
                                jsonObj.getString("trip_status_id"),
                                jsonObj.getString("last_km"),
                                jsonObj.getString("vehicle_origin_actual_reporting_date"),
                                jsonObj.getString("vehicle_origin_actual_reporting_time"),
                                jsonObj.getString("vehicle_actual_loading_date"),
                                jsonObj.getString("vehicle_actual_loading_time"),
                                jsonObj.getString("trip_actual_start_date"),
                                jsonObj.getString("trip_actual_start_time"),
                                jsonObj.getString("vehicle_destination_actual_reporting_date"),
                                jsonObj.getString("vehicle_destination_actual_reporting_time"),
                                jsonObj.getString("extra_expense_status"),
                                jsonObj.getString("vechile_actual_unloading_date"),
                                jsonObj.getString("vehicle_actual_unloading_time"),
                                jsonObj.getString("order_allocation")
                        );

                        runSheetList.add(runSheet);
//                        String ewaybillno = jsonObj.getString("ewaybillno");
//                        System.out.println("ewaybillno in trip activity=="+ewaybillno);
                    }
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), exceptionMsg, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                if(runSheetList.size()>0){
                    System.out.println("RunSheetlist Size ==> " + runSheetList.size() );
                runSheetAdapter = new TripListAdapter(TripListActivity.this, runSheetList);
                lv.setAdapter(runSheetAdapter);
                }else{
                    Toast.makeText(getApplicationContext(), "No Records found", Toast.LENGTH_LONG).show();
                }
            }else{

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }

        }


    }



    public void onBackPressed() {


        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setTitle("Leave application?");
//        dialog.setMessage("Are you sure you want to leave the application?");
        dialog.setContentView(R.layout.exit_alert_layout);
        dialog.setCancelable(false);
        Button yesButton = (Button) dialog.findViewById(R.id.yes);
        Button noButton = (Button) dialog.findViewById(R.id.no);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView message = (TextView) dialog.findViewById(R.id.message);
        title.setText("Leave application?");
        message.setText("Are you sure you want to leave the application?");


        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.logoutUser();
            }
        });
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dialog != null && dialog.isShowing()) {
            System.out.println("dialog is showing");
            dialog.dismiss();
        }
    }

}
