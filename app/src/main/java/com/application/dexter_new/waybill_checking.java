package com.application.dexter_new;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.HashMap;

public class waybill_checking extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    String Username, userId, DesingId,branchId;
    SessionManager session;
    DrawerLayout drawer;
    CardView paybill,Topay,Credit,Waybills;
    final Context context = this;
    ProgressBar pb;
    String Waybillmode;
    BottomNavigationView bottomNavigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waybill_checking);
        pb = findViewById(R.id.loading_spinner);
        paybill = findViewById(R.id.Paybill);
        Topay = findViewById(R.id.Topay);
        Credit = findViewById(R.id.Credit);
        Waybills = findViewById(R.id.Waybills);

        session = new SessionManager(getApplicationContext());


        //Bottom navigation code

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        //Bottom navigation code Ended


        //side navigation code Start
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Pilot");
        Switch aswitch = headerView.findViewById(R.id.aswitch);

        if (session.getState() == true) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        if (session.getState() == true) {
            aswitch.setChecked(true);
        }
        aswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
                if (ischecked == true) {
                    session.setState(true);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                } else {
                    session.setState(false);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
            }
        });

        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
        MenuItem view_orders = nav_Menu.findItem(R.id.view_orders);
        nav_dashboard.setVisible(false);
        view_orders.setVisible(false);
        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code

        session.checkLogin();
        if (session.isLoggedIn()) {
            System.out.println("yes ur loged in");
            HashMap<String, String> user = session.getUserDetails();
            Username = user.get(SessionManager.KEY_NAME);
            userId = user.get(SessionManager.KEY_USERID);
            DesingId = user.get(SessionManager.KEY_DESIG);
            branchId = user.get(SessionManager.KEY_BRANCH);
            System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId + " branchId == " + branchId);

            paybill.setOnClickListener(v -> {
                Intent intObj = new Intent(this, PaidWayBill.class);
                Waybillmode = "1";
                intObj.putExtra("Waybillmode", Waybillmode);
                intObj.putExtra("branchId", branchId);
                intObj.putExtra("userId",userId);
                startActivity(intObj);
            });
            Topay.setOnClickListener(v -> {
                Intent intObj = new Intent(this, waybill_webview_load.class);
                Waybillmode = "2";
                intObj.putExtra("Waybillmode", Waybillmode);
                intObj.putExtra("branchId", branchId);
                intObj.putExtra("userId",userId);
                startActivity(intObj);

            });
            Credit.setOnClickListener(v -> {
                Intent intObj = new Intent(this, waybill_webview_load.class);
                Waybillmode = "3";
                intObj.putExtra("Waybillmode", Waybillmode);
                intObj.putExtra("branchId", branchId);
                intObj.putExtra("userId",userId);
                startActivity(intObj);

            });

            Waybills.setOnClickListener(v -> {
                Intent intObj = new Intent(this, waybill_webview_load.class);
                Waybillmode = "4";
                intObj.putExtra("Waybillmode", Waybillmode);
                intObj.putExtra("branchId", branchId);
                intObj.putExtra("userId",userId);
                startActivity(intObj);

            });

        } else {
            session.logoutUser();
        }


    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.delivery:
                            Intent intObj1 = new Intent(waybill_checking.this, RunSheetListActivity.class);
                            startActivity(intObj1);
                            return true;
                        case R.id.orderlst:
                            Intent intObj2 = new Intent(waybill_checking.this, DashBoard.class);
                            startActivity(intObj2);
                            return true;
                    }
                    return false;
                }
            };

    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }
    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this,DashBoard.class);
        startActivity(intent);


    }
    //navigation
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        switch (menuItem.getItemId()) {
            case R.id.orderlst:
                Intent intObj1 = new Intent(this, DashBoard.class);
                startActivity(intObj1);
                return true;
            case R.id.delivery:
                Intent intObj2 = new Intent(this, RunSheetListActivity.class);
                startActivity(intObj2);
                return true;
            case R.id.logout:
                session.logoutUser();
        }
        return false;
    }
}