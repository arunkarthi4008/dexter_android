package com.application.dexter_new;

public class RunSheetTO {

    String runsheetId = null;
    String runSheetNumber = null;
    String driverPhone = null;

    public RunSheetTO(String runsheetId, String runSheetNumber, String driverPhone) {
        this.runsheetId = runsheetId;
        this.runSheetNumber = runSheetNumber;
        this.driverPhone = driverPhone;
    }


    public String getRunsheetId() {
        return runsheetId;
    }

    public void setRunsheetId(String runsheetId) {
        this.runsheetId = runsheetId;
    }

    public String getRunSheetNumber() {
        return runSheetNumber;
    }

    public void setRunSheetNumber(String runSheetNumber) {
        this.runSheetNumber = runSheetNumber;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }
}
