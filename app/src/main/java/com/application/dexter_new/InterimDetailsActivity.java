package com.application.dexter_new;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class InterimDetailsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    String Username, userId, DesingId;
    SessionManager session;
    DrawerLayout drawer;
    private ProgressDialog pDialog;
    Dialog dialog;
    final Context context = this;
    ListView lv;
    SwipeRefreshLayout mSwipeRefreshLayout;

    InterimTripListAdapter runSheetAdapter;
    String exceptionMsg = null;
    String tripId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username in upload doc==" + Username + " userId in upload doc==" + userId + " DesigId in upload doc==" + DesingId);

        Intent intent = getIntent();
        tripId = intent.getStringExtra("tripId");
        new getInteriemDetails().execute();
        setContentView(R.layout.interiem_list_activity);


        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Pilot");
        Switch aswitch = headerView.findViewById(R.id.aswitch);

        if (session.getState() == true) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        if (session.getState() == true) {
            aswitch.setChecked(true);
        }
        aswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
                if (ischecked == true) {
                    session.setState(true);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                } else {
                    session.setState(false);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
            }
        });

        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
        MenuItem view_orders = nav_Menu.findItem(R.id.view_orders);
        nav_dashboard.setVisible(false);
        view_orders.setVisible(false);
        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code

        lv = (ListView) findViewById(R.id.homeListView);
    }

    public void onBackPressed() {

        Intent mainIntent = new Intent(context, TripListActivity.class);
        context.startActivity(mainIntent);
//        dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
////        dialog.setTitle("Leave application?");
////        dialog.setMessage("Are you sure you want to leave the application?");
//        dialog.setContentView(R.layout.custom_alert_dialouge);
//        dialog.setCancelable(false);
//        Button yesButton = (Button) dialog.findViewById(R.id.yes);
//        Button noButton = (Button) dialog.findViewById(R.id.no);
//        TextView title = (TextView) dialog.findViewById(R.id.title);
//        TextView message = (TextView) dialog.findViewById(R.id.message);
//        title.setText("Alert?");
//        message.setText("Are you sure you want to Go Back?");
//
//
//        yesButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent mainIntent = new Intent(context, TripListActivity.class);
//                context.startActivity(mainIntent);
//            }
//        });
//        noButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            session.logoutUser();
        }
//        if (id == R.id.view_orders) {
//            if (DesingId.equals("1042")) {
//                Intent mainIntent = new Intent(FuelFilling.this, MainActivity.class);
//                startActivity(mainIntent);
//            }
////            if (DesingId.equals("1059")) {
////
////            }
//        }
//        if (id == R.id.nav_dashboard) {
//            Intent mainIntent = new Intent(FuelFilling.this, DeliveryRunSheetListActivity.class);
//            startActivity(mainIntent);
//        }
        return true;
    }

    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }

    @SuppressLint("StaticFieldLeak")
    public class getInteriemDetails extends AsyncTask<String, String, String> {
        String tripDetails = null;

        @Override
        protected void onPreExecute() {
//            pDialog = new ProgressDialog(InterimDetailsActivity.this);
//            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
//            pDialog.setCancelable(false);
//            pDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {
//            tripDetails = webService.getInterimTripList(tripId);
            System.out.println("tripDetails==" + tripDetails);
            return tripDetails;
        }

        @Override
        protected void onPostExecute(String result) {

//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
            System.out.println("tripDetails==" + tripDetails);
            if (tripDetails != null) {
                ArrayList<InterimTripListTO> runSheetList = new ArrayList<InterimTripListTO>();
                try {
                    JSONArray array = new JSONArray(tripDetails);
                    JSONObject jsonObject = array.getJSONObject(0);
                    exceptionMsg = jsonObject.getString("expOccur");
                    if (exceptionMsg.equals("N")) {

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObj = array.getJSONObject(i);
                            InterimTripListTO runSheet = new InterimTripListTO(
                                    jsonObj.getString("pointId"),
                                    jsonObj.getString("pointName"),
                                    jsonObj.getString("inDate"),
                                    jsonObj.getString("inTime"),
                                    jsonObj.getString("outDate"),
                                    jsonObj.getString("outTime"),
                                    jsonObj.getString("fileName"),
                                    jsonObj.getString("completedFlag"),
                                    jsonObj.getString("routeOrder")
                            );
                            runSheetList.add(runSheet);
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), exceptionMsg, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                runSheetAdapter = new InterimTripListAdapter(InterimDetailsActivity.this, runSheetList, tripId);
                lv.setAdapter(runSheetAdapter);


            } else {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });
            }

        }

    }




}
