package com.application.dexter_new;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class WaybillListAdapter extends BaseAdapter {

    Context context;
    ArrayList<WaybillListTO> runSheetList;
    ArrayList<WaybillListTO> runSheetListDisplayed;
    TextView waybill_number, consignee_name, consignee_address, consignee_num;
    Button startDelivery, un_deliver;
    LinearLayout map;
    int itPosition = 0;
    String waybillId,consigneeName,consignorName,noofArticle,totalWeight;

    public WaybillListAdapter(Context context, ArrayList<WaybillListTO> runSheetList) {
        this.context = context;
        this.runSheetList = runSheetList;
        this.runSheetListDisplayed = runSheetList;
    }

    @Override
    public int getCount() {
        return runSheetListDisplayed.size();
    }

    @Override
    public Object getItem(int i) {
        return runSheetListDisplayed.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        convertView = null;


        convertView = mInflater.inflate(R.layout.waybill_list_adapter, null);

        waybill_number = (TextView) convertView.findViewById(R.id.order_id);
        consignee_name = (TextView) convertView.findViewById(R.id.consignee_name);
        consignee_address = (TextView) convertView.findViewById(R.id.consignee_add);
        consignee_num = (TextView) convertView.findViewById(R.id.consignee_num);
        startDelivery = (Button) convertView.findViewById(R.id.buttonView);

        startDelivery.setTag(position);

        WaybillListTO runSheet = runSheetListDisplayed.get(position);
        waybill_number.setText(runSheet.getWaybillNum());
        consignee_name.setText(runSheet.getConsigneeName());
        consignee_address.setText(runSheet.getConsigneeAddress());
        consignee_num.setText(runSheet.getConsigneephone());

        startDelivery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int pos = (Integer) v.getTag();
                WaybillListTO OrderClicked = runSheetListDisplayed.get(pos);
                itPosition = pos;
                waybillId = OrderClicked.getWaybillId();
                consignorName = OrderClicked.getConsignorName();
                consigneeName = OrderClicked.getConsigneeName();
                totalWeight = OrderClicked.getTotalWeight();
                noofArticle = OrderClicked.getNoofArticles();

                Intent intent = new Intent(context, UploadPod.class);
                intent.putExtra("waybillId", waybillId);
                intent.putExtra("consignorName", consignorName);
                intent.putExtra("consigneeName", consigneeName);
                intent.putExtra("totalWeight", totalWeight);
                intent.putExtra("noofArticle", noofArticle);
                context.startActivity(intent);

//                    Intent intent = new Intent(context, DeliverOrder.class);
//                    intent.putExtra("order_id", orderId);
//                    intent.putExtra("customer_name", customerName);
//                    intent.putExtra("address_1", address1);
//                    intent.putExtra("address_2", address2);
//                    intent.putExtra("landmark", landMark);
//                    intent.putExtra("district", District);
//                    intent.putExtra("state", State);
//                    intent.putExtra("product", product);
//                    intent.putExtra("un_num_1", unNumber1);
//                    intent.putExtra("un_num_2", unNumber2);
//                    intent.putExtra("loan_id", loanId);
//                    intent.putExtra("vendor_id", vendorId);
//                    intent.putExtra("resend_status", resend_status);


            }
        });
        return convertView;
    }


}
