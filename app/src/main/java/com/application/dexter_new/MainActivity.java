package com.application.dexter_new;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    String Username, userId, DesingId,branchId;
    SessionManager session;
    final Context context = this;
    ProgressBar pb;
    boolean isPermitted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkRunTimePermission();
        pb = findViewById(R.id.loading_spinner);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        if (session.isLoggedIn()) {
            System.out.println("yes ur loged in");
            HashMap<String, String> user = session.getUserDetails();
            Username = user.get(SessionManager.KEY_NAME);
            userId = user.get(SessionManager.KEY_USERID);
            DesingId = user.get(SessionManager.KEY_DESIG);
            branchId = user.get(SessionManager.KEY_BRANCH);
            System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId + " BarnchId == " +branchId);
            if (DesingId.equals("1033")) {//delivery boy
                Intent mainIntent = new Intent(MainActivity.this, DashBoard.class);
                startActivity(mainIntent);
            } else {
                Toast.makeText(getApplicationContext(), "UnAuthorized User", Toast.LENGTH_LONG).show();
                Intent mainIntent = new Intent(MainActivity.this, CheckLoginActivity.class);
                startActivity(mainIntent);
            }
        } else {
            session.logoutUser();
        }


    }


    private void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
        } else {
            // if already permition granted
            // PUT YOUR ACTION (Like Open cemara etc..)
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean openActivityOnce = true;
        boolean openDialogOnce = true;
        if (requestCode == 11111) {
            for (int i = 0; i < grantResults.length; i++) {
                String permission = permissions[i];
                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        //execute when 'never Ask Again' tick and permission dialog not show
                    } else {
                        if (openDialogOnce) {
                            alertView();
                        }
                    }
                }
            }
        }
    }

    private void alertView() {
        new AlertDialog.Builder(this)
                .setTitle("Permission Denied")
                .setMessage("Without those permission the app is unable to save your profile.Are you sure you want to deny this permission?")
                .setPositiveButton("RE-TRY", (dialog, which) -> checkRunTimePermission())
                .setNegativeButton("I'M SURE", null)
                .show();
    }


}