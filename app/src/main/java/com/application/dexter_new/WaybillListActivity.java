package com.application.dexter_new;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class WaybillListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    String Username, userId, DesingId;
    SessionManager session;
    DrawerLayout drawer;
    Dialog dialog;
    final Context context = this;
    String jsonStr;
    String exceptionMsg,runsheetId;
    ListView lv;
    WaybillListAdapter adapter;
    DatabaseHelper databaseHelper;
    int connectionStatus = 0;
    private BroadcastReceiver mNetworkReceiver;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println(" user Details in runsheetActivity== username==" + Username + " userId==" + userId + " DesigId==" + DesingId);
        setContentView(R.layout.waybill_list_activity);

        Intent intent = getIntent();
        runsheetId = intent.getStringExtra("runsheetId");

        getWaybillList();
        databaseHelper = new DatabaseHelper(getApplicationContext());
        mNetworkReceiver = new NetworkStateChecker();


        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Pilot");
        Switch aswitch = headerView.findViewById(R.id.aswitch);

        if (session.getState() == true) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        if (session.getState() == true) {
            aswitch.setChecked(true);
        }
        aswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
                if (ischecked == true) {
                    session.setState(true);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                } else {
                    session.setState(false);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
            }
        });

        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
        MenuItem view_orders = nav_Menu.findItem(R.id.view_orders);
        nav_dashboard.setVisible(false);
        view_orders.setVisible(false);
        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code


        lv = findViewById(R.id.listview);

    }

    //navigation
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            session.logoutUser();
        }

        return true;
    }

    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }

    //navigation


    public void getWaybillList() {

        pDialog = new ProgressDialog(WaybillListActivity.this);
        pDialog.setMessage("Connecting Server... \n Please Wait!!!");
        pDialog.setCancelable(false);
        pDialog.show();

        String obj = null;

        new BackgroundTask(WaybillListActivity.this) {
            @Override
            public void doInBackground() {
                //put you background code
                //same like doingBackground
                //Background Thread
                jsonStr = webService.getWaybillList(userId,runsheetId);
                System.out.println("json_string_output====" + jsonStr);

            }

            @Override
            public void onPostExecute() {
//                if (pDialog.isShowing()) {
//                    pDialog.dismiss();
//                }

                ArrayList<WaybillListTO> deliveryOrderList = new ArrayList<WaybillListTO>();
                if (jsonStr != null) {
                    try {
                        JSONArray jsonArray = new JSONArray(jsonStr);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int n = 0; n < jsonArray.length(); n++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(n);
                                WaybillListTO runSheet = new WaybillListTO(
                                        jsonObject.getString("waybillId"),
                                        jsonObject.getString("waybillNumber"),
                                        jsonObject.getString("customerName"),
                                        jsonObject.getString("consignorName"),
                                        jsonObject.getString("consignorAddress"),
                                        jsonObject.getString("consignorPhone"),
                                        jsonObject.getString("consigneeName"),
                                        jsonObject.getString("consigneeAddress"),
                                        jsonObject.getString("consigneePhone"),
                                        jsonObject.getString("noofArticle"),
                                        jsonObject.getString("totalWeight")
                                );
                                deliveryOrderList.add(runSheet);
                                System.out.println("Runsheet Output String list ====> "+runSheet);
                            }
                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "No Waybill Available", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                        pDialog.hide();
                    } catch (final JSONException e) {
                        System.out.println("JSONException=========" + e);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
//                    databaseHelper = new DatabaseHelper(getApplicationContext());
//                    databaseHelper.openDB();
//                    databaseHelper.clearRunSheetList();
//                    databaseHelper.insertRunSheetLists(deliveryOrderList);
//                    databaseHelper.closeDB();

                    try {
                        if (deliveryOrderList.size() > 0) {
                            adapter = new WaybillListAdapter(WaybillListActivity.this, deliveryOrderList);
                            lv.setAdapter(adapter);
                        }
                    } catch (Exception e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        }.execute();
    }
}
