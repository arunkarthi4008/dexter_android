package com.application.dexter_new;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.transferwise.sequencelayout.SequenceStep;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class TripTrackingActivity extends AppCompatActivity {

    String Username, userId, DesingId;
    TextView Customername,Destination;
    SessionManager session;
    private ProgressDialog pDialog;
    SequenceStep step1, step2, step3, step4, step5, step6;
    Button view,lrprint;
    String tripId, sub_status, exceptionMsg, eWayBillNo,odometerReading,subStatusId,Complete_flag;
    String reportingDate, reportingTime, loadingTime, loadingDate, tripStartDate, tripEndDate;
    String trip_id,trip_code,substatus,customerName,Origin,destination,ewaybill_no,trip_status_id,last_km,vehicle_origin_actual_reporting_date,
            vehicle_origin_actual_reporting_time,vehicle_actual_loading_date,vehicle_actual_loading_time,trip_actual_start_date,
            trip_actual_start_time,vehicle_destination_actual_reporting_date,vehicle_destination_actual_reporting_time,
            extra_expense_status,vechile_actual_unloading_date,vehicle_actual_unloading_time,order_allocation,complete_flag = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_tracking);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username==" + Username + " userId==" + userId + " DesigId==" + DesingId);

        step1 = (SequenceStep) findViewById(R.id.step1);
        step2 = (SequenceStep) findViewById(R.id.step2);
        step3 = (SequenceStep) findViewById(R.id.step3);
        step4 = (SequenceStep) findViewById(R.id.step4);
        step5 = (SequenceStep) findViewById(R.id.step5);
        step6 = (SequenceStep) findViewById(R.id.step6);

        Customername = findViewById(R.id.customername);
        Destination = findViewById(R.id.destination1);


        
        view = findViewById(R.id.button1);
        lrprint = findViewById(R.id.button2);


        Intent intent = getIntent();
        trip_id = intent.getStringExtra("trip_id");
        trip_code = intent.getStringExtra("trip_code");
        substatus = intent.getStringExtra("substatus");
        customerName = intent.getStringExtra("customerName");
        Origin = intent.getStringExtra("Origin");
        destination = intent.getStringExtra("destination");
        ewaybill_no = intent.getStringExtra("ewaybill_no");
        trip_status_id = intent.getStringExtra("trip_status_id");
        last_km = intent.getStringExtra("last_km");
        vehicle_origin_actual_reporting_date = intent.getStringExtra("vehicle_origin_actual_reporting_date");
        vehicle_origin_actual_reporting_time = intent.getStringExtra("vehicle_origin_actual_reporting_time");
        vehicle_actual_loading_date = intent.getStringExtra("vehicle_actual_loading_date");
        vehicle_actual_loading_time = intent.getStringExtra("vehicle_actual_loading_time");
        trip_actual_start_date = intent.getStringExtra("trip_actual_start_date");
        trip_actual_start_time = intent.getStringExtra("trip_actual_start_time");
        vehicle_destination_actual_reporting_date = intent.getStringExtra("vehicle_destination_actual_reporting_date");
        vehicle_destination_actual_reporting_time = intent.getStringExtra("vehicle_destination_actual_reporting_time");
        extra_expense_status = intent.getStringExtra("extra_expense_status");
        vechile_actual_unloading_date = intent.getStringExtra("vechile_actual_unloading_date");
        vehicle_actual_unloading_time = intent.getStringExtra("vehicle_actual_unloading_time");
        order_allocation = intent.getStringExtra("order_allocation");
//        complete_flag = intent.getStringExtra("complete_flag");

        Customername.setText(customerName);
        Destination.setText(Origin + "  to  " + destination);

        step1.setAnchor(Origin);
        step1.setAnchorTextAppearance(R.style.bold);
        step6.setAnchorTextAppearance(R.style.bold);
        step6.setAnchor(destination);
        step1.setTitle("Reporting Details");
        step2.setTitle("Loading Details");
        step3.setTitle("Document Details");
        step4.setTitle("Gate Pass Details");
        step5.setTitle("Trip Start");
        step6.setTitle("Reached Destination");


        if (vehicle_origin_actual_reporting_date.equals("null") && vehicle_origin_actual_reporting_time.equals("null")) {
            step1.setSubtitle("Update");
        } else {
            step1.setSubtitle(vehicle_origin_actual_reporting_date + "  " + vehicle_origin_actual_reporting_time);
        }

        if (vehicle_actual_loading_date.equals("null") && vehicle_actual_loading_time.equals("null")) {
            step2.setSubtitle("Update");
        } else {
            step2.setSubtitle(vehicle_actual_loading_date + "  " + vehicle_actual_loading_time);
        }


        step3.setSubtitle("Upload");

        step4.setSubtitle("Generate");
        if (trip_actual_start_date.equals("null") && trip_actual_start_time.equals("null")) {
            step5.setSubtitle("Update");
        } else {
            step5.setSubtitle(trip_actual_start_date + "  " + trip_actual_start_time);
        }

        if (vehicle_destination_actual_reporting_date.equals("null") && vehicle_destination_actual_reporting_time.equals("null")) {
            step6.setSubtitle("update");
        } else {
            step6.setSubtitle(vehicle_destination_actual_reporting_date + "  " + vehicle_destination_actual_reporting_time);
        }

//        step7.setSubtitle("Upload");


        if (substatus.equals("0")) {
            step1.setActive(true);
            step1.setTitleTextAppearance(R.style.boldblue);
            step1.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Update");
        }
        if (substatus.equals("1")) {
            step2.setActive(true);
            step2.setTitleTextAppearance(R.style.boldblue);
            step2.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Update");
        }
        if (substatus.equals("2")) {
            step3.setActive(true);
            step3.setTitleTextAppearance(R.style.boldblue);
            step3.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Upload");
        }
        //own ==3
        //hire ==4
//        if(own){
        if (order_allocation.equals("1") && substatus.equals("3")) {
            step4.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setSubtitle("In Progress...");
            step4.setTitleTextAppearance(R.style.boldblue);
            step4.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Check");
//            view.setVisibility(View.GONE);

        }
        if (order_allocation.equals("1") && substatus.equals("4") && extra_expense_status.equals("0")) {
            step4.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Expense Details");
            step4.setSubtitle("InProgress");
            step4.setTitleTextAppearance(R.style.boldblue);
            step4.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Check");
//            view.setVisibility(View.GONE);

        }
        if (order_allocation.equals("1") && substatus.equals("4") && extra_expense_status.equals("1")) {
            step5.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Expense Details");
            step4.setSubtitle("Updated");
            step5.setTitleTextAppearance(R.style.boldblue);
            step5.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Start Trip");
//            view.setVisibility(View.GONE);

        }

        if (order_allocation.equals("1") && substatus.equals("5")  &&
                vehicle_destination_actual_reporting_date.equals("null")) {
            step6.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Expense Details");
            step4.setSubtitle("Updated");
            step6.setTitleTextAppearance(R.style.boldblue);
            step6.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Report Destination Reached");
//            view.setVisibility(View.GONE);

        }
        if (order_allocation.equals("1") && substatus.equals("5")  &&
                vehicle_actual_loading_date.equals("null")) {
            step6.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Expense Details");
            step4.setSubtitle("Updated");
            step6.setTitleTextAppearance(R.style.boldblue);
            step6.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Update Unloading Details");
//            view.setVisibility(View.GONE);

        }

        if (order_allocation.equals("1") && substatus.equals("5")  &&
                ! vehicle_actual_loading_date.equals("null")) {
            step6.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Expense Details");
            step4.setSubtitle("Updated");
//            step6.setTitleTextAppearance(R.style.boldblue);
//            step6.setSubtitleTextAppearance(R.style.boldblue);
            view.setVisibility(View.GONE);

        }
//            else if (order_allocation.equals("1") && substatus.equals("5")  &&
//                    getPod_file().equals("null")) {
//                step6.setActive(true);
//                step3.setSubtitle("Uploaded");
//                step4.setTitle("Expense Details");
//                step4.setSubtitle("Updated");
//                step6.setTitleTextAppearance(R.style.boldblue);
//                step6.setSubtitleTextAppearance(R.style.boldblue);
//                view.setText("Update POD");
////            view.setVisibility(View.GONE);
//
//            }
        if (order_allocation.equals("1") && substatus.equals("5") && !complete_flag.equals("1")) {
            step5.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Expense Details");
            step4.setSubtitle("Updated");
            step5.setTitleTextAppearance(R.style.boldblue);
            step5.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Update Interim Details");
//            view.setVisibility(View.GONE);

        }


//        }
//        if(hire){
        if (order_allocation.equals("2") && substatus.equals("3")) {
            step4.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Gate Pass Details");
            step4.setSubtitle("In progress");
            step4.setTitleTextAppearance(R.style.boldblue);
            step4.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Check");
//            view.setVisibility(View.GONE);

        }
        if (order_allocation.equals("2") && substatus.equals("4")) {
            step5.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Gate Pass Details");
            step4.setSubtitle("Updated");
            step5.setTitleTextAppearance(R.style.boldblue);
            step5.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Start Trip");
//            view.setVisibility(View.GONE);

        }

        if (order_allocation.equals("2") && substatus.equals("5")  &&
                vehicle_destination_actual_reporting_date.equals("null")) {
            step6.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Expense Details");
            step4.setSubtitle("Updated");
            step6.setTitleTextAppearance(R.style.boldblue);
            step6.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Report Destination Reached");
//            view.setVisibility(View.GONE);

        }
        if (order_allocation.equals("2") && substatus.equals("5")  &&
                vechile_actual_unloading_date.equals("null")) {
            step6.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Expense Details");
            step4.setSubtitle("Updated");
            step6.setTitleTextAppearance(R.style.boldblue);
            step6.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Update Unloading Details");
//            view.setVisibility(View.GONE);

        }

        if (order_allocation.equals("2") && substatus.equals("5")  &&
                !vechile_actual_unloading_date.equals("null")) {
            step6.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Expense Details");
            step4.setSubtitle("Updated");
//            step6.setTitleTextAppearance(R.style.boldblue);
//            step6.setSubtitleTextAppearance(R.style.boldblue);

            view.setVisibility(View.GONE);

        }

        if (order_allocation.equals("2") && substatus.equals("5") &&  !complete_flag.equals("1")) {
            step5.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Gate Pass Details");
            step4.setSubtitle("Updated");
            step5.setTitleTextAppearance(R.style.boldblue);
            step5.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Update Interim Details");
//            view.setVisibility(View.GONE);

        }


//        }

        if (substatus.equals("200")) {
            step5.setActive(true);
            step3.setSubtitle("Uploaded");
            step4.setTitle("Lock Status");
            step4.setSubtitle("Locked");
            step5.setTitleTextAppearance(R.style.boldblue);
            step5.setSubtitleTextAppearance(R.style.boldblue);
            view.setText("Start Trip");
            lrprint.setVisibility(View.GONE);
        }


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int pos = (Integer) v.getTag();
//                TripListTO OrderClicked = runSheetList.get(pos);
//                itPosition = pos;

                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

                System.out.println("currentTime===" + currentTime);
                System.out.println("currentDate===" + currentDate);

                tripId = trip_id;
                sub_status = substatus;
                String expense_status = extra_expense_status;

                if (sub_status.equals("0")) {
                    subStatusId = "1";
                    reportingDate = currentDate;
                    reportingTime = currentTime;
                    new TripTrackingActivity.updateReportingDetails().execute();
                }
                if (sub_status.equals("1")) {
                    subStatusId = "2";
                    reportingDate = currentDate;
                    reportingTime = currentTime;
                    new TripTrackingActivity.updateLoadingDetails().execute();
                }
                if (sub_status.equals("2")) {
                    //uploadDocument Screen
                    Intent intent = new Intent(TripTrackingActivity.this, UploadDocument.class);
                    intent.putExtra("tripId", tripId);
                    startActivity(intent);
                }
                if (sub_status.equals("3")) {
                    Intent mainIntent = new Intent(TripTrackingActivity.this, TripListActivity.class);
                    TripTrackingActivity.this.startActivity(mainIntent);
                }
                if (sub_status.equals("3") && order_allocation.equals("2")) {
                    Intent mainIntent = new Intent(TripTrackingActivity.this, TripListActivity.class);
                    TripTrackingActivity.this.startActivity(mainIntent);
                }

                if (order_allocation.equals("1") && sub_status.equals("4") && expense_status.equals("0")) {
                    Intent mainIntent = new Intent(TripTrackingActivity.this, TripListActivity.class);
                    TripTrackingActivity.this.startActivity(mainIntent);
                }
                if (order_allocation.equals("1") && sub_status.equals("4") && expense_status.equals("1") ) {
//                    final int x = (int) getItemId(position);
                    final Dialog dialog = new Dialog(TripTrackingActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.odometer_capture_dialouge);
                    dialog.setCancelable(true);
                    EditText km = (EditText) dialog.findViewById(R.id.TripStartKm);
                    TextView endKm = (TextView) dialog.findViewById(R.id.endKm);
                    endKm.setText(last_km);
                    Button dialogButton = dialog.findViewById(R.id.tripStart);
                    dialogButton.setText("Start Trip");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String lastKm =last_km + "";
                            if (km.getText().length() != 0 && km.getText().toString() != "") {
                                String odometerReadings = km.getText().toString();
                                float i = Float.parseFloat(km.getText().toString()+".0");
                                float j = Float.parseFloat(last_km);
                                float k = j + 5;
                                System.out.println("Kms==>" + i + j + k);
                                if (i > j && i <= k) {
                                    subStatusId = "5";
                                    reportingDate = currentDate;
                                    reportingTime = currentTime;
                                    odometerReading = odometerReadings;
                                    new TripTrackingActivity.updateGateoutDetails().execute();
                                    dialog.dismiss();
                                } else {
                                    Toast.makeText(TripTrackingActivity.this, "Please Enter Start KM higher than last End Km", Toast.LENGTH_LONG).show();
                                }

                            } else {
                                Toast.makeText(TripTrackingActivity.this, "Please Enter Odometer Reading", Toast.LENGTH_LONG).show();
                                dialog.show();
                            }

                        }
                    });
                    dialog.show();
                }




                if (sub_status.equals("4") &&  order_allocation.equals("2")) {

//                    final int x = (int) getItemId(position);
                    final Dialog dialog = new Dialog(TripTrackingActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.odometer_capture_dialouge);
                    dialog.setCancelable(true);
                    EditText km = (EditText) dialog.findViewById(R.id.TripStartKm);
                    TextView endKm = (TextView) dialog.findViewById(R.id.endKm);
                    endKm.setText(last_km);
                    TextView heading = dialog.findViewById(R.id.heading);
                    heading.setText("Enter Trip Start Km");
                    Button dialogButton = (Button) dialog.findViewById(R.id.tripStart);
                    dialogButton.setText("Start Trip");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String lastKm =last_km;
                            if (km.getText().length() != 0 && km.getText().toString() != "") {
                                String odometerReadings = km.getText().toString();
                                int i = Integer.parseInt(km.getText().toString());
                                int j = Integer.parseInt(lastKm);
                                int k = j + 5;
                                if (i > j && i <= k) {
                                    subStatusId = "5";
                                    reportingDate = currentDate;
                                    reportingTime = currentTime;
                                    odometerReading = odometerReadings;
                                    new TripTrackingActivity.updateGateoutDetails().execute();
                                    dialog.dismiss();
                                } else {
                                    Toast.makeText(TripTrackingActivity.this, "Please Enter Start KM higher than last End Km", Toast.LENGTH_LONG).show();
                                }

                            } else {
                                Toast.makeText(TripTrackingActivity.this, "Please Enter Odometer Reading", Toast.LENGTH_LONG).show();
                                dialog.show();
                            }

                        }
                    });
                    dialog.show();
                }




                if (sub_status.equals("200")) {
//                    final int x = (int) getItemId(position);
                    final Dialog dialog = new Dialog(TripTrackingActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.odometer_capture_dialouge);
                    dialog.setCancelable(true);
                    EditText km = (EditText) dialog.findViewById(R.id.TripStartKm);
                    TextView endKm = (TextView) dialog.findViewById(R.id.endKm);
                    endKm.setText(last_km);
                    TextView heading = dialog.findViewById(R.id.heading);
                    heading.setText("Enter Trip Start Km");
                    Button dialogButton = (Button) dialog.findViewById(R.id.tripStart);
                    dialogButton.setText("Start Trip");
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String lastKm =last_km;
                            if (km.getText().length() != 0 && km.getText().toString() != "") {
                                String odometerReadings = km.getText().toString();
                                int i = Integer.parseInt(km.getText().toString());
                                int j = Integer.parseInt(lastKm);
                                int k = j + 5;
                                if (i > j && i <= k) {
                                    subStatusId = "5";
                                    reportingDate = currentDate;
                                    reportingTime = currentTime;
                                    odometerReading = odometerReadings;
                                    new TripTrackingActivity.updateGateoutDetails().execute();
                                    dialog.dismiss();
                                } else {
                                    Toast.makeText(TripTrackingActivity.this, "Please Enter Start KM higher than last End Km", Toast.LENGTH_LONG).show();
                                }

                            } else {
                                Toast.makeText(TripTrackingActivity.this, "Please Enter Odometer Reading", Toast.LENGTH_LONG).show();
                                dialog.show();
                            }

                        }
                    });
                    dialog.show();
                }

                if (order_allocation.equals("1") && sub_status.equals("5") && !complete_flag.equals("1")) {
                    Intent intent = new Intent(TripTrackingActivity.this, InterimDetailsActivity.class);
                    intent.putExtra("tripId", tripId);
                    TripTrackingActivity.this.startActivity(intent);
                }
                if (order_allocation.equals("2") && sub_status.equals("5") &&  !complete_flag.equals("1")) {
                    Intent intent = new Intent(TripTrackingActivity.this, InterimDetailsActivity.class);
                    intent.putExtra("tripId", tripId);
                    TripTrackingActivity.this.startActivity(intent);
                }

                if (order_allocation.equals("2") && sub_status.equals("5")
                        && vehicle_destination_actual_reporting_date.equals("null")) {

                    reportingDate = currentDate;
                    reportingTime = currentTime;
                    new TripTrackingActivity.updateDestinationReachedDetails().execute();

                }
                if (order_allocation.equals("2") && sub_status.equals("5")
                        && vechile_actual_unloading_date.equals("null")) {
                    reportingDate = currentDate;
                    reportingTime = currentTime;
                    new TripTrackingActivity.updateUnloadingDetails().execute();
                }

                if (order_allocation.equals("1") && sub_status.equals("5")
                        && vehicle_destination_actual_reporting_date.equals("null")) {
                    reportingDate = currentDate;
                    reportingTime = currentTime;
                    new TripTrackingActivity.updateDestinationReachedDetails().execute();
                }
                if (order_allocation.equals("1") && sub_status.equals("5")
                        && vehicle_actual_loading_date.equals("null")) {
                    reportingDate = currentDate;
                    reportingTime = currentTime;
                    new TripTrackingActivity.updateUnloadingDetails().execute();
                }

            }
        });
    }


public class updateReportingDetails extends AsyncTask<String, Void, Void> {
    String obj = null;
    String jsonStr = null;

    @Override
    protected void onPreExecute() {
//            pDialog = new ProgressDialog(TripTrackingActivity.this);
//            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
//            pDialog.setCancelable(false);
//            pDialog.show();
    }

    @Override
    protected Void doInBackground(String... strings) {

        jsonStr = webService.updateReportingDetails(trip_id, subStatusId, reportingDate, reportingTime);
        System.out.println("json_string_output====" + jsonStr);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
        if (jsonStr != null) {
            try {
                JSONArray jsonArray = new JSONArray(jsonStr);
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                exceptionMsg = jsonObject.getString("expOccur");
                if (exceptionMsg.equalsIgnoreCase("N")) {
                    Toast.makeText(getApplicationContext(),"Sucessfully Updated!",Toast.LENGTH_LONG);
//                    Intent intent = new Intent(TripTrackingActivity.this, TripListActivity.class);
//                    startActivity(intent);
                } else {
                    ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //TripTrackingActivity.this.refreshInbox();
                            Toast.makeText(TripTrackingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            } catch (final JSONException e) {
                ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TripTrackingActivity.this.refreshInbox();
                        Toast.makeText(TripTrackingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                });
            }
        } else {
            ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //TripTrackingActivity.this.refreshInbox();
                    Toast.makeText(TripTrackingActivity.this, "Could't get data from the server", Toast.LENGTH_LONG).show();
                }
            });

        }

    }


}

public class updateLoadingDetails extends AsyncTask<String, Void, Void> {
    String obj = null;
    String jsonStr = null;

    @Override
    protected void onPreExecute() {
//            pDialog = new ProgressDialog(TripTrackingActivity.this);
//            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
//            pDialog.setCancelable(false);
//            pDialog.show();
    }

    @Override
    protected Void doInBackground(String... strings) {

        jsonStr = webService.updateLoadingDetails(tripId, subStatusId, reportingDate, reportingTime);
        System.out.println("json_string_output====" + jsonStr);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
        if (jsonStr != null) {
            try {
                JSONArray jsonArray = new JSONArray(jsonStr);
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                exceptionMsg = jsonObject.getString("expOccur");
                if (exceptionMsg.equalsIgnoreCase("N")) {
                    Intent intent = new Intent(TripTrackingActivity.this, TripListActivity.class);
                   startActivity(intent);
                    Toast.makeText(getApplicationContext(),"Sucessfully Updated!",Toast.LENGTH_LONG);

                } else {
                    ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //TripTrackingActivity.this.refreshInbox();
                            Toast.makeText(TripTrackingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            } catch (final JSONException e) {
                ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TripTrackingActivity.this.refreshInbox();
                        Toast.makeText(TripTrackingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                });
            }
        } else {
            ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //TripTrackingActivity.this.refreshInbox();
                    Toast.makeText(TripTrackingActivity.this, "Could't get data from the server", Toast.LENGTH_LONG).show();
                }
            });

        }

    }


}

public class updateGateoutDetails extends AsyncTask<String, Void, Void> {
    String obj = null;
    String jsonStr = null;

    @Override
    protected void onPreExecute() {
//            pDialog = new ProgressDialog(TripTrackingActivity.this);
//            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
//            pDialog.setCancelable(false);
//            pDialog.show();
    }

    @Override
    protected Void doInBackground(String... strings) {

        jsonStr = webService.updateGateoutDetails(tripId, subStatusId, reportingDate, reportingTime,odometerReading);
        System.out.println("json_string_output====" + jsonStr);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
        if (jsonStr != null) {
            try {
                JSONArray jsonArray = new JSONArray(jsonStr);
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                exceptionMsg = jsonObject.getString("expOccur");
                if (exceptionMsg.equalsIgnoreCase("N")) {
                    Intent intent = new Intent(TripTrackingActivity.this, TripListActivity.class);
                   startActivity(intent);
                    Toast.makeText(getApplicationContext(),"Sucessfully Updated!",Toast.LENGTH_LONG);
                } else {
                    ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //TripTrackingActivity.this.refreshInbox();
                            Toast.makeText(TripTrackingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            } catch (final JSONException e) {
                ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TripTrackingActivity.this.refreshInbox();
                        Toast.makeText(TripTrackingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                });
            }
        } else {
            ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //TripTrackingActivity.this.refreshInbox();
                    Toast.makeText(TripTrackingActivity.this, "Could't get data from the server", Toast.LENGTH_LONG).show();
                }
            });

        }

    }


}


public class updateDestinationReachedDetails extends AsyncTask<String, Void, Void> {
    String obj = null;
    String jsonStr = null;

    @Override
    protected void onPreExecute() {
        pDialog = new ProgressDialog(TripTrackingActivity.this);
        pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    protected Void doInBackground(String... strings) {

        jsonStr = webService.updateDestinationReachedDetails(tripId, reportingDate, reportingTime);
        System.out.println("json_string_output====" + jsonStr);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }
        if (jsonStr != null) {
            try {
                JSONArray jsonArray = new JSONArray(jsonStr);
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                exceptionMsg = jsonObject.getString("expOccur");
                if (exceptionMsg.equalsIgnoreCase("N")) {
                    Intent intent = new Intent(TripTrackingActivity.this, TripListActivity.class);
                   startActivity(intent);
                    Toast.makeText(getApplicationContext(),"Sucessfully Updated!",Toast.LENGTH_LONG);
                } else {
                    ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //TripTrackingActivity.this.refreshInbox();
                            Toast.makeText(TripTrackingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            } catch (final JSONException e) {
                ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TripTrackingActivity.this.refreshInbox();
                        Toast.makeText(TripTrackingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                });
            }
        } else {
            ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //TripTrackingActivity.this.refreshInbox();
                    Toast.makeText(TripTrackingActivity.this, "Could't get data from the server", Toast.LENGTH_LONG).show();
                }
            });

        }

    }


}

public class updateUnloadingDetails extends AsyncTask<String, Void, Void> {
    String obj = null;
    String jsonStr = null;

    @Override
    protected void onPreExecute() {
        pDialog = new ProgressDialog(TripTrackingActivity.this);
        pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    protected Void doInBackground(String... strings) {

        jsonStr = webService.updateUnloadingDetails(tripId, reportingDate, reportingTime);
        System.out.println("json_string_output====" + jsonStr);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }
        if (jsonStr != null) {
            try {
                JSONArray jsonArray = new JSONArray(jsonStr);
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                exceptionMsg = jsonObject.getString("expOccur");
                if (exceptionMsg.equalsIgnoreCase("N")) {
                    Intent intent = new Intent(TripTrackingActivity.this, TripListActivity.class);
                   startActivity(intent);
                    Toast.makeText(getApplicationContext(),"Sucessfully Updated!",Toast.LENGTH_LONG);

                } else {
                    ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //TripTrackingActivity.this.refreshInbox();
                            Toast.makeText(TripTrackingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            } catch (final JSONException e) {
                ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TripTrackingActivity.this.refreshInbox();
                        Toast.makeText(TripTrackingActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                });
            }
        } else {
            ((AppCompatActivity) TripTrackingActivity.this).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //TripTrackingActivity.this.refreshInbox();
                    Toast.makeText(TripTrackingActivity.this, "Could't get data from the server", Toast.LENGTH_LONG).show();
                }
            });

        }

    }

}


}