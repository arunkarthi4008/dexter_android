package com.application.dexter_new;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RunSheetListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    String Username, userId, DesingId;
    SessionManager session;
    DrawerLayout drawer;
    Dialog dialog;
    final Context context = this;
    String jsonStr;
    String exceptionMsg;
    ListView lv;
    CardView nodatacard;
    ProgressBar loading_sppiner;
    RunSheetListAdapter adapter;
    BottomNavigationView bottomNavigationView;
    DatabaseHelper databaseHelper;
    int connectionStatus = 0;
    private BroadcastReceiver mNetworkReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println(" user Details in runsheetActivity== username==" + Username + " userId==" + userId + " DesigId==" + DesingId);
        setContentView(R.layout.runsheet_list_activity);

        databaseHelper = new DatabaseHelper(getApplicationContext());
        mNetworkReceiver = new NetworkStateChecker();

        //Bottom Navigation

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (DesingId.equals("1034")){
            bottomNavigationView.getMenu().findItem( R.id.line_haul).setVisible(true);
        }
        else{
            bottomNavigationView.getMenu().findItem( R.id.line_haul).setVisible(false);
        }
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Pilot");
        Switch aswitch = headerView.findViewById(R.id.aswitch);

        if (session.getState() == true) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        if (session.getState() == true) {
            aswitch.setChecked(true);
        }
        aswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
                if (ischecked == true) {
                    session.setState(true);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                } else {
                    session.setState(false);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
            }
        });

        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
        MenuItem view_orders = nav_Menu.findItem(R.id.view_orders);
        MenuItem delivery = nav_Menu.findItem(R.id.delivery);


        nav_dashboard.setVisible(false);
        view_orders.setVisible(false);
        delivery.setVisible(true);

        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code

        lv = findViewById(R.id.listview);
        loading_sppiner = findViewById(R.id.loading_spinner);
        nodatacard = findViewById(R.id.noinvoicecard);
        connectionStatus = getConnectionType(context);
        System.out.println("connectionStatus=======" + connectionStatus);

        if (connectionStatus > 0) {
            getRunsheetList();
//            getWaybillList();
        } else {
//            Toast.makeText(getApplicationContext(), "disconnected", Toast.LENGTH_LONG).show();
            databaseHelper.openDB();
            ArrayList<RunSheetTO> deliveryList = (ArrayList<RunSheetTO>) databaseHelper.getRunsheetList();
            databaseHelper.closeDB();
            if (deliveryList.size() > 0) {
                adapter = new RunSheetListAdapter(RunSheetListActivity.this, deliveryList);
                lv.setAdapter(adapter);
            }
        }




    }

    @Override
    protected void onStart() {
        super.onStart();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    protected  void onStop(){
        super.onStop();
        unregisterReceiver(mNetworkReceiver);
    }


    //navigation
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        switch (menuItem.getItemId()) {
            case R.id.orderlst:
                Intent intObj1 = new Intent(this, DashBoard.class);
                startActivity(intObj1);
                return true;
            case R.id.booking:
                Intent intObj2 = new Intent(this, waybill_checking.class);
                startActivity(intObj2);
                return true;
            case R.id.line_haul:
                Intent intObj3 = new Intent(this, TripListActivity.class);
                startActivity(intObj3);
                return true;
            case R.id.logout:
                session.logoutUser();
        }
        return false;
    }
    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.orderlst:
                            Intent intObj1 = new Intent(RunSheetListActivity.this, DashBoard.class);
                            startActivity(intObj1);
                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                            return true;
                        case R.id.booking:
                            Intent intObj2 = new Intent(RunSheetListActivity.this, waybill_checking.class);
                            startActivity(intObj2);
                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                            return true;
                        case R.id.line_haul:
                            Intent intObj3 = new Intent(RunSheetListActivity.this, TripListActivity.class);
                            startActivity(intObj3);
                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                            return true;
                    }
                    return false;
                }
            };
    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }

    //navigation

    //getConnectionStatus
    @IntRange(from = 0, to = 3)
    public static int getConnectionType(Context context) {
        int result = 0; // Returns connection type. 0: none; 1: mobile data; 2: wifi
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (cm != null) {
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = 2;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = 1;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_VPN)) {
                        result = 3;
                    }
                }
            }
        } else {
            if (cm != null) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork != null) {
                    // connected to the internet
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                        result = 2;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        result = 1;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_VPN) {
                        result = 3;
                    }
                }
            }
        }
        return result;
    }
    //getConnectionStatus



    public void getRunsheetList() {

//        pDialog = new ProgressDialog(RunSheetListActivity.this);
//        pDialog.setMessage("Connecting Server... \n Please Wait!!!");
//        pDialog.setCancelable(false);
//        pDialog.show();

        String obj = null;

        new BackgroundTask(RunSheetListActivity.this) {
            @Override
            public void doInBackground() {
                //put you background code
                //same like doingBackground
                //Background Thread
                jsonStr = webService.getRunSheetList(userId, DesingId);
                System.out.println("json_string_output====" + jsonStr);

            }

            @Override
            public void onPostExecute() {
//                if (pDialog.isShowing()) {
//                    pDialog.dismiss();
//                }
                //hear is result part same
                //same like post execute
                //UI Thread(update your UI widget)

                ArrayList<RunSheetTO> deliveryOrderList = new ArrayList<RunSheetTO>();
                if (jsonStr != null) {
                    try {
                        JSONArray jsonArray = new JSONArray(jsonStr);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int n = 0; n < jsonArray.length(); n++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(n);
                                RunSheetTO runSheet = new RunSheetTO(
                                        jsonObject.getString("runsheetId"),
                                        jsonObject.getString("runSheetNumber"),
                                        jsonObject.getString("driverMobile")
                                );
                                deliveryOrderList.add(runSheet);
                            }
                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "No Runsheet Available", Toast.LENGTH_LONG).show();
                                    loading_sppiner.setVisibility(View.GONE);
                                    nodatacard.setVisibility(View.VISIBLE);
                                }
                            });
                        }

                    } catch (final JSONException e) {
                        System.out.println("JSONException=========" + e);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                                loading_sppiner.setVisibility(View.GONE);
                                nodatacard.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                    databaseHelper = new DatabaseHelper(getApplicationContext());
                    databaseHelper.openDB();
                    databaseHelper.clearRunSheetList();
                    databaseHelper.insertRunSheetLists(deliveryOrderList);
                    databaseHelper.closeDB();

                    try {
                        if (deliveryOrderList.size() > 0) {
                            loading_sppiner.setVisibility(View.GONE);
                            lv.setVisibility(View.VISIBLE);
                            nodatacard.setVisibility(View.GONE);
                            adapter = new RunSheetListAdapter(RunSheetListActivity.this, deliveryOrderList);
                            lv.setAdapter(adapter);
                        }
                    } catch (Exception e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loading_sppiner.setVisibility(View.GONE);
                            nodatacard.setVisibility(View.VISIBLE);
                            Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        }.execute();
    }



}
