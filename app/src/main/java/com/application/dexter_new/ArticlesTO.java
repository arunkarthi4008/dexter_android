package com.application.dexter_new;

public class ArticlesTO {

    String Weight_Type_Id;
    String WayBill_Article_Id;
    String WayBill_Mode;
    String PKG_Name;
    String Ewaybill_id;
    String NoOfSaidToContains;
    String Breadth;
    String Article_Name;
    String Uom_Type_Id;
    String NoOfArticle;
    String Length;
    String SaidToContain_Id;
    String SaidTOContain;
    String Height;
    String Volumetricweight;


    public ArticlesTO(String weight_type_id, String wayBill_article_id, String wayBill_mode, String pkg_name, String ewaybill_id, String noOfSaidToContains, String breadth, String article_name, String uom_type_id, String noOfArticle, String length, String saidToContain_id, String saidTOContain, String height,String Volumetricweight) {

        this.Weight_Type_Id = weight_type_id;
        this.WayBill_Article_Id = wayBill_article_id;
        this.WayBill_Mode = wayBill_mode;
        this.PKG_Name = pkg_name;
        this.Ewaybill_id = ewaybill_id;
        this.NoOfSaidToContains = noOfSaidToContains;
        this.Breadth = breadth;
        this.Article_Name = article_name;
        this.Uom_Type_Id = uom_type_id;
        this.NoOfArticle = noOfArticle;
        this.Length = length;
        this.SaidToContain_Id = saidToContain_id;
        this.SaidTOContain = saidTOContain;
        this.Height = height;
        this.Volumetricweight = Volumetricweight;
    }

    public String getArticle_Name() {
        return Article_Name;
    }

    public String getBreadth() {
        return Breadth;
    }

    public String getEwaybill_id() {
        return Ewaybill_id;
    }

    public String getHeight() {
        return Height;
    }

    public String getLength() {
        return Length;
    }

    public String getNoOfArticle() {
        return NoOfArticle;
    }

    public String getNoOfSaidToContains() {
        return NoOfSaidToContains;
    }

    public String getPKG_Name() {
        return PKG_Name;
    }

    public String getSaidTOContain() {
        return SaidTOContain;
    }

    public String getSaidToContain_Id() {
        return SaidToContain_Id;
    }

    public String getUom_Type_Id() {
        return Uom_Type_Id;
    }

    public String getWayBill_Article_Id() {
        return WayBill_Article_Id;
    }

    public String getWayBill_Mode() {
        return WayBill_Mode;
    }

    public String getWeight_Type_Id() {
        return Weight_Type_Id;
    }

    public void setArticle_Name(String article_Name) {
        Article_Name = article_Name;
    }

    public void setBreadth(String breadth) {
        Breadth = breadth;
    }

    public void setEwaybill_id(String ewaybill_id) {
        Ewaybill_id = ewaybill_id;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public void setLength(String length) {
        Length = length;
    }

    public void setNoOfArticle(String noOfArticle) {
        NoOfArticle = noOfArticle;
    }

    public void setNoOfSaidToContains(String noOfSaidToContains) {
        NoOfSaidToContains = noOfSaidToContains;
    }

    public String getVolumetricweight() {
        return Volumetricweight;
    }

    public void setPKG_Name(String PKG_Name) {
        this.PKG_Name = PKG_Name;
    }

    public void setSaidTOContain(String saidTOContain) {
        SaidTOContain = saidTOContain;
    }

    public void setSaidToContain_Id(String saidToContain_Id) {
        SaidToContain_Id = saidToContain_Id;
    }

    public void setUom_Type_Id(String uom_Type_Id) {
        Uom_Type_Id = uom_Type_Id;
    }

    public void setWayBill_Article_Id(String wayBill_Article_Id) {
        WayBill_Article_Id = wayBill_Article_Id;
    }

    public void setWayBill_Mode(String wayBill_Mode) {
        WayBill_Mode = wayBill_Mode;
    }

    public void setWeight_Type_Id(String weight_Type_Id) {
        Weight_Type_Id = weight_Type_Id;
    }

    public void setVolumetricweight(String volumetricweight) {
        Volumetricweight = volumetricweight;
    }
}

