package com.application.dexter_new;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.HashMap;

public class waybill_webview_load extends AppCompatActivity {

    String Waybillmode, branchId, userId, Username, Password, DesingId;
    ProgressBar progressBar;
    SessionManager session;
    LinearLayout webviewlayout, progressBarlayout;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waybill_webview_load);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBarlayout = (LinearLayout) findViewById(R.id.progressBarlayout);
        webviewlayout = (LinearLayout) findViewById(R.id.webviewlayout);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        Password = user.get(SessionManager.KEY_PASSWORD);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        branchId = user.get(SessionManager.KEY_BRANCH);


        WebView myWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = myWebView.getSettings();



        //Set login URL for session management





        Intent intObj = getIntent();
        Waybillmode = intObj.getStringExtra("Waybillmode");
        branchId = intObj.getStringExtra("branchId");
        userId = intObj.getStringExtra("userId");

        String loginUrl = "http://54.71.179.240/throttle/login.do?userName=" + Username + "&password=" + Password + "&branchId=" + branchId;






        if (Waybillmode.equals("1")) {
            myWebView.loadUrl(loginUrl);
            new android.os.Handler(Looper.getMainLooper()).postDelayed(
                    new Runnable() {
                        public void run() {
                            myWebView.loadUrl("http://54.71.179.240/throttle/handleWayBillAdd.do?menuClick=1&waybillMode=1&multiplicationFactor=1&branchId=" + branchId + "&fromBranchTypeId=" + userId);
                        }
                    },
                    3000);

            progressBarlayout.setVisibility(View.GONE);
            webviewlayout.setVisibility(View.VISIBLE);
            myWebView.setWebViewClient(new MyWebViewClient());
            webSettings.setJavaScriptEnabled(true);


        } else if (Waybillmode.equals("2")) {
            myWebView.loadUrl(loginUrl);
            new android.os.Handler(Looper.getMainLooper()).postDelayed(
                    new Runnable() {
                        public void run() {
                            myWebView.loadUrl("http://54.71.179.240/throttle/handleWayBillAdd.do?menuClick=1&waybillMode=1&multiplicationFactor=1&branchId=" + branchId + "&fromBranchTypeId=" + userId);
                        }
                    },
                    3000);
            progressBarlayout.setVisibility(View.GONE);
            webviewlayout.setVisibility(View.VISIBLE);
            myWebView.setWebViewClient(new MyWebViewClient());
            webSettings.setJavaScriptEnabled(true);

        } else if (Waybillmode.equals("3")) {
            myWebView.loadUrl(loginUrl);
            new android.os.Handler(Looper.getMainLooper()).postDelayed(
                    new Runnable() {
                        public void run() {
                            myWebView.loadUrl("http://54.71.179.240/throttle/handleWayBillAdd.do?menuClick=1&waybillMode=1&multiplicationFactor=1&branchId=" + branchId + "&fromBranchTypeId=" + userId);
                        }
                    },
                    3000);
            progressBarlayout.setVisibility(View.GONE);
            webviewlayout.setVisibility(View.VISIBLE);
            myWebView.setWebViewClient(new MyWebViewClient());
            webSettings.setJavaScriptEnabled(true);

        }





    }
    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            Toast.makeText(getApplicationContext(),"loaded Sucessfully =", Toast.LENGTH_SHORT);


        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Toast.makeText(getApplicationContext(),"loaded Sucessfully ==",Toast.LENGTH_SHORT);
            return true;


        }

        @Override
        public void onPageFinished(WebView view, String url) {
            //Page load finished
            super.onPageFinished(view, url);
            Toast.makeText(getApplicationContext(),"loaded Sucessfully ===",Toast.LENGTH_SHORT);


        }

    }

    @Override
    public void onBackPressed() {

        WebView myWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = myWebView.getSettings();
        String logoutUrl= "http://54.71.179.240/throttle/logout.do?menuClick=1";

        System.out.println("Login Url For the the String "+logoutUrl);

        myWebView.setWebViewClient(new MyWebViewClient());
        webSettings.setJavaScriptEnabled(true);
        myWebView.loadUrl(logoutUrl);
        progressBarlayout.setVisibility(View.GONE);
        webviewlayout.setVisibility(View.VISIBLE);


        Intent intent = new Intent(this,waybill_checking.class);
        startActivity(intent);


    }
}



