package com.application.dexter_new;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import static android.content.ContentValues.TAG;

public class webService {



    //    private static String RestURL = "http://52.66.244.61/HSServiceAPI/api/v1/bfil/";
    //    private static String RestURL = "http://demo.throttletms.com:8088/ThrottleService/WS/finAgg/";
//    private static String RestURL1 = "http://52.66.244.61/HSServiceAPI/api/v1/bfil/";

//    private static String RestURL = "http://52.66.244.61/HSServiceAPI/api/v1/bfil/";
//    private static String RestURL1 = "http://52.66.244.61/HSServiceAPI/api/v1/bfil/";

//    private static String RestURL = "http://124.123.68.18:8086/CSDServiceAPI/api/v1/";
//    private static String RestURL1 = "http://124.123.68.18:8086/CSDServiceAPI/api/v1/";

//    private static String RestURL = "http://192.168.0.172:8080/CSDServiceAPI/api/v1/";//kova local
//    private static String RestURL1 = "http://192.168.0.172:8080/CSDServiceAPI/api/v1/";//kova local

//    private static String RestURL = "http://54.71.179.240:8899/CSDServiceAPI/api/v1/";//csd demo
//    private static String RestURL1 = "http://54.71.179.240:8899/CSDServiceAPI/api/v1/";//csd demo

//    private static String RestURL = "http://18.237.31.209:8899/CSDServiceAPI/api/v1/";//csd live
//    private static String RestURL1 = "http://18.237.31.209:8899/CSDServiceAPI/api/v1/";//csd live

//    public static String RestURL = "http://dexters.throttletms.com:8899/DEXTERServiceAPI/api/v1/";//dexter live
//    public static String RestURL1 = "http://dexters.throttletms.com:8899/DEXTERServiceAPI/api/v1/";//dexter live

    public static String RestURL = "http://192.168.0.135:8080/DEXTERServiceAPI/api/v1/";//dexter local
    public static String RestURL1 = "http://192.168.0.135:8080/DEXTERServiceAPI/api/v1/";//dexter local

//        public static String RestURL = "http://192.168.0.197:8080/DEXTERServiceAPI/api/v1/";//dexter local
//        public static String RestURL1 = "http://192.168.0.197:8080/DEXTERServiceAPI/api/v1/";//dexter local

//        public static String RestURL = "http://192.168.43.191:8080/DEXTERServiceAPI/api/v1/";//dexter local
//        public static String RestURL1 = "http://192.168.43.191:8080/DEXTERServiceAPI/api/v1/";//dexter local


//       public static String RestURL = "http://183.83.187.31:8080/DEXTERServiceAPI/api/v1/";//dexter local
//        public static String RestURL1 = "http://183.83.187.31:8080/DEXTERServiceAPI/api/v1/";//dexter local

//     private static String RestURL = "http://59.144.140.13:8088/HSServiceAPI/api/v1/bfil/";//local
//    private static String RestURL1 = "http://59.144.140.13:8088/HSServiceAPI/api/v1/bfil/";//local

//     private static String RestURL = "http://csd.throttletms.com:8899/CSDServiceAPI/api/v1/";//live
//    private static String RestURL1 = "http://csd.throttletms.com:8899/CSDServiceAPI/api/v1/";//live

//    http://192.168.0.172:8080

    private static final String ContentType = "application/json";


    public static String verifyUser(String userName,String password) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "checkLoginUser?username="+userName+"&password="+password;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;

        System.out.println("Response of the login ============ >> "+ response);
        return response;

    }

    public static String getRunSheetList(String userId, String desigId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getRunSheetDetails?userId=" + userId + "&desigId=" + desigId;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getWaybillList(String userId, String runsheetId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getWaybillDetails?userId=" + userId + "&runsheetId=" + runsheetId;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;


        return response;

    }



    public static String updatePod(String waybillId, String obj, String userId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updatePod?waybillId=" + waybillId + "&userId=" + userId ;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
            output.write(obj);
            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }






    private static String convertStreamToString(InputStream is) {
        RestURL = RestURL1;
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }



    public static String callapi(String ewaybillnum ,String gstno) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            gstno = "05AAAAU1183B5ZW";
            RestURL = RestURL1 + "callEwayBillApi?ewayBillNo=" + ewaybillnum + "&gstNo=" + gstno;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;

        System.out.println("responseresponse"+response);
        return response;

    }

    public static String getOrderlist(String userId, String desigId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getOrderlist?userId=" + userId + "&desigId=" + desigId;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getManualnolist(String BranchId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getManualnolist?BranchId=" + BranchId ;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getSkunamelist(String BranchId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getSKUNamelist?BranchId=" + BranchId ;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public  static String getPakaginglist(){
        String response = null;
        HttpURLConnection conn = null;

        try {
            String   RestURLpkg = "http://dexters.throttletms.com:8899/DEXTERServiceAPI/api/v1/getPakaginglist" ;
//            String   RestURLpkg = "http://192.168.0.197:8080/DEXTERServiceAPI/api/v1/getPakaginglist" ;
//            String   RestURLpkg = "http://183.83.187.31:8080/DEXTERServiceAPI/api/v1/getPakaginglist" ;
//            String   RestURLpkg = "http://192.168.43.191:8080/DEXTERServiceAPI/api/v1/getPakaginglist" ;
            System.out.println("Url===" + RestURLpkg);
            URL url = new URL(RestURLpkg);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        return response;
    }
    public static String Insertarticle(String waybillId, String obj, String userId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "Insertarticle?waybillId=" + waybillId + "&userId=" + userId ;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
            output.write(obj);
            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public  static String getArticlesList(String WaybilId){
        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getArticlelist?waybillId=" + WaybilId;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;
    }

        public static String confirmArticles(String waybilId, String UserId){
            String response = null;
            HttpURLConnection conn = null;
            try {
            RestURL = RestURL + "confirmArticles?waybilId="+waybilId+"&UserId="+UserId;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("PUT");
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
            }catch (MalformedURLException e) {
                    Log.e(TAG, "MalformedURLException: " + e.getMessage());
                } catch (ProtocolException e) {
                    Log.e(TAG, "ProtocolException: " + e.getMessage());
                } catch (IOException e) {
                    Log.e(TAG, "IOException: " + e.getMessage());
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                } finally {
                    conn.disconnect();
                }
                RestURL = RestURL1;
                return response;
            }

    public static String Insertinvoice(String obj, String invoicenum, String userId) {

        String response = null;
        HttpURLConnection conn = null;

        try {

            System.out.println("object og ewaybill insert ===>"+ obj);
            RestURL = RestURL + "Insertewaybilllist?waybillId=" + invoicenum + "&userId=" + userId ;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
                conn.setRequestMethod("POST");


            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
            output.write(obj);
            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }
    public static String Deletearticles(String waybilId){
        String response = null;
        HttpURLConnection conn = null;
        try {
            RestURL = RestURL + "Deletearticles?waybilId="+waybilId;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
        }catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;
    }
    public static String GetArticleStatus(String waybilnum){
        String response = null;
        HttpURLConnection conn = null;
        try {
            RestURL = RestURL + "GetArticleStatus?waybilnum="+waybilnum;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
        }catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }


    public static String Ewaybillinsert(String obj,String WaybillId ,String userId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "Insertoperationwaybill?ewaybillId="+WaybillId + "&userId=" + userId ;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
            output.write(obj);
            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String getTripList(String userId,String desigId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "getTripList?userId=" + userId + "&desigId=" + desigId;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("GET");


            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }


    public static String updateReportingDetails(String tripId,String subStatusId,String reportingDate,String reportingTime) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateReportingDetails?tripId=" + tripId+"&substatusId="+subStatusId+"&reportingDate="+reportingDate+"&repotingTime="+reportingTime;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }


    public static String updateLoadingDetails(String tripId,String subStatusId,String reportingDate,String reportingTime) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateLoadingDetails?tripId=" + tripId+"&substatusId="+subStatusId+"&reportingDate="+reportingDate+"&repotingTime="+reportingTime;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }
    public static String updateGateoutDetails(String tripId,String subStatusId,String reportingDate,String reportingTime,String odoMeterReading) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateGateoutDetails?tripId=" + tripId+"&substatusId="+subStatusId+"&reportingDate="+reportingDate+"&repotingTime="+reportingTime+"&odometerReading="+odoMeterReading;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String updateDestinationReachedDetails(String tripId,String reportingDate,String reportingTime) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateDestinationReachedDetails?tripId=" + tripId+"&reportingDate="+reportingDate+"&repotingTime="+reportingTime;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String updateUnloadingDetails(String tripId,String reportingDate,String reportingTime) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateUnloadingDetails?tripId=" + tripId+"&reportingDate="+reportingDate+"&repotingTime="+reportingTime;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }


    public static String updateTripEnd(String tripId,String reportingDate,String reportingTime) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateTripEnd?tripId=" + tripId+"&reportingDate="+reportingDate+"&repotingTime="+reportingTime;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }


    public static String updateDocument(String tripId, String obj, String userId) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateDocument?tripId=" + tripId + "&userId=" + userId ;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
            output.write(obj);
            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String updateInterimGateInReportingDetails(String tripId,String pointId,String reportingDate,String reportingTime, String routeOrder) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateInterimGateInReportingDetails?tripId="+tripId+"&pointId="+pointId+"&reportingDate="+reportingDate+"&repotingTime="+reportingTime+"&routeOrder="+routeOrder;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String updateInterimGateOutReportingDetails(String tripId,String pointId,String reportingDate,String reportingTime,String routeOrder) {

        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "updateInterimGateOutReportingDetails?tripId="+tripId+"&pointId="+pointId+"&reportingDate="+reportingDate+"&repotingTime="+reportingTime+"&routeOrder="+routeOrder;
            System.out.println("Url===" + RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");


//            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
//            output.write(obj);
//            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;

    }

    public static String uploadInterimPod(String trip_id,String user_id,String pointId,String obj,String routeOrder) {
        String response = null;
        HttpURLConnection conn = null;

        try {
            RestURL = RestURL + "uploadInterimPod?tripId="+trip_id+"&userId="+user_id+"&pointId="+pointId+"&routeOrder="+routeOrder;
            System.out.println("Url==="+RestURL);
            URL url = new URL(RestURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", ContentType);
            conn.setRequestMethod("POST");

            OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
            output.write(obj);
            output.flush();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        } finally {
            conn.disconnect();
        }
        RestURL = RestURL1;
        return response;
    }





}
