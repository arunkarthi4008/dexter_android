package com.application.dexter_new;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.transferwise.sequencelayout.SequenceStep;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class TripListAdapter extends BaseAdapter {

    Context context;
    ArrayList<TripListTO> runSheetList;
    TextView trip_code_TV, customer_name_TV, origin_TV, destination_TV, status_TV;
    Button view,lrprint;
    int itPosition = 0;
    String tripId, sub_status, exceptionMsg, eWayBillNo,odometerReading;
    String reportingDate, reportingTime, loadingTime, loadingDate, tripStartDate, tripEndDate, subStatusId;
    //    String CustomerName,origin,destination,Consignor,trip_status,weight,qty,Package,remarks,
//            vehicle_id,startKm,des_cityId,org_cityId,lastKm,trip_status_id,inv_id,inv_no,imgUploadStatus,trip_type;
//
    private ProgressDialog pDialog;
    SequenceStep step1, step2, step3, step4, step5, step6;

    public TripListAdapter(Context context, ArrayList<TripListTO> runSheetList) {
        this.context = context;
        this.runSheetList = runSheetList;
    }

    @Override
    public int getCount() {
        return runSheetList.size();
    }

    @Override
    public Object getItem(int position) {
        return runSheetList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return runSheetList.indexOf(getItem(position));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, final ViewGroup viewGroup) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = null;
        convertView = mInflater.inflate(R.layout.trip_list_adapter, null);

        trip_code_TV = (TextView) convertView.findViewById(R.id.tripcode);
        customer_name_TV = (TextView) convertView.findViewById(R.id.customername);
        destination_TV = (TextView) convertView.findViewById(R.id.destination1);
        status_TV = (TextView) convertView.findViewById(R.id.status);


        TripListTO runSheet = runSheetList.get(position);
        trip_code_TV.setText(runSheet.getTripCode());
        System.out.println("CustomerName==>" + runSheet.getCustomerName());
        String Customer_name = runSheet.getCustomerName();
        customer_name_TV.setText(Customer_name);
        destination_TV.setText(runSheet.getOrigin() + "  to   " + runSheet.getDestination());


        if (runSheet.getTripStatus().equals("8")) {
            status_TV.setText("Yet To Start");
            status_TV.setTextColor(Color.parseColor("#ff0000"));
        }
        if (runSheet.getTripStatus().equals("10")) {
            status_TV.setText("Started");
            status_TV.setTextColor(Color.parseColor("#15f505"));
        }

        view = (Button) convertView.findViewById(R.id.button1);

        view.setTag(position);






        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, TripTrackingActivity.class);
                intent.putExtra("trip_id", runSheet.getTripId());
                intent.putExtra("trip_code", runSheet.getTripCode());
                intent.putExtra("substatus", runSheet.getSubStatus());
                intent.putExtra("customerName", runSheet.getCustomerName());
                intent.putExtra("Origin", runSheet.getOrigin());
                intent.putExtra("destination", runSheet.getDestination());
                intent.putExtra("ewaybill_no", runSheet.geteWayBillNumber());
                intent.putExtra("trip_status_id", runSheet.getTripStatus());
                intent.putExtra("last_km", runSheet.getLastEndKm());
                intent.putExtra("vehicle_origin_actual_reporting_date", runSheet.getVehicle_origin_actual_reporting_date());
                intent.putExtra("vehicle_origin_actual_reporting_time", runSheet.getVehicle_origin_actual_reporting_time());
                intent.putExtra("vehicle_actual_loading_date", runSheet.getVehicle_actual_loading_date());
                intent.putExtra("vehicle_actual_loading_time", runSheet.getVehicle_actual_loading_time());
                intent.putExtra("trip_actual_start_date", runSheet.getTrip_actual_start_date());
                intent.putExtra("trip_actual_start_time", runSheet.getTrip_actual_start_time());
                intent.putExtra("vehicle_destination_actual_reporting_date", runSheet.getVehicle_destination_actual_reporting_date());
                intent.putExtra("vehicle_destination_actual_reporting_time", runSheet.getVehicle_destination_actual_reporting_time());
                intent.putExtra("extra_expense_status", runSheet.getExpense_status());
                intent.putExtra("vechile_actual_unloading_date", runSheet.getVehicle_actual_unloading_date());
                intent.putExtra("vehicle_actual_unloading_time", runSheet.getVehicle_actual_unloading_time());
                intent.putExtra("order_allocation", runSheet.getOrder_allocation());
                intent.putExtra("complete_flag", runSheet.getComplete_flag());
                context.startActivity(intent);
            }
        });



        return convertView;
    }




}
