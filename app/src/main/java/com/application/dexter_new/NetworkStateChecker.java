package com.application.dexter_new;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NetworkStateChecker extends BroadcastReceiver {


    private Context context;
    private DatabaseHelper db;
    SessionManager session;
    String Username, userId, DesingId;

    @Override
    public void onReceive(Context context, Intent intent) {

        session = new SessionManager(context);
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);

        try
        {
            if (isOnline(context)) {
//                dialog(true);
//                Toast.makeText(context, "Back to Online", Toast.LENGTH_SHORT).show();
                getRunSheetList();
            } else {
//                dialog(false);
                Toast.makeText(context, "No Inernet", Toast.LENGTH_SHORT).show();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }
    private boolean isOnline(Context context) {
        try {
            System.out.println(":iam in isonline=====");
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
//            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            System.out.println("capabilities======"+capabilities);
            return (capabilities != null );
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }
    private void getRunSheetList() {
        System.out.println("================calling api in background==============");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, webService.RestURL+"getRunSheetDetails?userId="+userId+ "&desigId=" + DesingId,
                response -> {
                    try {
                        ArrayList<RunSheetTO> deliveryOrderList = new ArrayList<RunSheetTO>();
                        System.out.println("response in background========"+response);
                        JSONArray jsonArray = new JSONArray(response);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int n = 0; n < jsonArray.length(); n++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(n);
                                RunSheetTO runSheet = new RunSheetTO(
                                        jsonObject.getString("runsheetId"),
                                        jsonObject.getString("runSheetNumber"),
                                        jsonObject.getString("driverMobile")
                                );
                                deliveryOrderList.add(runSheet);
                            }
                            db = new DatabaseHelper(context);
                            db.openDB();
                            db.clearRunSheetList();
                            db.insertRunSheetLists(deliveryOrderList);
                            db.closeDB();
                        }else{

                        }
//                        context.sendBroadcast(new Intent("com.application.dexter.RunSheetListActivity"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {
                    System.out.println("============error happended==========");
                }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                params.put("name", name);
//                return params;
//            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }











}
