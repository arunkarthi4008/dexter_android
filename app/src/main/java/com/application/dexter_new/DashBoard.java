package com.application.dexter_new;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DashBoard extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    String Username, userId, DesingId, branchId;
    SessionManager session;
    DrawerLayout drawer;
    BottomNavigationView bottomNavigationView;
    Dialog dialog;
    String jsonStr;
    final Context context = this;
    DatabaseHelper databaseHelper;
    OrderListAdapter adapter;
    ListView listview;
    ProgressBar loading_spinner;
    TextView count ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        branchId = user.get(SessionManager.KEY_BRANCH);
        System.out.println("username==" + Username + "  userId==" + userId + " DesigId==" + DesingId + "BranchId == " + branchId);

        setContentView(R.layout.dashboard);

        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (DesingId.equals("1033")){
            navigationView.getMenu().findItem(R.id.line_haul).setVisible(true);
        }else{
            navigationView.getMenu().findItem(R.id.line_haul).setVisible(false);

        }
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        listview = findViewById(R.id.listview);
        loading_spinner = findViewById(R.id.loading_spinner);
        navReName.setText("Pilot");
        Switch aswitch = headerView.findViewById(R.id.aswitch);
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        if (session.getState() == true) {
            aswitch.setChecked(true);
        }
        aswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
                if (ischecked == true) {
                    session.setState(true);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                } else {
                    session.setState(false);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
            }
        });

        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
        MenuItem view_orders = nav_Menu.findItem(R.id.view_orders);
        nav_dashboard.setVisible(false);
        view_orders.setVisible(false);
        getOrderList();
        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code

        //Bottom Navigation

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        if (DesingId.equals("1034")){
            bottomNavigationView.getMenu().findItem(R.id.line_haul).setVisible(true);
        }
        else{
            bottomNavigationView.getMenu().findItem(R.id.line_haul).setVisible(false);
        }
        count = findViewById(R.id.count);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        System.out.println("Menu items show == >" + menuItem.getItemId());
        switch (menuItem.getItemId()) {
            case R.id.delivery:
                Intent intObj1 = new Intent(DashBoard.this, RunSheetListActivity.class);
                startActivity(intObj1);
                return true;
            case R.id.booking:
                Intent intObj2 = new Intent(DashBoard.this, waybill_checking.class);
                startActivity(intObj2);
                return true;
            case R.id.line_haul:
                Intent intObj3 = new Intent(DashBoard.this, TripListActivity.class);
                startActivity(intObj3);
                return true;
            case R.id.logout:
                session.logoutUser();
                return true;
        }
        return false;
    }
    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.delivery:
                            Intent intObj1 = new Intent(DashBoard.this, RunSheetListActivity.class);
                            startActivity(intObj1);
                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                            return true;
                        case R.id.booking:
                            Intent intObj2 = new Intent(DashBoard.this, waybill_checking.class);
                            startActivity(intObj2);
                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                            return true;
                        case R.id.line_haul:
                            Intent intObj3 = new Intent(DashBoard.this, TripListActivity.class);
                            startActivity(intObj3);
                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                            return true;
                    }
                    return false;
                }
            };
    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }
    @Override
    public void onBackPressed() {


        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setTitle("Leave application?");
//        dialog.setMessage("Are you sure you want to leave the application?");
        dialog.setContentView(R.layout.exit_alert_layout);
        dialog.setCancelable(false);
        Button yesButton = (Button) dialog.findViewById(R.id.yes);
        Button noButton = (Button) dialog.findViewById(R.id.no);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView message = (TextView) dialog.findViewById(R.id.message);
        title.setText("Leave application?");
        message.setText("Are you sure you want to leave the application?");


        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.logoutUser();
            }
        });
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void getOrderList() {

        String obj = null;

        new BackgroundTask(this) {
            @Override
            public void doInBackground() {
                jsonStr = webService.getOrderlist(userId, DesingId);
//                jsonStr =webService.getOrderlist();
                System.out.println("json_string_output====" + jsonStr);

            }

            @Override
            public void onPostExecute() {

                ArrayList<OrderListTo> Orderlist = new ArrayList<OrderListTo>();
                if (jsonStr != null) {
                    try {
                        JSONArray jsonArray = new JSONArray(jsonStr);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int n = 0; n < jsonArray.length(); n++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(n);
                                OrderListTo order_list = new OrderListTo(
                                        jsonObject.getString("cust_id"),
                                        jsonObject.getString("customer_name"),
                                        jsonObject.getString("vehicle_type"),
                                        jsonObject.getString("driver_name"),
                                        jsonObject.getString("way_bill_type"),
                                        jsonObject.getString("CFT_factor"),
                                        jsonObject.getString("requestId"),
                                        jsonObject.getString("prerequest_code")
                                        );
                                Orderlist.add(order_list);
                                String Count = String.valueOf(Orderlist.size());
                                count.setText(Count);


                            }
                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "No Orderlist Found!", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                    } catch (final JSONException e) {
                        System.out.println("JSONException=========" + e);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
//                    databaseHelper = new DatabaseHelper(getApplicationContext());
//                    databaseHelper.openDB();
//                    databaseHelper.clearRunSheetList();
//                    databaseHelper.insertRunSheetLists(Orderlist);
//                    databaseHelper.closeDB();

                    try {
                        if (Orderlist.size() > 0) {
                            loading_spinner.setVisibility(View.GONE);
                            listview.setVisibility(View.VISIBLE);
                            adapter = new OrderListAdapter(DashBoard.this, Orderlist);
                            listview.setAdapter(adapter);
                        }
                    } catch (Exception e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        }.execute();



    }

}
