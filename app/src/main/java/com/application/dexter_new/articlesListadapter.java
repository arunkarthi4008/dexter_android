package com.application.dexter_new;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class articlesListadapter  extends BaseAdapter implements Filterable {

        Context context;
        ArrayList<ArticlesTO> articlesList;
        ArrayList<ArticlesTO> articlesListDisplayed;
        TextView skunameval,pkgtypelay,nospkglay,lengthtxtlay,breadthval,heightval,per_piece_weightla,volumetricval;
        Button Editbtn,removebtn;
        int itPosition = 0;
        String customerId;

        public articlesListadapter(Context context, ArrayList<ArticlesTO> articlesList) {

            this.context = context;
            this.articlesList = articlesList;
            this.articlesListDisplayed = articlesList;
        }

        @Override
        public int getCount() {
            return articlesListDisplayed.size();
        }

        @Override
        public Object getItem(int i) {
            return articlesListDisplayed.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {

            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = null;


            convertView = mInflater.inflate(R.layout.article_list_adapter, null);


            skunameval = (TextView) convertView.findViewById(R.id.skunameval);
            pkgtypelay = (TextView) convertView.findViewById(R.id.pkgtypeingval);
            nospkglay = (TextView) convertView.findViewById(R.id.nospkg);
//            lengthtxtlay = (TextView) convertView.findViewById(R.id.lengthval);
//            breadthval = (TextView) convertView.findViewById(R.id.breadthval);
//            heightval = (TextView) convertView.findViewById(R.id.heightval);
//            per_piece_weightla = (TextView) convertView.findViewById(R.id.perpieceval);
            volumetricval = (TextView) convertView.findViewById(R.id.volumetricval);
            Editbtn = (Button) convertView.findViewById(R.id.Editdetailbtn);

            Editbtn.setTag(position);

            ArticlesTO articlelist = articlesListDisplayed.get(position);


                    skunameval.setText(articlelist.getArticle_Name());
                    pkgtypelay.setText(articlelist.getPKG_Name());
//                    nospkglay.setText(articlelist.getNoOfArticle());
//                    lengthtxtlay.setText(articlelist.getLength());
//                     breadthval.setText(articlelist.getBreadth());
//                    heightval.setText(articlelist.getHeight());
                    volumetricval.setText(articlelist.getVolumetricweight());




            Editbtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int pos = (Integer) v.getTag();

                    Toast.makeText(context.getApplicationContext(), "Edit position is == > ",Toast.LENGTH_LONG);
                                  }
            });
            return convertView;
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    articlesListDisplayed = (ArrayList<ArticlesTO>) results.values; // has the filtered values
                    notifyDataSetChanged();  // notifies the data with new filtered values
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                    ArrayList<RunSheetTO> FilteredArrList = new ArrayList<RunSheetTO>();

                    if (articlesList == null) {
                        articlesList = new ArrayList<ArticlesTO>(articlesListDisplayed); // saves the original data in mOriginalValues
                    }
                    /********
                     *
                     *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                     *  else does the Filtering and returns F
                     *  ilteredArrList(Filtered)
                     *
                     ********/
                    if (constraint == null || constraint.length() == 0) {

                        // set the Original result to return
                        results.count = articlesList.size();
                        results.values = articlesList;
                    } else {
                        constraint = constraint.toString().toLowerCase();
                        for (int i = 0; i < articlesList.size(); i++) {
//                            if (data.toLowerCase().contains(constraint.toString())) {
//                                FilteredArrList.add(new ArticlesTO(
////                                        articlesList.get(i).getCustomerId(),
////                                        articlesList.get(i).getCustomerName(),
////                                        articlesList.get(i).getDrivername()));
//                            }
                        }
                        results.count = FilteredArrList.size();
                        results.values = FilteredArrList;
                    }
                    return results;
                }
            };
            return filter;
        }
    }

