package com.application.dexter_new;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class InterimTripListAdapter extends BaseAdapter {
    Context context;
    ArrayList<com.application.dexter_new.InterimTripListTO> runSheetList;
    LinearLayout gateInOutDetails;
    TextView  gateInOut, destination_TV;
    Button view;
    int itPosition = 0;
    String pointId,tripId,reportingDate, reportingTime,exceptionMsg,routeOrder;
    private ProgressDialog pDialog;

    public InterimTripListAdapter(Context context, ArrayList<com.application.dexter_new.InterimTripListTO> runSheetList, String tripId) {
        this.context = context;
        this.runSheetList = runSheetList;
        this.tripId = tripId;
    }

    @Override
    public int getCount() {
        return runSheetList.size();
    }

    @Override
    public Object getItem(int position) {
        return runSheetList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return runSheetList.indexOf(getItem(position));
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup viewGroup) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = null;
        convertView = mInflater.inflate(R.layout.interim_trip_list_adapter, null);

        destination_TV = (TextView) convertView.findViewById(R.id.destination);
        gateInOutDetails = convertView.findViewById(R.id.gateInDetails);
        gateInOut = convertView.findViewById(R.id.gateInOut);
        view = convertView.findViewById(R.id.button1);
        view.setTag(position);

        com.application.dexter_new.InterimTripListTO runSheet = runSheetList.get(position);
        destination_TV.setText(runSheet.getPoint_name());

        if(runSheet.getCompleted_flag().equals("1")){
            gateInOutDetails.setVisibility(View.VISIBLE);
            gateInOut.setText("Gate in :"+runSheet.getIn_date()+ " " +runSheet.getIn_time() + "\n Gate out :"+runSheet.getOut_date()+ " " +runSheet.getOut_time() );
            view.setVisibility(View.GONE);
        } else if(runSheet.getIn_date() .equals("null") || runSheet.getIn_time() .equals("null")){
        view.setText("Update In Date & Time");
        } else if(runSheet.getFile_name().equals("")){
        view.setText("UpLoad POD");
        }else if(runSheet.getOut_date() .equals("null") || runSheet.getOut_time() .equals("null")){
            view.setText("Update Out Date & Time");
        }

        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int pos = (Integer) v.getTag();
                com.application.dexter_new.InterimTripListTO OrderClicked = runSheetList.get(pos);
                itPosition = pos;

                String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

                pointId = OrderClicked.getPoint_id();
                System.out.println("tripId==="+tripId+" ,pointId==="+pointId);
                System.out.println("date==="+OrderClicked.getIn_date());
                if(OrderClicked.getIn_date() .equals("null") || OrderClicked.getIn_time() .equals("null")){
                    routeOrder = OrderClicked.getRoute_order();
                    reportingDate = currentDate;
                    reportingTime = currentTime;
                    new updateInterimGateInReportingDetails().execute();
                }else if(OrderClicked.getFile_name().equals("")){
                    Intent intent = new Intent(context, UploadInterimDocument.class);
                    intent.putExtra("tripId", tripId);
                    intent.putExtra("pointId", pointId);
                    intent.putExtra("routeOrder", OrderClicked.getRoute_order());

                    context.startActivity(intent);
                }else if(OrderClicked.getOut_date() .equals("null") || OrderClicked.getOut_time() .equals("null")){
                    routeOrder = OrderClicked.getRoute_order();
                    reportingDate = currentDate;
                    reportingTime = currentTime;
                    new updateInterimGateOutReportingDetails().execute();
                }
            }
        });

        return convertView;
    }


    public class updateInterimGateInReportingDetails extends AsyncTask<String, Void, Void> {
        String obj = null;
        String jsonStr = null;

        @Override
        protected void onPreExecute() {
//            pDialog = new ProgressDialog(context);
//            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
//            pDialog.setCancelable(false);
//            pDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {

//            jsonStr = webService.updateInterimGateInReportingDetails(tripId, pointId, reportingDate, reportingTime, routeOrder);
            System.out.println("json_string_output====" + jsonStr);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
            if (jsonStr != null) {
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    exceptionMsg = jsonObject.getString("expOccur");
                    if (exceptionMsg.equalsIgnoreCase("N")) {
                        Intent intent = new Intent(context, com.application.dexter_new.InterimDetailsActivity.class);
                        intent.putExtra("tripId",tripId);
                        context.startActivity(intent);
                    } else {
                        ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //context.refreshInbox();
                                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                } catch (final JSONException e) {
                    ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //context.refreshInbox();
                            Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else {
                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //context.refreshInbox();
                        Toast.makeText(context, "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });

            }

        }


    }

    public class updateInterimGateOutReportingDetails extends AsyncTask<String, Void, Void> {
        String obj = null;
        String jsonStr = null;

        @Override
        protected void onPreExecute() {
//            pDialog = new ProgressDialog(context);
//            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
//            pDialog.setCancelable(false);
//            pDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {

//            jsonStr = webService.updateInterimGateOutReportingDetails(tripId, pointId, reportingDate, reportingTime,routeOrder);
            System.out.println("json_string_output====" + jsonStr);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
            if (jsonStr != null) {
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    exceptionMsg = jsonObject.getString("expOccur");
                    if (exceptionMsg.equalsIgnoreCase("N")) {
                        Intent intent = new Intent(context, com.application.dexter_new.InterimDetailsActivity.class);
                        intent.putExtra("tripId",tripId);
                        context.startActivity(intent);
                    } else {
                        ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //context.refreshInbox();
                                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                } catch (final JSONException e) {
                    ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //context.refreshInbox();
                            Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else {
                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //context.refreshInbox();
                        Toast.makeText(context, "Could't get data from the server", Toast.LENGTH_LONG).show();
                    }
                });

            }

        }


    }



}
