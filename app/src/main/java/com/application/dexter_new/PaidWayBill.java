            package com.application.dexter_new;

            import androidx.appcompat.app.AppCompatActivity;
            import androidx.localbroadcastmanager.content.LocalBroadcastManager;

            import android.annotation.SuppressLint;
            import android.app.Dialog;
            import android.app.ProgressDialog;
            import android.content.Context;
            import android.content.Intent;
            import android.content.SharedPreferences;
            import android.os.Bundle;
            import android.text.Editable;
            import android.text.TextWatcher;
            import android.view.View;
            import android.view.Window;
            import android.widget.AdapterView;
            import android.widget.ArrayAdapter;
            import android.widget.Button;
            import android.widget.EditText;
            import android.widget.ImageView;
            import android.widget.ListView;
            import android.widget.Spinner;
            import android.widget.TextView;
            import android.widget.Toast;

            import com.google.android.material.textfield.TextInputEditText;

            import org.json.JSONArray;
            import org.json.JSONException;
            import org.json.JSONObject;

            import java.io.Serializable;
            import java.text.DateFormat;
            import java.text.DecimalFormat;
            import java.text.ParseException;
            import java.text.SimpleDateFormat;
            import java.util.ArrayList;
            import java.util.Date;
            import java.util.HashMap;
            import java.util.List;

            public class PaidWayBill extends AppCompatActivity {

                public Button addartbtn, submitbtn;
                SessionManager session;
                DateFormat f = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
                DateFormat Formatabledate = new SimpleDateFormat("dd/MM/yyyy");
                String Username, userId, DesingId;
                String waybillId, consignorName, consignorPincode, ItemSize, consigneePlace, consignorPlace;
                String ewaybill_date, ewaybill_no, ewaybill_expiry_date, invoice_date, invoice_value, vehicle_id, vehicle_no, from_gst_no, to_gst_no, from_state, Ewaybillnum,to_state;
                String consignorGst, InvoiceNum, consigneeGst, consigneeName, consigneeAddress, consigneePincode, ewaybillnum, Customername, Contractfactor;
                public String SKU_Name, Pkgtype;
                public int UomType;
                final Context context = this;
                ArrayList<ArticlesTO> articlesListDisplayed;
                ArrayList RetainDatalist = new ArrayList();
                ArrayList<WayBillScanListTo> wayBillScanListDisplayed = new ArrayList();
                public float VolumetricWeight = 0;
                public int Weight_type, WayBillType;
                public String SKU_Id, Pkg_Id;
                public TextView invoicenum, from, destinationval, customername, consignor, consignee, Grossweight, cbmtxt, actualweight, volumetricweight, volumetrictxtval;
                List<String> SKUnamelist = new ArrayList<>();
                List<String> SKUIdlist = new ArrayList<>();
                JSONArray SkuEntryList = new JSONArray();
                JSONArray Ewaybillscanlist = new JSONArray();
                boolean IsValidation = false;
                String obj,RequestID,CustID;
                articlesListadapter adapter;
                Dialog dialog;
                ListView articleslistview;
                final DecimalFormat df = new DecimalFormat("0.00");

                EditText Nostxt, Lengthtxt, Breadthtxt, Heighttxt, per_piece_weighttxt, Nospkg;


                //    List<String> SKUEntrylist = new ArrayList<>();
                List<String> pkgtypelist = new ArrayList<>();
                List<String> pkgtypeIdlist = new ArrayList<>();
                List<String> uomtypelist = new ArrayList<>();
                List<String> weighttypelist = new ArrayList<>();
                ArrayList tempartilceslist = new ArrayList<>();

                Spinner SKUspinner, pkgtypeing, uomtype, weighttype;
                private ProgressDialog pDialog;

                ArrayList<WayBillScanListTo> wayBillScanList;
//                ArrayList<WayBillScanListTo> wayBillScanListDisplayed;
                public TextInputEditText email_edtTxt1, email_edtTxt2, email_edtTxt3, email_edtTxt4, email_edtTxt5, email_edtTxt6, email_edtTxt7, email_edtTxt8;


                @SuppressLint("SetTextI18n")
                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    session = new SessionManager(getApplicationContext());
                    HashMap<String, String> user = session.getUserDetails();
                    Username = user.get(SessionManager.KEY_NAME);
                    userId = user.get(SessionManager.KEY_USERID);
                    DesingId = user.get(SessionManager.KEY_DESIG);
                    setContentView(R.layout.paid_way_bill);

                    addartbtn = findViewById(R.id.addarticle);
                    submitbtn = findViewById(R.id.submit);
                    customername = findViewById(R.id.customer_name);
                    invoicenum = findViewById(R.id.invoicenum);
                    consignor = findViewById(R.id.consignorval);
                    consignee = findViewById(R.id.consigneeval);
                    weighttype = findViewById(R.id.weighttype);
                    articleslistview = findViewById(R.id.articleslistview);
                    volumetrictxtval = findViewById(R.id.volweightvalue);
                    actualweight = findViewById(R.id.actualweight);


                    Intent intent = getIntent();
                    Customername = intent.getStringExtra("Customername");
                    Contractfactor = intent.getStringExtra("Contractfactor");
                    consignorGst = intent.getStringExtra("consignorGst");
                    InvoiceNum = intent.getStringExtra("InvoiceNum");
                    consigneeGst = intent.getStringExtra("consigneeGst");
                    consigneeName = intent.getStringExtra("consigneeName");
                    consigneeAddress = intent.getStringExtra("consigneeAddress");
                    consigneePlace = intent.getStringExtra("consigneePlace");
                    consigneePincode = intent.getStringExtra("consigneePincode");
                    ItemSize = intent.getStringExtra("ItemSize");
                    consignorPincode = intent.getStringExtra("consignorPincode");
                    consignorName = intent.getStringExtra("consignorName");
                    consignorPlace = intent.getStringExtra("consignorPlace");
                    waybillId = intent.getStringExtra("waybillId");
                    ewaybill_no = intent.getStringExtra("ewaybill_no ");
                    ewaybill_date = intent.getStringExtra("ewaybill_date ");
                    ewaybill_expiry_date = intent.getStringExtra("ewaybill_expiry_date ");
                    invoice_date = intent.getStringExtra("invoice_date ");
                    invoice_value = intent.getStringExtra("invoice_value ");
                    vehicle_no = intent.getStringExtra("vehicle_no ");
                    from_gst_no = intent.getStringExtra("from_gst_no ");
                    to_gst_no = intent.getStringExtra("to_gst_no ");
                    from_state = intent.getStringExtra("from_state ");
                    to_state = intent.getStringExtra("to_state ");
                    Ewaybillnum = intent.getStringExtra("Ewaybillnum");
                    CustID = intent.getStringExtra("CustID");
                    RequestID = intent.getStringExtra("RequestID");
                    RetainDatalist = intent.getCharSequenceArrayListExtra("RetainDatalist");
                    Bundle bundle = intent.getExtras();
                   wayBillScanListDisplayed= (ArrayList<WayBillScanListTo>)bundle.getSerializable("wayBillScanListDisplayed");

                    System.out.println("wayBillScanListDisplayed Size ==> " + wayBillScanListDisplayed.size());

                    invoicenum.setText(InvoiceNum);
                    consignor.setText(consignorName + " - " + consignorPincode + " ( " + consignorPlace + " ) ");
                    consignee.setText(consigneeName + " - " + consigneePincode + " ( " + consigneePlace + " ) ");
                    customername.setText(Customername);
                    weighttypelist.add(0, "select Weight Type");
                    weighttypelist.add(1, "Input Type");
                    weighttypelist.add(2, "CFT type");
                    uomtypelist.add(0, "select UOM Type");
                    uomtypelist.add(1, "Inches");
                    uomtypelist.add(2, "CM");
                    ArrayAdapter WeightType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, weighttypelist);

                    weighttype.setAdapter(WeightType);

                    weighttype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                            Weight_type = position;
                            //                    Toast.makeText(getApplicationContext(), "Already selected" + SKUnamelist.get(position), Toast.LENGTH_SHORT).show();

                            //                    System.out.println("Already selected" + SKUnamelist.get(position));
                            //                        if (!vehIdList.get(position).equals("0")) {
                            //                            if (!selectedVehList.contains(vehIdList.get(position))) {
                            //                                selectedVehList.add(vehIdList.get(position));
                            //                            } else {
                            //                                spinner1.setAdapter(adapter);
                            //                                Toast.makeText(getApplicationContext(), "Already selected", Toast.LENGTH_SHORT).show();
                            //                            }
                            //                        }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                            //                        tvp_dealer_edit.setText("");
                        }
                    });


                    weighttype.setSelection(1);
                    addartbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            System.out.println("Arun is Here" + actualweight.getText().toString());

                            if (actualweight.getText().toString().equals("") || actualweight.getText().toString().length() == 0) {
                                actualweight.setError("Actualweight is Required");
                            } else {

                                showDialog();
                                dialog.show();
                            }
                        }

                    });
                    submitbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            JSONObject articles = new JSONObject();


                            try {
                                String[] invoice_date1 = invoice_date.split("-");
                                invoice_date = invoice_date1[2] + "-" + invoice_date1[1] + "-" + invoice_date1[0];
                                articles.put("ewaybill_no", Ewaybillnum);
                                articles.put("invoice_no", InvoiceNum);
                                articles.put("ewaybill_date", formatdate(ewaybill_date));
                                articles.put("ewaybill_expiry_date", formatdate(ewaybill_expiry_date));
                                articles.put("invoice_date", (invoice_date));
                                articles.put("invoice_value", invoice_value);
                                articles.put("vehicle_no", vehicle_no);
                                articles.put("from_gst_no", from_gst_no);
                                articles.put("to_gst_no", to_gst_no);
                                articles.put("from_state", from_state);
                                articles.put("to_state", to_state);
                                articles.put("ewaybill_id", waybillId);
                                Ewaybillscanlist.put(articles);

                                Insertinvoice();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    });
                }

                //##########  Article Dailog code start here ############//


                private void showDialog() {
                    dialog = new Dialog(PaidWayBill.this);
                    dialog.setContentView(R.layout.add_article_popup);
                    dialog.getWindow().setBackgroundDrawableResource(R.drawable.gradiant);

                    SKUspinner = dialog.findViewById(R.id.skuname);
                    pkgtypeing = dialog.findViewById(R.id.pkgtypeing);
                    uomtype = dialog.findViewById(R.id.uomtype);

                    if(!(SKUnamelist.contains("Choose Item Name"))) {
                        SKUnamelist.add(0, "Choose Item Name");
                        SKUIdlist.add(0, "0");
                    }
                    if(!(pkgtypelist.contains("Choose Package Type"))) {
                        pkgtypelist.add(0, "Choose Package Type");
                        pkgtypeIdlist.add(0, "0");
                    }

                        ArrayAdapter SKUspinneradp = new ArrayAdapter<String>(this, R.layout.popup_dropdown_items, R.id.list_item, SKUnamelist);

                        ArrayAdapter pkgtypeingadp = new ArrayAdapter<String>(this, R.layout.popup_dropdown_items, R.id.list_item, pkgtypelist);

                        ArrayAdapter uomtypeadp = new ArrayAdapter<String>(this, R.layout.popup_dropdown_items, R.id.list_item, uomtypelist);

                        SKUspinner.setAdapter(SKUspinneradp);
                        pkgtypeing.setAdapter(pkgtypeingadp);
                        uomtype.setAdapter(uomtypeadp);


                        SKUspinner.setSelection(0);
                        uomtype.setSelection(0);
                        pkgtypeing.setSelection(0);
                        getSkunamelist();
                        getPakaginglist();
                        SKUspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                if (!SKUnamelist.get(position).equals("0")) {
                                    SKU_Name = SKUnamelist.get(position);
                                    SKU_Id = SKUIdlist.get(position);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Please select Item Name", Toast.LENGTH_SHORT).show();
                                }
                                //                    System.out.println("Already selected" + SKUnamelist.get(position));
                                //                        if (!vehIdList.get(position).equals("0")) {
                                //                            if (!selectedVehList.contains(vehIdList.get(position))) {
                                //                                selectedVehList.add(vehIdList.get(position));
                                //                            } else {
                                //                                spinner1.setAdapter(adapter);
                                //                                Toast.makeText(getApplicationContext(), "Already selected", Toast.LENGTH_SHORT).show();
                                //                            }
                                //                        }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // TODO Auto-generated method stub
//                        tvp_dealer_edit.setText("");
                            }
                        });

                        pkgtypeing.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                                Pkgtype = pkgtypelist.get(position);
                                Pkg_Id = pkgtypeIdlist.get(position);
//                    Toast.makeText(getApplicationContext(), "Already selected" + SKUnamelist.get(position), Toast.LENGTH_SHORT).show();

//                    System.out.println("Already selected" + SKUnamelist.get(position));
//                        if (!vehIdList.get(position).equals("0")) {
//                            if (!selectedVehList.contains(vehIdList.get(position))) {
//                                selectedVehList.add(vehIdList.get(position));
//                            } else {
//                                spinner1.setAdapter(adapter);
//                                Toast.makeText(getApplicationContext(), "Already selected", Toast.LENGTH_SHORT).show();
//                            }
//                        }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // TODO Auto-gene   rated method stub
//                        tvp_dealer_edit.setText("");
                            }
                        });
                        uomtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                UomType = position;
//                    Toast.makeText(getApplicationContext(), "Already selected" + SKUnamelist.get(position), Toast.LENGTH_SHORT).show();

//                    System.out.println("Already selected" + SKUnamelist.get(position));
//                        if (!vehIdList.get(position).equals("0")) {
//                            if (!selectedVehList.contains(vehIdList.get(position))) {
//                                selectedVehList.add(vehIdList.get(position));
//                            } else {
//                                spinner1.setAdapter(adapter);
//                                Toast.makeText(getApplicationContext(), "Already selected", Toast.LENGTH_SHORT).show();
//                            }
//                        }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // TODO Auto-generated method stub
//                        tvp_dealer_edit.setText("");
                            }
                        });

//            TextView ItemSizestr = dialog.findViewById(R.id.itemsize);
                        TextView invoicenum = dialog.findViewById(R.id.invoicenum);
                        Nostxt = dialog.findViewById(R.id.nos);
                        Lengthtxt = dialog.findViewById(R.id.length);
                        Breadthtxt = dialog.findViewById(R.id.breadth);
                        Heighttxt = dialog.findViewById(R.id.height);
                        per_piece_weighttxt = dialog.findViewById(R.id.perpice);
                        volumetricweight = dialog.findViewById(R.id.cbm);
                        Nospkg = dialog.findViewById(R.id.nospkg);
                        TextView Grossweight = dialog.findViewById(R.id.grossweightval);


                        invoicenum.setText(InvoiceNum);


                        per_piece_weighttxt.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                if (Nostxt.getText().length() > 0) {
                                    int value1 = Integer.parseInt(Nostxt.getText().toString());
                                    int value2 = Integer.parseInt(per_piece_weighttxt.getText().toString());
                                    String Grossweightvalue = (value1 * value2) + ".00";

                                    Grossweight.setText(Grossweightvalue);
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable editable) {

                            }
                        });
                        Lengthtxt.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                if (Lengthtxt.getText().length() > 0 && Breadthtxt.getText().length() > 0 && Heighttxt.getText().length() > 0 && Nostxt.getText().length() > 0) {

                                    float SKUnos = Integer.parseInt(Nostxt.getText().toString());
                                    float Length = 0, Breadth = 0, Height = 0;
                                    Length = Integer.parseInt(Lengthtxt.getText().toString());
                                    Breadth = Integer.parseInt(Breadthtxt.getText().toString());
                                    Height = Integer.parseInt(Heighttxt.getText().toString());
                                    int tempContractfactor = 0;
                                    tempContractfactor = Integer.parseInt(Contractfactor);
                                    if (UomType == 1) {

                                        Length = (float) (Length * 2.54);
                                        Breadth = (float) (Breadth * 2.54);
                                        Height = (float) (Height * 2.54);
                                        tempContractfactor = Integer.parseInt(Contractfactor);
                                    }
                                    System.out.println("Length = " + Length + "Breadth = " + Breadth + "Height = " + Height);
                                    String volumetricvalue = (df.format(((Length * Breadth * Height) / tempContractfactor) * SKUnos));
                                    System.out.println(volumetricvalue);
                                    volumetricweight.setText(volumetricvalue);
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable editable) {

                            }
                        });
                        Heighttxt.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                if (Nostxt.getText().length() == 0 || Nostxt.getText().equals("")) {
                                    Nostxt.setError("No.of Item is Required !");
                                    Heighttxt.getText().clear();
                                }
                                if (uomtype.getSelectedItem().toString().trim().equals("select UOM Type")) {
                                    Toast.makeText(PaidWayBill.this, "UOM Type is Required!", Toast.LENGTH_SHORT).show();
                                    Heighttxt.getText().clear();

                                }
                                if (Lengthtxt.getText().equals("") || Lengthtxt.getText().length() == 0) {
                                    Lengthtxt.setError("Length is Required !");
                                    Heighttxt.getText().clear();
                                }
                                if (Breadthtxt.getText().equals("") || Breadthtxt.getText().length() == 0) {
                                    Breadthtxt.setError("Breadth is Required !");
                                    Heighttxt.getText().clear();
                                }
                                if (Heighttxt.getText().equals("") || Heighttxt.getText().length() == 0) {
                                    Heighttxt.setError("Height Is Required");
                                    Heighttxt.getText().clear();
                                } else {
                                    float SKUnos = Integer.parseInt(Nostxt.getText().toString());
                                    float Length, Breadth, Height;
                                    Length = Integer.parseInt(Lengthtxt.getText().toString());
                                    Breadth = Integer.parseInt(Breadthtxt.getText().toString());
                                    Height = Integer.parseInt(Heighttxt.getText().toString());
                                    int tempContractfactor = 0;
                                    tempContractfactor = Integer.parseInt(Contractfactor);
                                    if (UomType == 1) {

                                        Length = (float) (Length * 2.54);
                                        Breadth = (float) (Breadth * 2.54);
                                        Height = (float) (Height * 2.54);


                                    }
                                    System.out.println("Length = " + Length + "Breadth = " + Breadth + "Height = " + Height);
                                    String volumetricvalue = (df.format(((Length * Breadth * Height) / tempContractfactor) * SKUnos));
                                    System.out.println(volumetricvalue);
                                    volumetricweight.setText(volumetricvalue);
                                }

                            }

                            @Override
                            public void afterTextChanged(Editable editable) {

                            }
                        });
                        ImageView closebtn = dialog.findViewById(R.id.btn_close);
                        Button cancelbtn = dialog.findViewById(R.id.cancel_button);
                        Button Addbtn = dialog.findViewById(R.id.addbtn);
                        closebtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        cancelbtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        Addbtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                IsValidation = CheckfieldValidation();

                                if (IsValidation) {
                                    JSONObject articles = new JSONObject();

                                    try {

                                        //articles.put("SKU_name", SKU_Name);
                                        // articles.put("Pkgtype", Pkgtype);
                                        articles.put("UomType", UomType);
                                        articles.put("weight_type", Weight_type);
                                        articles.put("nospkg", Nospkg.getText());
                                        articles.put("nossku", Nostxt.getText());
                                        articles.put("Length", Lengthtxt.getText());
                                        articles.put("Breadth", Breadthtxt.getText());
                                        articles.put("Height", Heighttxt.getText());
                                        articles.put("SaidtoContianId", Pkg_Id);
                                        articles.put("SKUNameId", SKU_Id);
                                        articles.put("volumetricweight", volumetricweight.getText().toString());
                                        articles.put("Grossweight", Grossweight.getText().toString());
                                        articles.put("per_piece_weight", "0");
                                        articles.put("WaybillMode", WayBillType);
                                        articles.put("Rate_type", "0");
                                        SkuEntryList.put(articles);


                                        InsertArticle();


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }


                            }
                        });
                    }


                    public String formatdate(String Date) {

                    String s1 = Date;
                    String s2 = null;
                    java.util.Date d;
                    try {
                        d = f.parse(s1);
                        Date = (new SimpleDateFormat("yyyy-MM-dd")).format(d);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return Date;
                }

                public boolean CheckfieldValidation() {
                    if (SKUspinner.getSelectedItem().toString().trim().equals("Choose Item Name")) {
                        Toast.makeText(PaidWayBill.this, "Item Name is Required!", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    if (Nostxt.getText().length() == 0 || Nostxt.getText().equals("")) {
                        Nostxt.setError("No.of Item is Required !");
                        return false;
                    }
                    if (pkgtypeing.getSelectedItem().toString().trim().equals("Choose Package Type")) {
                        Toast.makeText(PaidWayBill.this, "Package Type is Required!", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    if (Nospkg.getText().equals("") || Nospkg.getText().length() == 0) {
                        Nospkg.setError("No.of Packages is Required !");
                        return false;
                    }
                    if (uomtype.getSelectedItem().toString().trim().equals("select UOM Type")) {
                        Toast.makeText(PaidWayBill.this, "UOM Type is Required!", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    if (Lengthtxt.getText().equals("") || Lengthtxt.getText().length() == 0) {
                        Lengthtxt.setError("Length is Required !");
                        return false;
                    }
                    if (Breadthtxt.getText().equals("") || Breadthtxt.getText().length() == 0) {
                        Breadthtxt.setError("Breadth is Required !");
                        return false;
                    }
                    if (Heighttxt.getText().equals("") || Heighttxt.getText().length() == 0) {
                        Heighttxt.setError("Height Is Required");
                        return false;
                    }


                    return true;
                }
//                public void confirmArticles() {
//
//                    pDialog = new ProgressDialog(PaidWayBill.this);
//                    pDialog.setMessage("Connecting Server... \n Please Wait!!!");
//                    pDialog.setCancelable(false);
//                    pDialog.show();
//
//
//                    new BackgroundTask(this) {
//                        @Override
//                        public void doInBackground() {
//                            //put you background code
//                            //same like doingBackground
//                            //Background Thread
//                            String jsonStr = webService.confirmArticles(waybillId, userId);
//                            System.out.println("json_string_output====" + jsonStr);
//
//                            if (jsonStr != null) {
//                                try {
//                                    JSONArray array = new JSONArray(jsonStr);
//                                    if (array.length() > 0) {
//                                        JSONArray resultArray = new JSONArray();
//                                        JSONObject jsonObj = array.getJSONObject(0);
//                                        String exceptionMsg = jsonObj.getString("Code");
//                                        if ("1".equalsIgnoreCase(exceptionMsg)) {
//
//                                            Insertinvoice();
//                                            runOnUiThread(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    Toast.makeText(getApplicationContext(), "Article Confimed Successfully", Toast.LENGTH_LONG).show();
//
//                                                }
//                                            });
//
//                                        } else {
//                                            runOnUiThread(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
//                                                }
//                                            });
//                                        }
//
//
//                                    } else {
//                                        runOnUiThread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                Toast.makeText(getApplicationContext(), "Articles Confrm Server errror", Toast.LENGTH_LONG).show();
//                                            }
//                                        });
//
//                                    }
//
//                                } catch (final JSONException e) {
//                                    System.out.println("JSONException=========" + e);
//                                    runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
//                                        }
//                                    });
//                                }
//                            } else {
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        Toast.makeText(getApplicationContext(), "Could't connect to the server", Toast.LENGTH_LONG).show();
//                                    }
//                                });
//
//                            }
//
//
//                        }
//
//                        @Override
//                        public void onPostExecute() {
//                            if (pDialog.isShowing()) {
//                                pDialog.dismiss();
//                            }
//                        }
//                    }.execute();
//                }


                public void InsertArticle() {

                    pDialog = new ProgressDialog(PaidWayBill.this);
                    pDialog.setMessage("Connecting Server... \n Please Wait!!!");
                    pDialog.setCancelable(false);
                    pDialog.show();


                    new BackgroundTask(this) {
                        @Override
                        public void doInBackground() {
                            //put you background code
                            //same like doingBackground
                            //Background Thread
                            obj = SkuEntryList.toString();
                            String jsonStr = webService.Insertarticle(waybillId, obj, userId);
                            System.out.println("json_string_output====" + jsonStr);

                            if (jsonStr != null) {
                                try {
                                    JSONArray array = new JSONArray(jsonStr);
                                    if (array.length() > 0) {
                                        JSONArray resultArray = new JSONArray();
                                        JSONObject jsonObj = array.getJSONObject(0);
                                        String exceptionMsg = jsonObj.getString("Code");
                                        if ("1".equalsIgnoreCase(exceptionMsg)) {
                                            pDialog.dismiss();
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    dialog.dismiss();
                                                    Toast.makeText(getApplicationContext(), "Article Entered Successfully", Toast.LENGTH_LONG).show();
                                                    GetAticleslist();
                                                }
                                            });
                                        } else {
                                            pDialog.dismiss();
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {

                                                    Toast.makeText(getApplicationContext(), "Article Entry Failed Please Try Again", Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }


                                    } else {
                                        pDialog.dismiss();
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "Articles insertion Server errror", Toast.LENGTH_LONG).show();
                                            }
                                        });

                                    }

                                } catch (final JSONException e) {
                                    pDialog.dismiss();
                                    System.out.println("JSONException=========" + e);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            } else {
                                pDialog.dismiss();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                                    }
                                });

                            }


                        }

                        @Override
                        public void onPostExecute() {
                            if (pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                        }
                    }.execute();
                }

                public void getSkunamelist() {

                    //        pDialog = new ProgressDialog(PaidWayBill.this);
                    //        pDialog.setMessage("Connecting Server... \n Please Wait!!!");
                    //        pDialog.setCancelable(false);
                    //        pDialog.show();

                    String obj = null;

                    new BackgroundTask(this) {
                        @Override
                        public void doInBackground() {
                            //put you background code
                            //same like doingBackground
                            //Background Thread
                            String jsonStr = webService.getSkunamelist("26");
                            System.out.println("json_string_output====" + jsonStr);

                            if (jsonStr != null) {
                                try {
                                    JSONArray array = new JSONArray(jsonStr);
                                    if (array.length() > 0) {
                                        //                            pDialog.dismiss();
                                        for (int i = 0; i < array.length(); i++) {
                                            JSONObject jsonObj = array.getJSONObject(i);
                                            SKUnamelist.add(jsonObj.getString("SKU_Name"));
                                            SKUIdlist.add(jsonObj.getString("Article_ID"));
                                        }

                                    } else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "Manual number not Available", Toast.LENGTH_LONG).show();
                                            }
                                        });

                                    }

                                } catch (final JSONException e) {
                                    System.out.println("JSONException=========" + e);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                                    }
                                });

                            }


                        }

                        @Override
                        public void onPostExecute() {
//                                if (pDialog.isShowing()) {
//                                    pDialog.dismiss();
//                                }

                        }
                    }.execute();
                }

                public void getPakaginglist() {

                    //        pDialog = new ProgressDialog(PaidWayBill.this);
                    //        pDialog.setMessage("Connecting Server... \n Please Wait!!!");
                    //        pDialog.setCancelable(false);
                    //        pDialog.show();

                    String obj = null;

                    new BackgroundTask(this) {
                        @Override
                        public void doInBackground() {
                            //put you background code
                            //same like doingBackground
                            //Background Thread
                            String jsonStr = webService.getPakaginglist();
                            System.out.println("json_string_output====" + jsonStr);

                            if (jsonStr != null) {
                                try {
                                    JSONArray array = new JSONArray(jsonStr);
                                    if (array.length() > 0) {
                                        //                                pDialog.dismiss();

                                        for (int i = 0; i < array.length(); i++) {
                                            JSONObject jsonObj = array.getJSONObject(i);
                                            //                                System.out.println(jsonObj);
                                            pkgtypelist.add(jsonObj.getString("PKG_value"));
                                            pkgtypeIdlist.add(jsonObj.getString("Pkg_Id"));
                                        }

                                    } else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "Pakaging type not Available", Toast.LENGTH_LONG).show();
                                            }
                                        });

                                    }

                                } catch (final JSONException e) {
                                    System.out.println("JSONException=========" + e);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                                    }
                                });

                            }


                        }

                        @Override
                        public void onPostExecute() {
                            //                if (pDialog.isShowing()) {
                            //                    pDialog.dismiss();
                            //                }
                        }
                    }.execute();
                }

                public void GetAticleslist() {

//                        pDialog = new ProgressDialog(PaidWayBill.this);
//                        pDialog.setMessage("Connecting Server... \n Please Wait!!!");
//                        pDialog.setCancelable(false);
//                        pDialog.show();

                    String obj = null;
                    final String[] jsonStr = {null};

                    new BackgroundTask(PaidWayBill.this) {
                        @Override
                        public void doInBackground() {
                            //put you background code
                            //same like doingBackground
                            //Background Thread
                            jsonStr[0] = webService.getArticlesList(waybillId);
                            System.out.println("json_string_output====" + jsonStr[0]);

                        }

                        @Override
                        public void onPostExecute() {

                            ArrayList<ArticlesTO> articlesList = new ArrayList<ArticlesTO>();
                            if (jsonStr[0] != null) {
                                try {
                                    JSONArray jsonArray = new JSONArray(jsonStr[0]);
                                    if (jsonArray != null && jsonArray.length() > 0) {
                                        for (int n = 0; n < jsonArray.length(); n++) {
                                            JSONObject jsonObject = jsonArray.getJSONObject(n);
                                            ArticlesTO articles = new ArticlesTO(
                                                    jsonObject.getString("Weight_Type_Id"),
                                                    jsonObject.getString("WayBill_Article_Id"),
                                                    jsonObject.getString("WayBill_Mode"),
                                                    jsonObject.getString("PKG_Name"),
                                                    jsonObject.getString("ewaybill_id"),
                                                    jsonObject.getString("noOfSaidToContains"),
                                                    jsonObject.getString("Breadth"),
                                                    jsonObject.getString("Article_Name"),
                                                    jsonObject.getString("Uom_Type_Id"),
                                                    jsonObject.getString("NoOfArticle"),
                                                    jsonObject.getString("Length"),
                                                    jsonObject.getString("saidToContain_Id"),
                                                    jsonObject.getString("SaidTOContain"),
                                                    jsonObject.getString("Height"),
                                                    jsonObject.getString("Volumetricweight")
                                            );
                                            tempartilceslist.add(String.valueOf(articles));
                                            articlesList.add(articles);
                                        }
                                    } else {
//                                            pDialog.dismiss();
                                        runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), " No Articles Available", Toast.LENGTH_LONG).show();

                                            }
                                        });
                                    }

                                } catch (final JSONException e) {
                                    System.out.println("JSONException=========" + e);
//                                        pDialog.dismiss();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), "Invalid Articles Data", Toast.LENGTH_LONG).show();

                                        }
                                    });
                                }


                                try {
                                    String VolumetricWeightval = "0.00";
                                    if (articlesList.size() > 0) {
//                                            pDialog.hide();
                                        articleslistview.setVisibility(View.VISIBLE);
                                        submitbtn.setVisibility(View.VISIBLE);
                                        adapter = new articlesListadapter(PaidWayBill.this, articlesList);
                                        articleslistview.setAdapter(adapter);


                                        try {


                                            float tempVolumval = 0;
                                            for (int i = 0; i < articlesList.size(); i++) {
//                                                    System.out.println("looop==>"+ (articlesList.get(i).getVolumetricweight()) );
                                                tempVolumval = Float.parseFloat(articlesList.get(i).getVolumetricweight());
//                                                System.out.println("+++++++++++" + tempVolumval);
                                                VolumetricWeight = (float) (VolumetricWeight + tempVolumval);

                                                VolumetricWeightval = df.format(VolumetricWeight);
                                                volumetrictxtval.setText(VolumetricWeightval);
                                            }

                                            System.out.println("Volumetric Weight for the value of ====> " + VolumetricWeightval);


                                        } catch (Exception e) {
                                            System.out.println(e.getMessage());
                                        }

                                    }

                                } catch (Exception e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }

                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Could't get data from the server data of articls", Toast.LENGTH_LONG).show();
                                    }
                                });

                            }
                        }
                    }.execute();
                }




                public void Insertinvoice() {
                    pDialog = new ProgressDialog(PaidWayBill.this);
                    pDialog.setMessage("Connecting Server... \n Please Wait!!!");
                    pDialog.setCancelable(false);
                    pDialog.show();

                    String obj = Ewaybillscanlist.toString();

                    new BackgroundTask(this) {
                        @Override
                        public void doInBackground() {
                            //put you background code
                            //same like doingBackground
                            //Background Thread
                            String jsonStr = webService.Insertinvoice(obj, InvoiceNum, userId);
                            System.out.println("json_string_output====" + jsonStr);

                            if (jsonStr != null) {
                                try {
                                    JSONArray array = new JSONArray(jsonStr);
                                    if (array.length() > 0) {
                                        JSONArray resultArray = new JSONArray();
                                        JSONObject jsonObj = array.getJSONObject(0);
                                        String exceptionMsg = jsonObj.getString("Code");
                                        if ("1".equalsIgnoreCase(exceptionMsg)) {

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {

                                                    Toast.makeText(getApplicationContext(), "Invoice Insered Successfully", Toast.LENGTH_LONG).show();
                                                    Intent intenta = new Intent(PaidWayBill.this,WayBillScanActivity.class)
                                                            .putCharSequenceArrayListExtra("RetainDatalist",RetainDatalist)
                                                            .putExtra("Customernameintenta",Customername)
                                                            .putExtra("Ewaybillnumintenta",Ewaybillnum)
                                                            .putExtra("CustID",CustID)
                                                            .putExtra("RequstID",RequestID)
                                                            .putExtra("consigneeName",consigneeName)
                                                            .putExtra("consignorName",consignorName)
                                                            .putExtra("InvoiceNum",InvoiceNum)
                                                            .putExtra("ewabilldate",ewaybill_date)
                                                            .putExtra("ewabilldate",ewaybill_expiry_date)
                                                            .putExtra("waybillId",waybillId)
                                                            .putExtra("Actutal_Weight",actualweight.getText().toString())
                                                            .putExtra("Articles_Status" ,"1");
                                                            Bundle bundle = new Bundle();
                                                            bundle.putSerializable("wayBillScanListDisplayed", (Serializable) wayBillScanListDisplayed);
                                                            intenta.putExtras(bundle);
                                                    startActivity(intenta);

                                                }
                                            });




                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }


                                    } else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "Invoice insert Server errror", Toast.LENGTH_LONG).show();
                                            }
                                        });

                                    }

                                } catch (final JSONException e) {
                                    System.out.println("JSONException=========" + e);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Could't connect to the server", Toast.LENGTH_LONG).show();
                                    }
                                });

                            }


                        }

                        @Override
                        public void onPostExecute() {
                            if (pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                        }
                    }.execute();
                }

                @Override
                public void onBackPressed() {

                    if (tempartilceslist.size() > 0) {

                        dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.exit_alert_layout);
                        dialog.setCancelable(false);
                        Button yesButton = (Button) dialog.findViewById(R.id.yes);
                        Button noButton = (Button) dialog.findViewById(R.id.no);
                        TextView title = (TextView) dialog.findViewById(R.id.title);
                        TextView message = (TextView) dialog.findViewById(R.id.message);
                        title.setText("Go Back?");
                        message.setText("Are you sure you want to cancel the articles?");


                        yesButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                               Deletearticles();
                            }
                        });
                        noButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }else{
                        finish();
                    }


                }

                public void Deletearticles() {

                    pDialog = new ProgressDialog(PaidWayBill.this);
                    pDialog.setMessage("Connecting Server... \n Please Wait!!!");
                    pDialog.setCancelable(false);
                    pDialog.show();


                    new BackgroundTask(this) {
                        @Override
                        public void doInBackground() {
                            //put you background code
                            //same like doingBackground
                            //Background Thread
                            String jsonStr = webService.Deletearticles(waybillId);
                            System.out.println("json_string_output====" + jsonStr);

                            if (jsonStr != null) {
                                try {
                                    JSONArray array = new JSONArray(jsonStr);
                                    if (array.length() > 0) {
                                        JSONArray resultArray = new JSONArray();
                                        JSONObject jsonObj = array.getJSONObject(0);
                                        String exceptionMsg = jsonObj.getString("Code");
                                        if ("1".equalsIgnoreCase(exceptionMsg)) {

                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    GetAticleslist();
                                                    pDialog.hide();
                                                    Toast.makeText(getApplicationContext(), "Article Canceled Successfully", Toast.LENGTH_LONG).show();
                                                    finish();


                                                }
                                            });

                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    pDialog.hide();

                                                    Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }


                                    } else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pDialog.hide();
                                                Toast.makeText(getApplicationContext(), " Server errror", Toast.LENGTH_LONG).show();
                                            }
                                        });

                                    }

                                } catch (final JSONException e) {
                                    System.out.println("JSONException=========" + e);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            pDialog.hide();
                                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pDialog.hide();
                                        Toast.makeText(getApplicationContext(), "Could't connect to the server", Toast.LENGTH_LONG).show();
                                    }
                                });

                            }


                        }

                        @Override
                        public void onPostExecute() {
                            pDialog.hide();

                        }
                    }.execute();
                }


            }

