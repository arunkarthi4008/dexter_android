package com.application.dexter_new;

public class TripListTO {

    String tripCode = null;
    String tripId = null;
    String customerName = null;
    String origin = null;
    String destination = null;
    String eWayBillNumber = null;
    String lastEndKm = null;
    String tripStatus = null;
    String subStatus = null;
    String vehicle_origin_actual_reporting_date = null;
    String vehicle_origin_actual_reporting_time = null;
    String vehicle_actual_loading_date = null;
    String vehicle_actual_loading_time = null;
    String trip_actual_start_date = null;
    String trip_actual_start_time = null;
    String vehicle_destination_actual_reporting_date = null;
    String vehicle_destination_actual_reporting_time = null;
    String order_allocation = null;
    String expense_status = null;
    String complete_flag = null;
    String vehicle_actual_unloading_date = null;
    String vehicle_actual_unloading_time = null;
    String pod_file = null;



    public TripListTO( String tripId, String tripCode,String subStatus,String customerName, String origin, String destination, String eWayBillNumber,
                       String tripStatus, String lastEndKm, String vehicle_origin_actual_reporting_date,
                      String vehicle_origin_actual_reporting_time, String vehicle_actual_loading_date, String vehicle_actual_loading_time,
                      String trip_actual_start_date, String trip_actual_start_time,
                      String vehicle_destination_actual_reporting_date, String vehicle_destination_actual_reporting_time,
                       String expense_status ,String vechile_actual_unloading_date, String vehicle_actual_unloading_time, String order_allocation) {
        this.tripCode = tripCode;
        this.tripId = tripId;
        this.customerName = customerName;
        this.origin = origin;
        this.destination = destination;
        this.eWayBillNumber = eWayBillNumber;
        this.lastEndKm = lastEndKm;
        this.tripStatus = tripStatus;
        this.subStatus = subStatus;
        this.vehicle_origin_actual_reporting_date = vehicle_origin_actual_reporting_date;
        this.vehicle_origin_actual_reporting_time = vehicle_origin_actual_reporting_time;
        this.vehicle_actual_loading_date = vehicle_actual_loading_date;
        this.vehicle_actual_loading_time = vehicle_actual_loading_time;
        this.trip_actual_start_date = trip_actual_start_date;
        this.trip_actual_start_time = trip_actual_start_time;
        this.vehicle_destination_actual_reporting_date = vehicle_destination_actual_reporting_date;
        this.vehicle_destination_actual_reporting_time = vehicle_destination_actual_reporting_time;
        this.order_allocation = order_allocation;
        this.expense_status = expense_status;
        this.complete_flag = complete_flag;
        this.vehicle_actual_unloading_date = vechile_actual_unloading_date;
        this.vehicle_actual_unloading_time = vehicle_actual_unloading_time;
        this.pod_file = pod_file;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String geteWayBillNumber() {
        return eWayBillNumber;
    }

    public void seteWayBillNumber(String eWayBillNumber) {
        this.eWayBillNumber = eWayBillNumber;
    }

    public String getLastEndKm() {
        return lastEndKm;
    }

    public void setLastEndKm(String lastEndKm) {
        this.lastEndKm = lastEndKm;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(String subStatus) {
        this.subStatus = subStatus;
    }

    public String getVehicle_origin_actual_reporting_date() {
        return vehicle_origin_actual_reporting_date;
    }

    public void setVehicle_origin_actual_reporting_date(String vehicle_origin_actual_reporting_date) {
        this.vehicle_origin_actual_reporting_date = vehicle_origin_actual_reporting_date;
    }

    public String getVehicle_origin_actual_reporting_time() {
        return vehicle_origin_actual_reporting_time;
    }

    public void setVehicle_origin_actual_reporting_time(String vehicle_origin_actual_reporting_time) {
        this.vehicle_origin_actual_reporting_time = vehicle_origin_actual_reporting_time;
    }

    public String getVehicle_actual_loading_date() {
        return vehicle_actual_loading_date;
    }

    public void setVehicle_actual_loading_date(String vehicle_actual_loading_date) {
        this.vehicle_actual_loading_date = vehicle_actual_loading_date;
    }

    public String getVehicle_actual_loading_time() {
        return vehicle_actual_loading_time;
    }

    public void setVehicle_actual_loading_time(String vehicle_actual_loading_time) {
        this.vehicle_actual_loading_time = vehicle_actual_loading_time;
    }

    public String getTrip_actual_start_date() {
        return trip_actual_start_date;
    }

    public void setTrip_actual_start_date(String trip_actual_start_date) {
        this.trip_actual_start_date = trip_actual_start_date;
    }

    public String getTrip_actual_start_time() {
        return trip_actual_start_time;
    }

    public void setTrip_actual_start_time(String trip_actual_start_time) {
        this.trip_actual_start_time = trip_actual_start_time;
    }


    public String getVehicle_destination_actual_reporting_date() {
        return vehicle_destination_actual_reporting_date;
    }

    public void setVehicle_destination_actual_reporting_date(String vehicle_destination_actual_reporting_date) {
        this.vehicle_destination_actual_reporting_date = vehicle_destination_actual_reporting_date;
    }

    public String getVehicle_destination_actual_reporting_time() {
        return vehicle_destination_actual_reporting_time;
    }

    public void setVehicle_destination_actual_reporting_time(String vehicle_destination_actual_reporting_time) {
        this.vehicle_destination_actual_reporting_time = vehicle_destination_actual_reporting_time;
    }


    public String getOrder_allocation() {
        return order_allocation;
    }

    public void setOrder_allocation(String order_allocation) {
        this.order_allocation = order_allocation;
    }

    public String getExpense_status() {
        return expense_status;
    }

    public void setExpense_status(String expense_status) {
        this.expense_status = expense_status;
    }

    public String getComplete_flag() {
        return complete_flag;
    }

    public void setComplete_flag(String complete_flag) {
        this.complete_flag = complete_flag;
    }

    public String getVehicle_actual_unloading_date() {
        return vehicle_actual_unloading_date;
    }

    public void setVehicle_actual_unloading_date(String vehicle_actual_unloading_date) {
        this.vehicle_actual_unloading_date = vehicle_actual_unloading_date;
    }

    public String getVehicle_actual_unloading_time() {
        return vehicle_actual_unloading_time;
    }

    public void setVehicle_actual_unloading_time(String vehicle_actual_unloading_time) {
        this.vehicle_actual_unloading_time = vehicle_actual_unloading_time;
    }

    public String getPod_file() {
        return pod_file;
    }

    public void setPod_file(String pod_file) {
        this.pod_file = pod_file;
    }
}
