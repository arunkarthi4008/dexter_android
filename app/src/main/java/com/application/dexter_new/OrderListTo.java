package com.application.dexter_new;

public class OrderListTo {


    String customername;
    String vechiletype;
    String customerId;
    String drivername;
    String WaybillType;
    String contractfactor;
    String RequestId;
    String prerequest_code;
    
    
    public OrderListTo(String customerId, String customername, String vechiletype, String drivername, String WaybillType, String contractfactor, String RequestId, String prerequest_code){
        this.customername = customername;
        this.contractfactor = contractfactor;
        this.vechiletype = vechiletype;
        this.customerId = customerId;
        this.drivername = drivername;
        this.WaybillType = WaybillType;
        this.RequestId = RequestId;
        this.prerequest_code = prerequest_code;
    }

    public  String getWaybillType(){
        return WaybillType;
    }
    public void setWaybillType(String WaybillType){
        this.WaybillType = WaybillType;
    }
    public String getCustomerName(){
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getVechiletype() {
        return vechiletype;
    }

    public void setVechiletype(String vechiletype) {
        this.vechiletype = vechiletype;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }


    public String getContractfactor() {
        return this.contractfactor;
    }

    public  void setContractfactor(){

    }

    public void setRequestId(String requestId) {
        RequestId = requestId;
    }

    public String getRequestId() {
        return RequestId;
    }
    public String getPrerequest_code() {
        return prerequest_code;
    }

    public void setPrerequest_code(String prerequest_code) {
        this.prerequest_code = prerequest_code;
    }
}
