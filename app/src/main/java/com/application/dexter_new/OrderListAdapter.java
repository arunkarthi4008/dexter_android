package com.application.dexter_new;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
//import android.support.wearable.activity.WearableActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class OrderListAdapter extends BaseAdapter implements Filterable {

    Context context;
    ArrayList<OrderListTo> orderList;
    ArrayList<OrderListTo> orderListDisplayed;
    TextView drivername,vechiletype,customername,preRequestcode;
    Button viewdetailbtn;
    int itPosition = 0;
    String customerId;

    public OrderListAdapter(Context context, ArrayList<OrderListTo> orderList) {

        this.context = context;
        this.orderList = orderList;
        this.orderListDisplayed = orderList;
    }

    @Override
    public int getCount() {
        return orderListDisplayed.size();
    }

    @Override
    public Object getItem(int i) {
        return orderListDisplayed.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        convertView = null;


        convertView = mInflater.inflate(R.layout.order_list_adapter, null);


        drivername = (TextView) convertView.findViewById(R.id.drivername);
        customername = (TextView) convertView.findViewById(R.id.customername);
        vechiletype = (TextView) convertView.findViewById(R.id.vechiletype);
        viewdetailbtn = (Button) convertView.findViewById(R.id.viewdetailbtn);
        preRequestcode = (TextView) convertView.findViewById(R.id.preRequestcode);

        viewdetailbtn.setTag(position);

        OrderListTo orderlist = orderListDisplayed.get(position);
        customername.setText(orderlist.getCustomerName());
        vechiletype.setText(orderlist.getVechiletype());
        drivername.setText(orderlist.getDrivername());
        preRequestcode.setText(orderlist.getPrerequest_code());




            viewdetailbtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int pos = (Integer) v.getTag();
                    OrderListTo OrderClicked = orderListDisplayed.get(pos);
                    itPosition = pos;
                    customerId = OrderClicked.getCustomerId();
                    Intent intent = new Intent(context, WayBillScanActivity.class);
                    intent.putExtra("customerId", customerId);
                    intent.putExtra("customername",orderlist.getCustomerName());
                    intent.putExtra("Contractfactor",orderlist.getContractfactor());
                    intent.putExtra("RequestId",orderlist.getRequestId());

                    context.startActivity(intent);


                }
            });
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                orderListDisplayed = (ArrayList<OrderListTo>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<RunSheetTO> FilteredArrList = new ArrayList<RunSheetTO>();

                if (orderList == null) {
                    orderList = new ArrayList<OrderListTo>(orderListDisplayed); // saves the original data in mOriginalValues
                }
                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns F
                 *  ilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = orderList.size();
                    results.values = orderList;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < orderList.size(); i++) {
                        String data = orderList.get(i).getCustomerName();
                        if (data.toLowerCase().contains(constraint.toString())) {
                            FilteredArrList.add(new RunSheetTO(orderList.get(i).getCustomerId(), orderList.get(i).getCustomerName(),
                                    orderList.get(i).getDrivername()));
                        }
                    }
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }
}