package com.application.dexter_new;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class RunSheetListAdapter extends BaseAdapter implements Filterable {

    Context context;
    ArrayList<RunSheetTO> runSheetList;
    ArrayList<RunSheetTO> runSheetListDisplayed;
    TextView runsheet_number, customer_name, address, mobile_num, status;
    Button startDelivery, un_deliver;
    LinearLayout map;
    int itPosition = 0;
    String runsheetId;

    public RunSheetListAdapter(Context context, ArrayList<RunSheetTO> runSheetList) {
        this.context = context;
        this.runSheetList = runSheetList;
        this.runSheetListDisplayed = runSheetList;
    }

    @Override
    public int getCount() {
        return runSheetListDisplayed.size();
    }

    @Override
    public Object getItem(int i) {
        return runSheetListDisplayed.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        convertView = null;


        convertView = mInflater.inflate(R.layout.runsheet_list_adapter, null);

        runsheet_number = (TextView) convertView.findViewById(R.id.order_id);
        mobile_num = (TextView) convertView.findViewById(R.id.mobile_num);
        startDelivery = (Button) convertView.findViewById(R.id.buttonView);

        startDelivery.setTag(position);

        RunSheetTO runSheet = runSheetListDisplayed.get(position);
        runsheet_number.setText(runSheet.getRunSheetNumber());
        mobile_num.setText(runSheet.getDriverPhone());

        startDelivery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int pos = (Integer) v.getTag();
                RunSheetTO OrderClicked = runSheetListDisplayed.get(pos);
                itPosition = pos;
                runsheetId = OrderClicked.getRunsheetId();
                Intent intent = new Intent(context, WaybillListActivity.class);
                intent.putExtra("runsheetId", runsheetId);
                context.startActivity(intent);

//                    Intent intent = new Intent(context, DeliverOrder.class);
//                    intent.putExtra("order_id", orderId);
//                    intent.putExtra("customer_name", customerName);
//                    intent.putExtra("address_1", address1);
//                    intent.putExtra("address_2", address2);
//                    intent.putExtra("landmark", landMark);
//                    intent.putExtra("district", District);
//                    intent.putExtra("state", State);
//                    intent.putExtra("product", product);
//                    intent.putExtra("un_num_1", unNumber1);
//                    intent.putExtra("un_num_2", unNumber2);
//                    intent.putExtra("loan_id", loanId);
//                    intent.putExtra("vendor_id", vendorId);
//                    intent.putExtra("resend_status", resend_status);


            }
        });
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                runSheetListDisplayed = (ArrayList<RunSheetTO>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<RunSheetTO> FilteredArrList = new ArrayList<RunSheetTO>();

                if (runSheetList == null) {
                    runSheetList = new ArrayList<RunSheetTO>(runSheetListDisplayed); // saves the original data in mOriginalValues
                }
                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = runSheetList.size();
                    results.values = runSheetList;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < runSheetList.size(); i++) {
                        String data = runSheetList.get(i).getRunSheetNumber();
                        if (data.toLowerCase().contains(constraint.toString())) {
                            FilteredArrList.add(new RunSheetTO(runSheetList.get(i).getRunsheetId(), runSheetList.get(i).getRunSheetNumber(),
                                    runSheetList.get(i).getDriverPhone()));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

}
