package com.application.dexter_new;

import android.os.Parcel;
import android.os.Parcelable;

public class WayBillScanListTo implements Parcelable {

    String  ewayBillUpdatedDate,invoiceDate,consignorGst,consignorName,
            WaybillNum,consignorPlace,consignorPincode,InvoiceVal,consignorAddress,
            InvoiceNum, consigneeGst,consigneeName,consigneeAddress,
            wayBillDate,ewayBillExpiry,consigneePlace,consigneePincode,vehicleNo,ItemSize,waybillId
            ,consigneeMobilenum,GrossWeight,ConsigneeEmailId;


    public WayBillScanListTo(String itemSize,String invoiceNo,String invoiceDate,String invoiceValue,String ewayBillDate,String ewayBillExpiry,String ewayBillUpdatedDate,String consignorGst,String consignorName,String consignorAddress,String consignorPlace,String consignorPincode,String consigneeGst,String consigneeName,String consigneeAddress,String consigneePlace,String consigneePincode,String ewayBillId,String vehicleNo,String waybillNum){
        this.ItemSize = itemSize;
        this.ewayBillUpdatedDate = ewayBillUpdatedDate;
        this.invoiceDate = invoiceDate;
        this.consignorGst = consignorGst;
        this.consignorName = consignorName;
        this.WaybillNum = waybillNum;
        this.consignorPlace = consignorPlace;
        this.consignorPincode = consignorPincode;
        this.consignorAddress = consignorAddress;
        this.InvoiceVal = invoiceValue;
        this.InvoiceNum = invoiceNo;
        this.consigneeGst = consigneeGst;
        this.consigneeName = consigneeName;
        this.consigneeAddress = consigneeAddress;
        this.wayBillDate = ewayBillDate;
        this.ewayBillExpiry = ewayBillExpiry;
        this.consigneePlace = consigneePlace;
        this.consigneePincode = consigneePincode;
        this.waybillId = ewayBillId;
        this.vehicleNo = vehicleNo;
        this.consigneeMobilenum = consigneeMobilenum;
        this.ConsigneeEmailId = ConsigneeEmailId;
        this.GrossWeight = GrossWeight;


    }

    protected WayBillScanListTo(Parcel in) {
        ewayBillUpdatedDate = in.readString();
        invoiceDate = in.readString();
        consignorGst = in.readString();
        consignorName = in.readString();
        WaybillNum = in.readString();
        consignorPlace = in.readString();
        consignorPincode = in.readString();
        InvoiceVal = in.readString();
        consignorAddress = in.readString();
        InvoiceNum = in.readString();
        consigneeGst = in.readString();
        consigneeName = in.readString();
        consigneeAddress = in.readString();
        wayBillDate = in.readString();
        ewayBillExpiry = in.readString();
        consigneePlace = in.readString();
        consigneePincode = in.readString();
        vehicleNo = in.readString();
        ItemSize = in.readString();
        waybillId = in.readString();
        consigneeMobilenum = in.readString();
        GrossWeight = in.readString();
        ConsigneeEmailId = in.readString();
    }

    public static final Creator<WayBillScanListTo> CREATOR = new Creator<WayBillScanListTo>() {
        @Override
        public WayBillScanListTo createFromParcel(Parcel in) {
            return new WayBillScanListTo(in);
        }

        @Override
        public WayBillScanListTo[] newArray(int size) {
            return new WayBillScanListTo[size];
        }
    };

    public String getConsignorGst() {
        return consignorGst;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public String getEwayBillUpdatedDate() {
        return ewayBillUpdatedDate;
    }

    public String getConsignorPincode() {
        return consignorPincode;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public String getConsignorPlace() {
        return consignorPlace;
    }

    public String getWaybillId() {
        return waybillId;
    }

    public String getInvoiceVal() {
        return InvoiceVal;
    }

    public String getWaybillNum() {
        return WaybillNum;
    }

    public String getItemSize() {
        return ItemSize;
    }

    public void setItemSize(String itemSize) {
        ItemSize = itemSize;
    }

    public void setConsignorGst(String consignorGst) {
        this.consignorGst = consignorGst;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public void setConsignorPlace(String consignorPlace) {
        this.consignorPlace = consignorPlace;
    }

    public void setEwayBillUpdatedDate(String ewayBillUpdatedDate) {
        this.ewayBillUpdatedDate = ewayBillUpdatedDate;
    }

    public void setConsignorPincode(String consignorPincode) {
        this.consignorPincode = consignorPincode;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public void setWaybillId(String WaybillId) {
        waybillId = WaybillId;
    }

    public void setWaybillNum(String waybillNum) {
        WaybillNum = waybillNum;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public void setInvoiceVal(String invoiceVal) {
        InvoiceVal = invoiceVal;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public String getConsigneeGst() {
        return consigneeGst;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public String getConsigneePincode() {
        return consigneePincode;
    }

    public String getConsigneePlace() {
        return consigneePlace;
    }

    public String getInvoiceNum() {
        return InvoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        InvoiceNum = invoiceNum;
    }

    public String getEwayBillExpiry() {
        return ewayBillExpiry;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public String getWayBillDate() {
        return wayBillDate;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public void setConsigneeGst(String consigneeGst) {
        this.consigneeGst = consigneeGst;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public void setConsigneePincode(String consigneePincode) {
        this.consigneePincode = consigneePincode;
    }

    public void setConsigneePlace(String consigneePlace) {
        this.consigneePlace = consigneePlace;
    }

    public void setEwayBillExpiry(String ewayBillExpiry) {
        this.ewayBillExpiry = ewayBillExpiry;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public void setWayBillDate(String wayBillDate) {
        this.wayBillDate = wayBillDate;
    }

    public String getConsigneeMobile() {
        return consigneeMobilenum;
    }
    public void setConsigneeMobilenum(String consigneeMobilenum){
        this.consigneeMobilenum = consigneeMobilenum;
    }

    public String getConsigneeEmailId() {
        return ConsigneeEmailId;
    }
    public void setConsigneeEmailId(String ConsigneeEmailId){
        this.ConsigneeEmailId = ConsigneeEmailId;
    }
    public String getGrossWeight() {
        return GrossWeight;
    }
    public void setGrossWeight(String GrossWeight){
        this.GrossWeight = GrossWeight;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ewayBillUpdatedDate);
        parcel.writeString(invoiceDate);
        parcel.writeString(consignorGst);
        parcel.writeString(consignorName);
        parcel.writeString(WaybillNum);
        parcel.writeString(consignorPlace);
        parcel.writeString(consignorPincode);
        parcel.writeString(InvoiceVal);
        parcel.writeString(consignorAddress);
        parcel.writeString(InvoiceNum);
        parcel.writeString(consigneeGst);
        parcel.writeString(consigneeName);
        parcel.writeString(consigneeAddress);
        parcel.writeString(wayBillDate);
        parcel.writeString(ewayBillExpiry);
        parcel.writeString(consigneePlace);
        parcel.writeString(consigneePincode);
        parcel.writeString(vehicleNo);
        parcel.writeString(ItemSize);
        parcel.writeString(waybillId);
        parcel.writeString(consigneeMobilenum);
        parcel.writeString(GrossWeight);
        parcel.writeString(ConsigneeEmailId);
    }
}
