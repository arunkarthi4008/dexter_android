package com.application.dexter_new;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CheckLoginActivity extends AppCompatActivity {

    SessionManager session;
    ProgressBar ProgeressBar;
    TextInputEditText userNameET, passWordET;
    TextView statusTV;
    TextInputLayout userTI, passTI;
    Button loginButton;

    SharedPreferences sharedpreferences;
    String editTextPassword, editTextUsername;
    String exceptionMsg, userId, desigId, branchId;
//    String total, scheduled, pending;
    public static final String MyPREFERENCES = "MyPrefs";
    private ProgressDialog pDialog;
    String jsonStr = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        session = new SessionManager(getApplicationContext());
        ProgeressBar = (ProgressBar) findViewById(R.id.progressBar);
        userNameET = (TextInputEditText) findViewById(R.id.editText1);
        passWordET = (TextInputEditText) findViewById(R.id.editText2);
        statusTV = (TextView) findViewById(R.id.tv_result);
        loginButton = (Button) findViewById(R.id.login);
        userTI = findViewById(R.id.userNameInput);
        passTI = findViewById(R.id.passwordInput);

        if (session.getState() == true) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        userNameET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
//                    System.out.println("iam here");
                    if (userNameET.getText().length() == 0 && userNameET.getText().toString().equals("")) {
                        System.out.println("iam here 1");
                        userTI.setErrorEnabled(true);
                        userTI.setError("error");


                    } else {
                        userTI.setError(null);
                        userTI.setErrorEnabled(false);
                    }

//                    saveThisItem(txtClientID.getText().toString(), "name", txtName.getText().toString());
                }
            }
        });



        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Check if text controls are not empty

                if (userNameET.getText().length() != 0 && userNameET.getText().toString() != "") {
                    if (passWordET.getText().length() != 0 && passWordET.getText().toString() != "") {
                        editTextUsername = userNameET.getText().toString();
                        editTextPassword = passWordET.getText().toString();
                        statusTV.setText("");

//                		session set value
                        SharedPreferences.Editor editor = sharedpreferences.edit();

                        String n = editTextUsername;
                        String pw = editTextPassword;
                        editor.putString("Name", n);
                        editor.putString("pass", pw);
                        editor.commit();
                        verifyUser();
//                        //Create instance for AsyncCallWS
//                        AsyncCallWS task = new AsyncCallWS();
//                        //Call execute
//                        task.execute();
//                        new verifyUser().execute();
                    }
                    //If Password text control is empty
                    else {
                        statusTV.setText("Please enter Password");
                    }
                    //If Username text control is empty
                } else {
//                    userTI.setErrorEnabled(true);
//                    userTI.setError("You need to enter a name");
                    statusTV.setText("Please enter Username");
                }
            }
        });

    }




    public void verifyUser() {

//        pDialog = new ProgressDialog(CheckLoginActivity.this);
//        pDialog.setMessage("Connecting Server... \n Please Wait!!!");
//        pDialog.setCancelable(false);
//        pDialog.show();
        ProgeressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        String obj = null;

        new BackgroundTask(CheckLoginActivity.this) {
            @Override
            public void doInBackground() {
                //put you background code
                //same like doingBackground
                //Background Thread
                jsonStr = webService.verifyUser(editTextUsername, editTextPassword);
                System.out.println("json_string_output====" + jsonStr);
            }
            @Override
            public void onPostExecute() {
//                if (pDialog.isShowing()) {
//                    pDialog.dismiss();
//                }
                ProgeressBar.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                //hear is result part same
                //same like post execute
                //UI Thread(update your UI widget)

                if (jsonStr != null) {
                    try {
                        JSONArray jsonArray = new JSONArray(jsonStr);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        exceptionMsg = jsonObject.getString("status");
                        System.out.println("status===" + exceptionMsg);
                        if ("Success".equalsIgnoreCase(exceptionMsg)) {
                            userId = jsonObject.getString("UserId");
                            desigId = jsonObject.getString("DesigId");
                            branchId = jsonObject.getString("BranchId");

                            System.out.println("BarnchId Value form Url == " + branchId);

                            session.createLoginSession(editTextUsername, editTextPassword, desigId, userId,branchId);
                            Intent intObj = new Intent(CheckLoginActivity.this, MainActivity.class);
                            startActivity(intObj);

                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), exceptionMsg, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    } catch (final JSONException e) {
                        System.out.println("JSONException========="+ e);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        }.execute();
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);
        finishAffinity();
    }


}
