package com.application.dexter_new;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class UploadDocument extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    String Username, userId, DesingId;
    String tripId;
    SessionManager session;
    Button capturePod, submit;
    GridView gridView;
    String exceptionMsg = null;
    DrawerLayout drawer;
    private ProgressDialog pDialog;
    Dialog dialog;
    final Context context = this;
    int MY_CAMERA_PERMISSION_CODE = 100;
    int CAMERA_REQUEST = 1888;
    ArrayList imageString = new ArrayList();
    int count = 0;
    ArrayList<ColorItem> CamraTakeImgList = new ArrayList<>();
    byte[] CustomerSignature = null;
    String billImage = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        System.out.println("username in upload doc==" + Username + " userId in upload doc==" + userId + " DesigId in upload doc==" + DesingId);

        Intent intent = getIntent();
        tripId = intent.getStringExtra("tripId");


        setContentView(R.layout.activity_upload_document);


        //side navigation code
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        final Button openNav = findViewById(R.id.openNav);
        View headerView = navigationView.getHeaderView(0);
        TextView navName = headerView.findViewById(R.id.userName);
        navName.setText(Username);
        TextView navReName = headerView.findViewById(R.id.navReName);
        navReName.setText("Pilot");
        Switch aswitch = headerView.findViewById(R.id.aswitch);

        if (session.getState() == true) {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        if (session.getState() == true) {
            aswitch.setChecked(true);
        }
        aswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
                if (ischecked == true) {
                    session.setState(true);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                } else {
                    session.setState(false);
                    getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
            }
        });

        Button closeNav = headerView.findViewById(R.id.closeNav);
        Menu nav_Menu = navigationView.getMenu();
        MenuItem nav_dashboard = nav_Menu.findItem(R.id.nav_dashboard);
        MenuItem view_orders = nav_Menu.findItem(R.id.view_orders);
        nav_dashboard.setVisible(false);
        view_orders.setVisible(false);
        openNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        System.out.println("opening nav bar");
                        openDrawer();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(openNav.getWindowToken(), 0);
                    }
                });

        closeNav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        closeDrawer(null);
                    }

                });
//side navigation code


        capturePod = findViewById(R.id.button1);
        gridView = (GridView) findViewById(R.id.gridView);
        submit = findViewById(R.id.submit);


        capturePod.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                } else {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, 1);
                }
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageString.size()>0){
                new submitDocumentUpload().execute();
                }else{
                    Toast.makeText(UploadDocument.this, "Please Capture atleast One Image", Toast.LENGTH_LONG).show();
                }

            }
        });


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.
        closeDrawer(null);
        int id = menuItem.getItemId();
        if (id == R.id.logout) {
            session.logoutUser();
        }
//        if (id == R.id.view_orders) {
//            if (DesingId.equals("1042")) {
//                Intent mainIntent = new Intent(FuelFilling.this, MainActivity.class);
//                startActivity(mainIntent);
//            }
////            if (DesingId.equals("1059")) {
////
////            }
//        }
//        if (id == R.id.nav_dashboard) {
//            Intent mainIntent = new Intent(FuelFilling.this, DeliveryRunSheetListActivity.class);
//            startActivity(mainIntent);
//        }
        return true;
    }

    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            System.out.println("iam here");
            try {
                count++;
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                CustomerSignature = bos.toByteArray();
                billImage = Base64.encodeToString(CustomerSignature, Base64.DEFAULT);
                imageString.add(billImage);
                ColorItem select = new ColorItem();
                select.setBitmap(bitmap);
                select.setColorName("Pic" + " " + count);
                CamraTakeImgList.add(select);
                SelectedImageAdapter seletimg = new SelectedImageAdapter(UploadDocument.this, CamraTakeImgList);
                gridView.setAdapter(seletimg);
            } catch (NullPointerException e) {
                System.out.println("exec===" + e);
                e.printStackTrace();

            }

        }
    }



    public class submitDocumentUpload extends AsyncTask<String, String, String> {
        String updateBunkDetails;
        JSONObject jResult = new JSONObject();
        JSONArray jArray = new JSONArray();
        JSONArray main = new JSONArray();
        String obj = null;

        @Override
        //Make Progress Bar visible
        protected void onPreExecute() {
            pDialog = new ProgressDialog(UploadDocument.this);
            pDialog.setMessage("Connecting Server..\nPlease Wait!!!");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                for (int i = 0; i < imageString.size(); i++) {
                    JSONObject jGroup = new JSONObject();// /sub Object
                    jGroup.put("image", imageString.get(i));
                    jArray.put(jGroup);
                }
                jResult.put("podImage", jArray);
                main.put(jResult);
                obj = main.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            updateBunkDetails = webService.updateDocument(tripId, obj,userId);
            System.out.println("json_string_output====" + updateBunkDetails);

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            Intent intObj = new Intent(UploadDocument.this, TripListActivity.class);
            startActivity(intObj);
        }
    }







}
