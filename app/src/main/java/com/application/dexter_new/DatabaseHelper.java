package com.application.dexter_new;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DatabaseHelper extends SQLiteOpenHelper {


    private static DatabaseHelper databaseHelper;
    SQLiteDatabase sqliteDb;
    private AtomicInteger atomicInteger = new AtomicInteger();

    public DatabaseHelper(Context context) {
        super(context, "DEXTER.db", null, 1);
    }


    public static DatabaseHelper createInstance(Context context) {
        if (databaseHelper == null) {
            databaseHelper = new DatabaseHelper(context);
        }
        return databaseHelper;
    }

    public void openDB() {
        if (this.atomicInteger.incrementAndGet() == 1) {
            this.sqliteDb = getWritableDatabase();

        }
    }

    public void closeDB() {
        if (this.atomicInteger.decrementAndGet() == 0) {
            this.sqliteDb.close();
        }
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println("iam in creating db");
        db.execSQL("create table  user_table  (id integer primary key autoincrement,userId text, userName text)");
        db.execSQL("create table  runsheet_list (id integer primary key autoincrement,run_sheet_id text, run_sheet_number text,driver_mobile text)");
        db.execSQL("create table  waybill_list (id integer primary key autoincrement,waybill_id text,waybill_no text,consignee_name text,consignee_id text" +
                ", consignee_address text,consignee_phone text,consignee_pincode text, atricle_code text,article_name text,noofarticle text,articleweight text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        System.out.println("iam in upgrading db");
        db.execSQL("DROP TABLE IF EXISTS user_table");
        db.execSQL("DROP TABLE IF EXISTS waybill_list");
        db.execSQL("DROP TABLE IF EXISTS waybill_list");
        onCreate(db);
    }


    public boolean clearRunSheetList() {
        SQLiteDatabase myDB = this.getWritableDatabase();
//        myDB.delete("runsheet_list", null,null);
        myDB.execSQL("DELETE from runsheet_list");
        myDB.execSQL("update sqlite_sequence set seq=1 where name='runsheet_list'");
        return true;
    }

    public Boolean insertRunSheetLists(ArrayList<RunSheetTO> waybillList) {
        boolean result = false;
        int insertStatus = 0;
//        int listSize = waybillList.size();
//        int Counter = 0;
        Iterator it = waybillList.iterator();
        while (it.hasNext()) {
            insertStatus = insertRunSheetList((RunSheetTO) it.next());
//            Counter++;
        }
//        if (listSize == Counter) {
//            result = true;
//        }
        if(insertStatus >0){
            result = true;
        }
        return result;
    }

    public int insertRunSheetList(RunSheetTO waybillList) {
        int insert = 0;
        openDB();
        ContentValues contentValues = new ContentValues();
        contentValues.put("run_sheet_id", waybillList.getRunsheetId());
        contentValues.put("run_sheet_number", waybillList.getRunSheetNumber());
        contentValues.put("driver_mobile", waybillList.getDriverPhone());
        insert = (int) this.sqliteDb.insert("runsheet_list", null, contentValues);
        closeDB();
        return  insert;
    }

    public List getRunsheetList() {
        ArrayList<RunSheetTO> arrayList = new ArrayList<RunSheetTO>();
        Cursor rawQuery = this.sqliteDb.rawQuery("select run_sheet_id,run_sheet_number,driver_mobile from runsheet_list", null);
        if (rawQuery != null && rawQuery.getCount() > 0) {
            while (rawQuery.moveToNext()) {
                RunSheetTO runSheetDetails = new RunSheetTO(
                        rawQuery.getString(rawQuery.getColumnIndex("run_sheet_id")),
                        rawQuery.getString(rawQuery.getColumnIndex("run_sheet_number")),
                        rawQuery.getString(rawQuery.getColumnIndex("driver_mobile"))
                );
                arrayList.add(runSheetDetails);
            }
            rawQuery.close();
        }
        return arrayList;
    }


}
