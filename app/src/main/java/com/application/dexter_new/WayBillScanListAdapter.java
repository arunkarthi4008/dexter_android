package com.application.dexter_new;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.media.CamcorderProfile.get;

public class WayBillScanListAdapter  extends BaseAdapter implements Filterable {
    Context context;
    ArrayList<WayBillScanListTo>wayBillScanList;
    ArrayList<WayBillScanListTo>wayBillScanListDisplayed;
    TextView waybilldate,expirydate,waybillnum,invoicenum,invoicevalue,article_staus;
    String waybillId,consignorName,consignorPincode,ItemSize,consigneePlace,consignorPlace ;
    String ewaybill_date,ewaybill_no, ewaybill_expiry_date, invoice_date, invoice_value, vehicle_id, vehicle_no, from_gst_no, to_gst_no, from_state, to_state;
    String consignorGst,InvoiceNum,consigneeGst,consigneeName,consigneeAddress,consigneePincode,ewaybillnum;
    Button editbtn,removebtn,waybillcreatebtn;
    int WayBillType;
    Context WayBillScanActivity1;
    ArrayList RetainDatalist = new ArrayList();
    String Customername,Contractfactor,Article_status,Ewaybillnum,CustID,RequestID;

    public WayBillScanListAdapter(Context context, ArrayList<WayBillScanListTo> wayBillScanList, String ewaybillnum ,String Customername,String Contractfactor,int WaybillType,String Article_status, ArrayList RetainDatalist,String CustID,String RequestID) {

        this.context = context;
        this.wayBillScanList = wayBillScanList;
        this.wayBillScanListDisplayed = wayBillScanList;
        this.ewaybillnum = ewaybillnum;
        this.Customername = Customername;
        this.Contractfactor = Contractfactor;
        this.WayBillType = WaybillType;
        this.Article_status = Article_status;
        this.RetainDatalist = RetainDatalist;
        this.CustID = CustID;
        this.RequestID = RequestID;

    }


    @Override
    public int getCount() {
        return wayBillScanListDisplayed.size();
    }

    @Override
    public Object getItem(int position) {
        return get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        convertView = null;


        convertView = mInflater.inflate(R.layout.way_bill_scan_list_adapter, null);
//
//                waybilldate= convertView.findViewById(R.id.waybilldate);
//                expirydate= convertView.findViewById(R.id.expirydate);
                waybillnum= convertView.findViewById(R.id.waybillnum);
                invoicenum= convertView.findViewById(R.id.invoicenum);
                invoicevalue= convertView.findViewById(R.id.invoicevalue);
//                article_staus = convertView.findViewById(R.id.article_staus);
                waybillcreatebtn = convertView.findViewById(R.id.waybillcreatebtn);

                RetainDatalist = this.RetainDatalist;





        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("ppppppppppppppppp000"+wayBillScanListDisplayed.size());
        WayBillScanListTo wayBillScanList = wayBillScanListDisplayed.get(position);
        String waybilldate1 = dateFormat.format(date);
        String waybilldate2 = dateFormat.format(date);

                waybillnum.setText(wayBillScanList.getWaybillNum());
                Ewaybillnum = wayBillScanList.getWaybillNum();
                invoicenum.setText(wayBillScanList.getInvoiceNum());
                invoicevalue.setText(wayBillScanList.getInvoiceVal());

                waybillcreatebtn.setTag(position);
//                article_staus.setTag(position);

                System.out.println("this.Article_status " + this.Article_status );

//                if(this.Article_status.equals("1") || this.Article_status.equals("2")){
//                    if(this.Ewaybillnum == wayBillScanList.getWaybillNum()) {
//                        waybillcreatebtn.setVisibility(View.GONE);
//                        article_staus.setVisibility(View.VISIBLE);
//                    }
//                    else{
//                        waybillcreatebtn.setVisibility(View.VISIBLE);
//                        article_staus.setVisibility(View.GONE);
//                    }
//                }else{
//                    waybillcreatebtn.setVisibility(View.VISIBLE);
//                    article_staus.setVisibility(View.GONE);
//                }


                waybillId= wayBillScanList.getWaybillId();
                consignorGst = wayBillScanList.getConsigneeGst();
                InvoiceNum =wayBillScanList.getInvoiceNum();
                consigneeGst =wayBillScanList.getConsigneeGst();
                consigneeName =wayBillScanList.getConsigneeName();
                consigneeAddress =wayBillScanList.getConsigneeAddress();
                consigneePincode =wayBillScanList.getConsigneePincode();
                consigneePlace = wayBillScanList.getConsigneePlace();
                consignorName = wayBillScanList.getConsignorName();
                consignorPincode = wayBillScanList.getConsignorPincode();
                ItemSize = wayBillScanList.getItemSize();
                consignorPlace = wayBillScanList.getConsignorPlace();
                ewaybill_no = wayBillScanList.getWaybillNum();
                ewaybill_date = wayBillScanList.getWayBillDate();
                ewaybill_expiry_date = wayBillScanList.getEwayBillExpiry();
                invoice_date = wayBillScanList.getInvoiceDate();
                invoice_value = wayBillScanList.getInvoiceVal();
                vehicle_no = wayBillScanList.getVehicleNo();
                from_gst_no = wayBillScanList.getConsigneeGst();
                to_gst_no =wayBillScanList.getConsignorGst();
                from_state = wayBillScanList.getConsigneePlace();
                to_state = wayBillScanList.getConsignorPlace();

//                editbtn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v)  {
////                        showDialog();
//                    }
//                });

//                removebtn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v)  {
////                        showDialog();
//                    }
//                });

//
                waybillcreatebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)  {
                        Intent intent = new Intent(context,PaidWayBill.class);
                            intent.putExtra("Customername",Customername);
                            intent.putExtra("Contractfactor",Contractfactor);
                            intent.putExtra("WayBillType",WayBillType);
                            intent.putExtra("waybillId",waybillId );
                            intent.putExtra("consignorGst",consignorGst );
                            intent.putExtra("InvoiceNum",InvoiceNum );
                            intent.putExtra("consigneeGst",consigneeGst );
                            intent.putExtra("consigneeName",consigneeName );
                            intent.putExtra("consigneeAddress",consigneeAddress );
                            intent.putExtra("consigneePlace",consigneePlace );
                            intent.putExtra("consigneePincode",consigneePincode );
                            intent.putExtra("ItemSize",ItemSize );
                            intent.putExtra("consignorPincode",consignorPincode );
                            intent.putExtra("consignorName",consignorName );
                            intent.putExtra("consignorPlace",consignorPlace );
                            intent.putExtra("ewaybill_no ",ewaybill_no);
                            intent.putExtra("ewaybill_date ",ewaybill_date);
                            intent.putExtra("ewaybill_expiry_date ",ewaybill_expiry_date);
                            intent.putExtra("invoice_date ",invoice_date);
                            intent.putExtra("invoice_value ",invoice_value);
                            intent.putExtra("vehicle_no ",vehicle_no);
                            intent.putExtra("from_gst_no ",from_gst_no);
                            intent.putExtra("to_gst_no ",to_gst_no);
                            intent.putExtra("from_state ",from_state);
                            intent.putExtra("to_state ",to_state);
                            intent.putExtra("Ewaybillnum",Ewaybillnum);
                            intent.putExtra("CustID",CustID);
                            intent.putExtra("RequestID",RequestID);
                            intent.putCharSequenceArrayListExtra("RetainDatalist" ,RetainDatalist);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("wayBillScanListDisplayed", (Serializable) wayBillScanListDisplayed);
                            intent.putExtras(bundle);
                            context.startActivity(intent);



                    }
                });
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return null;
    }
}