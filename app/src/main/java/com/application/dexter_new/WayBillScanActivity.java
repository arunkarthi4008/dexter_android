package com.application.dexter_new;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class WayBillScanActivity extends AppCompatActivity {

    List<String> Temparraylist = new ArrayList<>();
    String Username, userId, DesingId, branchId;
    String  consignorName,InvoiceNum, consigneeName,RequestID,CustID,Customername,Contractfactor,WaybillIdintent;
//    TextView statusTV;
    String ewaybill_date, ewaybill_expiry_date, invoice_date;
    ProgressDialog pDialog;
    Dialog dialog;
    DateFormat f = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
    DateFormat Formatabledate = new SimpleDateFormat("dd/MM/yyyy");
    String Ewaybillnum,Gstnum,apptchrgdate,scheduleTime,Article_status = "0",RequestIDinten;
    Integer BillType,WaybillType,MovementType,PartnerType;
    SessionManager session;
    DrawerLayout drawer;
    BottomNavigationView bottomNavigationView;
    String jsonStr;
    final Context context = this;
    DatabaseHelper databaseHelper;
    WayBillScanListAdapter adapter;
    ProgressBar progressBar;
    ImageView scanner;
    ListView listview;
    List<String> manualnumlist = new ArrayList<>();
    List<String> displayVehList = new ArrayList<>();
    TextView customername;
    CheckBox Cod,appoinment,floor;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    int year;
    int month;
    int dayOfMonth, mMinute, mHour,Mannualnum;
    Calendar calendar;
    Spinner waybilltype,billtype ,manualnum,movmenttype,partnertype,weighttype,Specialratetype,barcodeType;
    LinearLayout btnlayout;
    RelativeLayout manual_numlay;
    Button freezebtn,resetbtn,submitbtn,createwaybill;
    EditText ewaybillnum,nosfloor;
    TextView apptdate,appttime;
    String []BillTypeoption = {"Choose Bill Type","Manual","Online"};
    String []WaybillTypeoption = {"Choose WayBill Type","Credit","Paid","ToPay","FOC"};
    String []MovementTypeoption = {"Choose Movement","Forward","Return"};
    String []PartnerTypeoption = {"Choose Partner type","B2B","B2C"};
    JSONArray jsonArray = new JSONArray();
    ArrayList RetainDatalist = new ArrayList();
    ArrayList TempWaybillscanlist = new ArrayList();
    ArrayList<WayBillScanListTo> wayBillScanListDisplayed = new ArrayList();
    int Weight_type = 0 ;
    int Special_ratetype =0 ;
    int barcode_type =0 ;
    boolean CodorDod ,Floor,Apptcharge;
    ArrayList<WayBillScanListTo> WayBillscanRemovedList = new ArrayList<>();
    ArrayList<WayBillScanListTo> WayBillGetList = new ArrayList<>();
    List<String> weighttypelist = new ArrayList<>();
    List<String> specialratetypelist = new ArrayList<>();
    List<String> barcodeTypelist = new ArrayList<>();
    List<String> waybillScanlist = new ArrayList<>();
    TextInputLayout  ewaybillnumlay;
    JSONArray Ewaybillinsertlist = new JSONArray();
    ArrayList<WayBillScanListTo> WayBillscanList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.way_bill_sacn_activity);


        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        Username = user.get(SessionManager.KEY_NAME);
        userId = user.get(SessionManager.KEY_USERID);
        DesingId = user.get(SessionManager.KEY_DESIG);
        branchId = user.get(SessionManager.KEY_BRANCH);


        listview = findViewById(R.id.listview);
        manual_numlay = findViewById(R.id.manual_numlay);
        scanner = findViewById(R.id.scanner);

        customername = findViewById(R.id.customername);

        progressBar = findViewById(R.id.progressBar);


        createwaybill = findViewById(R.id.createwaybill);

        manualnumlist.add(0,"Select Manual Number");
        btnlayout = findViewById(R.id.btnlayout);
        submitbtn = findViewById(R.id.submitbtn);
        ewaybillnum = findViewById(R.id.ewaybillnum);
        ewaybillnumlay = findViewById(R.id.ewaybillnumlay);
        weighttype = findViewById(R.id.weighttype);
        Specialratetype = findViewById(R.id.speciratetype);
        barcodeType = findViewById(R.id.barcodeType);
//        statusTV = (TextView) findViewById(R.id.errormsg);

        ewaybillnum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (ewaybillnum.getText().length() != 0 && !ewaybillnum.getText().toString().equals("")) {
                        Ewaybillnum = ewaybillnum.getText().toString();

                        Gstnum = Ewaybillnum;
//                    statusTV.setText("");
                        if(!(Temparraylist.contains(Ewaybillnum))) {
                            Temparraylist.add(Ewaybillnum);
                            getWaybillList();
                        }else{
                            Toast.makeText(getApplicationContext(),"E-waybilno Exist",Toast.LENGTH_LONG).show();
                        }
                        ewaybillnumlay.setError(null);
                        ewaybillnumlay.setErrorEnabled(false);
                    }
                    else {
                        ewaybillnumlay.setErrorEnabled(true);
                        ewaybillnumlay.setError("error");
//                    statusTV.setText("Please Enter E-WayBill No");
                    }
                    return true;
                }
                return false;
            }
        });


        Intent intenta = getIntent();
        ArrayList RetainDatintentalist = intenta.getCharSequenceArrayListExtra("RetainDatalist");
        Bundle bundle = intenta.getExtras();
        wayBillScanListDisplayed = (ArrayList<WayBillScanListTo>)bundle.getSerializable("wayBillScanListDisplayed");

        String Customernameintenta = intenta.getStringExtra("Customernameintenta");
        String Ewaybillnumintenta = intenta.getStringExtra("Ewaybillnumintenta");
        RequestIDinten = intenta.getStringExtra("RequstID");
        String CustIDinten = intenta.getStringExtra("CustID");
        String Actualweight = intenta.getStringExtra("Actutal_Weight");
        WaybillIdintent = intenta.getStringExtra("waybillId");
        String Article_statusintena = intenta.getStringExtra("Articles_Status");
        consignorName = intenta.getStringExtra("consignorName");
        consigneeName = intenta.getStringExtra("consigneeName");
        InvoiceNum = intenta.getStringExtra("InvoiceNum");
        ewaybill_date = intenta.getStringExtra("ewabilldate");
        ewaybill_expiry_date = intenta.getStringExtra("ewabilldate");
        submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject articles = new JSONObject();


                try {
                    articles.put("ewaybill_no", Ewaybillnum);
                    articles.put("invoice_no", InvoiceNum);
                    articles.put("ewaybill_date", formatdate2(ewaybill_date));
                    articles.put("waybilltype",RetainDatintentalist.get(2));
                    articles.put("customername",Customername);
                    articles.put("Cust_id",CustIDinten);
                    articles.put("Consignor_Name",consignorName);
                    articles.put("ConsigneeName",consigneeName);
                    articles.put("Actualweight",Actualweight);
                    articles.put("ewaybill_expiry_date",formatdate2( ewaybill_expiry_date));
                    articles.put("movmenttype",RetainDatintentalist.get(1));
                    articles.put("weighttype",RetainDatintentalist.get(4));
                    articles.put("RequestID",RequestIDinten);
                    if((Boolean) RetainDatintentalist.get(8)) {
                        articles.put("appointment_applicable", "1");

                        String time = (String) RetainDatintentalist.get(11).toString();

                        String[] time1 = time.split(":");

                        if(time1[0].length() == 1){
                            time1[0]="0"+time1[0];
                            String tome[] =  time1[1].split(" ");
                            time1[1]=tome[0]+":00 ";
                            time = time1[0]+":"+ time1[1];
                            System.out.println("Time==>" + time);
                        }else{
                            String tome[] =  time1[1].split(" ");
                            time1[1]=tome[0]+":00 ";
                            time = time1[0]+":"+ time1[1];
                        }
                String fomrateddateandtime = formatdate(((String) RetainDatintentalist.get(10).toString() ))+" "+time;
                        articles.put("appointment_date_time", fomrateddateandtime);

                    }else{
                        articles.put("appoinment", "0");
                        articles.put("appointment_date_time",null);
                    }
                    if((Boolean) RetainDatintentalist.get(7)) {
                        articles.put("floor_applicable", "1");
                        articles.put("no_of_floors",(String) RetainDatintentalist.get(9).toString() );

                    }else{
                        articles.put("floor_applicable", "0");
                        articles.put("no_of_floors",null);
                    }
                    if((Boolean) RetainDatintentalist.get(6)){
                    articles.put("coddod_applicable",1);
                    }else{
                        articles.put("coddod_applicable",0);
                    }
                    articles.put("Specialratetype",RetainDatintentalist.get(5));
                    articles.put("barcodeType",RetainDatintentalist.get(12));

                    if ((Integer) RetainDatintentalist.get(0) == 0 && RetainDatintentalist.size() > 13) {

                        articles.put("mannualnum", RetainDatintentalist.get(13));
                    }else{
                        articles.put("mannualnum", "");
                    }
                    Ewaybillinsertlist.put(articles);

                    Ewaybillinsert();
                    System.out.println("EwaybillList ==> "+ articles.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        if(Article_statusintena != null){
            Article_status = Article_statusintena;
        }


        if (Customernameintenta != null) {
            Customername = Customernameintenta;
            customername.setText(Customernameintenta);
        }
        if(wayBillScanListDisplayed != null) {

            System.out.println("wayBillScanListDisplayed Size ==> " + wayBillScanListDisplayed.size());
        if(wayBillScanListDisplayed.size() > 0 ) {
            GetArticleStatus();
            WayBillscanList = wayBillScanListDisplayed;
            Ewaybillnum = Ewaybillnumintenta;
            listview.setVisibility(View.VISIBLE);
            if (Article_status.equals("1")) {
                System.out.println("Article_Status For btn==>"  + Article_status);
                btnlayout.setVisibility(View.VISIBLE);
            } else {
                btnlayout.setVisibility(View.GONE);
            }
            adapter = new WayBillScanListAdapter(WayBillScanActivity.this, wayBillScanListDisplayed,Ewaybillnum,Customername,Contractfactor, (Integer) RetainDatintentalist.get(2),Article_status,RetainDatalist,CustIDinten,RequestIDinten);
            listview.setAdapter(adapter);
        }
        }




        System.out.println( "Intened RetainDatalist === > "  + RetainDatintentalist);
        weighttypelist.add(0,"select Weight Type");
        weighttypelist.add(1,"Input Type");
        weighttypelist.add(2,"CFT type");
        ArrayAdapter WeightType = new ArrayAdapter<String>(this,R.layout.dropdown_list_view,R.id.list_item,weighttypelist);

        weighttype.setAdapter(WeightType);

        weighttype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                Weight_type = position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
//                        tvp_dealer_edit.setText("");
            }
        });

        weighttype.setSelection(1);

        specialratetypelist.add(0,"select Special Rate");
        specialratetypelist.add(1,"General Type");
        specialratetypelist.add(2,"Special type");
        ArrayAdapter Specialratetypeadp = new ArrayAdapter<String>(this,R.layout.dropdown_list_view,R.id.list_item,specialratetypelist);

        Specialratetype.setAdapter(Specialratetypeadp);

        Specialratetype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                Special_ratetype = position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//                 TODO Auto-generated method stub
//                        tvp_dealer_edit.setText("");
            }
        });

        Specialratetype.setSelection(1);


        barcodeTypelist.add(0,"online");
        barcodeTypelist.add(1,"Manual");
        ArrayAdapter barcodeTypeadp = new ArrayAdapter<String>(this,R.layout.dropdown_list_view,R.id.list_item,barcodeTypelist);

        barcodeType.setAdapter(barcodeTypeadp);

        barcodeType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                barcode_type = position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//                 TODO Auto-generated method stub
//                        tvp_dealer_edit.setText("");
            }
        });

        barcodeType.setSelection(0);
        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WayBillScanActivity.this,WayBillQrScanner.class);
                startActivity(intent);
            }
        });
       Intent intent = getIntent();

      String Ewaybillnumintent = intent.getStringExtra("Waybillnum");
      String Customernameinten = intent.getStringExtra("customername");
                       CustID = intent.getStringExtra("customerId") ;
                    RequestID = intent.getStringExtra("RequestId") ;
        Contractfactor = intent.getStringExtra("Contractfactor");

          if (Customernameinten != null && Customernameintenta == null) {
              Customername = Customernameinten;
              customername.setText(Customernameinten);
          }
          if(Ewaybillnumintent != null ) {
              Ewaybillnum = Ewaybillnumintent;
              createWayBill();
          }

                ewaybillnum.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    System.out.println("iam here" + ewaybillnum.getText().length());
                    if (ewaybillnum.getText().length() == 0 && ewaybillnum.getText().toString().equals("")) {
                        System.out.println("iam here 1");
                        ewaybillnumlay.setErrorEnabled(true);
                        ewaybillnumlay.setError("error");


                    } else {
                        ewaybillnumlay.setError(null);
                        ewaybillnumlay.setErrorEnabled(false);
                    }

//                    saveThisItem(txtClientID.getText().toString(), "name", txtName.getText().toString());
                }
            }
        });
        createwaybill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ewaybillnum.getText().length() != 0 && !ewaybillnum.getText().toString().equals("")) {
                    Ewaybillnum = ewaybillnum.getText().toString();

                    Gstnum = Ewaybillnum;
//                    statusTV.setText("");


                    if(!(Temparraylist.contains(Ewaybillnum))) {
                        Temparraylist.add(Ewaybillnum);
                        getWaybillList();
                        System.out.println("Temparraylist===>"+ Temparraylist);
                    }else{
                        Toast.makeText(getApplicationContext(),"E-waybilno Exist",Toast.LENGTH_LONG).show();
                    }
                    ewaybillnumlay.setError(null);
                    ewaybillnumlay.setErrorEnabled(false);
                }
                else {
                    ewaybillnumlay.setErrorEnabled(true);
                    ewaybillnumlay.setError("error");
//                    statusTV.setText("Please Enter E-WayBill No");
                }

            }
        });

    //Billtype DropDown Code Here//
        billtype = findViewById(R.id.billtype);
        movmenttype = findViewById(R.id.movmenttype);
        waybilltype = findViewById(R.id.waybilltype);
        partnertype = findViewById(R.id.partnertype);
        Cod = findViewById(R.id.Cod);
        appoinment = findViewById(R.id.appoinment);
        apptdate = findViewById(R.id.apptdate);
        appttime = findViewById(R.id.appttime);
        floor = findViewById(R.id.floor);
        nosfloor = findViewById(R.id.floornos);


        if(RetainDatintentalist != null ){
            if(RetainDatintentalist.size() > 0) {
                billtype.setSelection((Integer) RetainDatintentalist.get(0));
                movmenttype.setSelection((Integer) RetainDatintentalist.get(1));
                waybilltype.setSelection((Integer) RetainDatintentalist.get(2));
                partnertype.setSelection((Integer) RetainDatintentalist.get(3));
                weighttype.setSelection((Integer) RetainDatintentalist.get(4));
                Specialratetype.setSelection((Integer) RetainDatintentalist.get(5));
                barcodeType.setSelection((Integer) RetainDatintentalist.get(12));
                Cod.setChecked((Boolean) RetainDatintentalist.get(6));
                floor.setChecked((Boolean) RetainDatintentalist.get(7));
                appoinment.setChecked((Boolean) RetainDatintentalist.get(8));
                nosfloor.setText((String) RetainDatintentalist.get(9));
                apptdate.setText((String) RetainDatintentalist.get(10).toString());
                appttime.setText((String) RetainDatintentalist.get(11).toString());

                if ((Integer) RetainDatintentalist.get(0) == 0 && RetainDatintentalist.size() == 13) {

                    manualnum.setSelection((Integer) RetainDatintentalist.get(13));
                    manual_numlay.setVisibility(View.VISIBLE);
                }else{
                    manual_numlay.setVisibility(View.GONE);
                }


                Datetimeenable((Boolean) RetainDatintentalist.get(7));
                Floorenable((Boolean) RetainDatintentalist.get(6));
            }
        }
        Cod.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              CodorDod = ((CheckBox) v).isChecked();
                                          }
                                      });

        appoinment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Apptcharge = ((CheckBox) v).isChecked();
                // Check which checkbox was clicked
                Datetimeenable(Apptcharge);

            }
        });

        floor.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              nosfloor.setText("");
                                              Floor = ((CheckBox) v).isChecked();
                                              // Check which checkbox was clicked
                                             Floorenable(Floor);
                                          }
                                      }
        );

        apptdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                apptdate.setText("");
                try {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    // TODO: handle exception
                }
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                    datePickerDialog = new DatePickerDialog(WayBillScanActivity.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                    apptchrgdate = day + "-" + (month + 1) + "-" + year;
                                    apptdate.setText(day + "-" + (month + 1) + "-" + year);
                                }
                            }, year, month, dayOfMonth);
                    datePickerDialog.show();

            }
        }
        );

        appttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Keyboard hide code*/
                appttime.setText("");

                try {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    // TODO: handle exception
                }
                calendar = Calendar.getInstance();
                mHour = calendar.get(Calendar.HOUR_OF_DAY);
                mMinute = calendar.get(Calendar.MINUTE);



                timePickerDialog = new TimePickerDialog(WayBillScanActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

//                                String tmpminitue,am_pm;
//
//                                if(String.valueOf(minute).length() < 2){
//                                    tmpminitue = "0" + String.valueOf(minute);
//                                }else {
//                                    tmpminitue = String.valueOf(minute);
//                                }
//                                if(hourOfDay > 12) {
//                                    am_pm = " PM";
//                                    hourOfDay = hourOfDay - 12;
//                                }
//                                else
//                                {
//                                    am_pm=" AM";
//                                }
//                                scheduleTime = hourOfDay + ":" + tmpminitue + am_pm;
                                scheduleTime = hourOfDay + ":" + minute;
                                appttime.setText(scheduleTime);
                                Toast.makeText(getApplicationContext(),scheduleTime,Toast.LENGTH_SHORT);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });




        ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.dropdown_list_view,R.id.list_item,BillTypeoption);
        billtype.setAdapter(arrayAdapter);
        ArrayAdapter arrayAdapter1 = new ArrayAdapter(this,R.layout.dropdown_list_view,R.id.list_item,WaybillTypeoption);
        waybilltype.setAdapter(arrayAdapter1);
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(this,R.layout.dropdown_list_view,R.id.list_item,MovementTypeoption);
        movmenttype.setAdapter(arrayAdapter2);
        ArrayAdapter arrayAdapter3 = new ArrayAdapter(this,R.layout.dropdown_list_view,R.id.list_item,PartnerTypeoption);
        partnertype.setAdapter(arrayAdapter3);

        waybilltype.setSelection(1);
        billtype.setSelection(2);
        movmenttype.setSelection(1);
        partnertype.setSelection(1);

        movmenttype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                MovementType = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                PartnerType = 1;
            }
        });
        partnertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                PartnerType = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                PartnerType = 1;
            }
        });
        waybilltype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                WaybillType = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                WaybillType = 1;
            }
        });

        billtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String Option = adapterView.getItemAtPosition(i).toString();
                BillType = i;
                manual_numlay.setVisibility(View.VISIBLE);
                getManualno(Option);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                manualnum.setSelection(0);
            }

        });
        RetainDatalist.add(1);
        RetainDatalist.add(1);
        RetainDatalist.add(1);
        RetainDatalist.add(1);
        RetainDatalist.add(1);
        RetainDatalist.add(2);
        RetainDatalist.add(false);
        RetainDatalist.add(false);
        RetainDatalist.add(false);
        RetainDatalist.add("");
        RetainDatalist.add("");
        RetainDatalist.add("");
        RetainDatalist.add(0);
        RetainDatalist.add(0);



    }
    public void closeDrawer(DrawerLayout.DrawerListener listener) {
        drawer.setDrawerListener(listener);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void openDrawer() {
        drawer.setDrawerListener(null);
        drawer.openDrawer(GravityCompat.START);
    }
    public  void Floorenable (boolean floorsta){
        if (floorsta) {
            nosfloor.setVisibility(View.VISIBLE);
        }else{
            nosfloor.setVisibility(View.GONE);
        }
    }
    public void Datetimeenable(boolean Apptcharge1) {
        if (Apptcharge1) {
            apptdate.setVisibility(View.VISIBLE);
            appttime.setVisibility(View.VISIBLE);
            apptdate.setInputType(0);
            appttime.setInputType(0);
        } else {
            // Do your coding
            apptdate.setVisibility(View.GONE);
            appttime.setVisibility(View.GONE);
        }
    }

public  void getManualno(String Option){
    if(Option.equals("Online")){
        manual_numlay.setVisibility(View.GONE);
    }
    else if(Option.equals("Manual")){
        manual_numlay.setVisibility(View.VISIBLE);

        getMannualNumber();

    }
}
    public String formatdate(String Date) {

        String s1 = Date;
        String s2 = null;
        java.util.Date d;
        try {
            d = Formatabledate.parse(s1);
            Date = (new SimpleDateFormat("yyyy-MM-dd")).format(d);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Date;
    }

    public String formatdate2(String Date) {

        String s1 = Date;
        String s2 = null;
        java.util.Date d;
        try {
            d = f.parse(s1);
            Date = (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")).format(d);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Date;
    }
public  void createWayBill(){
        RetainDatalist.clear();
//        System.out.println("Ewaybillnum ==========>"+Ewaybillnum);
        if (Ewaybillnum != null && Ewaybillnum !="" && Ewaybillnum.length() >0) {
            manual_numlay.setVisibility(View.VISIBLE);
            manual_numlay.setVisibility(View.VISIBLE);
            ewaybillnum.setText(Ewaybillnum);
            Gstnum = Ewaybillnum;
//            statusTV.setText("");


                getWaybillList();
        }
        else {
//            statusTV.setText("Please Enter E-Waybill Number"); [0, 0, 0, 1, 1, true, true, true, 25, 9/12/2021, 3:09 PM]
        }
    }
    public void getWaybillList() {
            if (Article_status.equals("1")) {
                System.out.println("Article_Status For btn==>"  + Article_status);
                btnlayout.setVisibility(View.VISIBLE);
            } else {
                btnlayout.setVisibility(View.GONE);
            }
        progressBar.setVisibility(View.VISIBLE);
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }

        String obj = null;

        new BackgroundTask(this) {
            private Object WayBillScanListAdapter;

            @Override
            public void doInBackground() {
                //put you background code
                //same like doingBackground
                //Background Thread


                jsonStr = webService.callapi(Ewaybillnum,Gstnum);
                System.out.println("json_string_output scanlist====" + jsonStr);

            }

            @Override
            public void onPostExecute() {

//                loaderDialog("1");

                if (jsonStr != null) {
                    try {
                        jsonArray = new JSONArray(jsonStr);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int n = 0; n < jsonArray.length(); n++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(n);
                                WayBillScanListTo waybilllist = new WayBillScanListTo(
                                        jsonObject.getString("itemSize"),
                                        jsonObject.getString("invoiceNo"),
                                        jsonObject.getString("invoiceDate"),
                                        jsonObject.getString("invoiceValue"),
                                        jsonObject.getString("ewayBillDate"),
                                        jsonObject.getString("ewayBillExpiry"),
                                        jsonObject.getString("ewayBillUpdatedDate"),
                                        jsonObject.getString("consignorGst"),
                                        jsonObject.getString("consignorName"),
                                        jsonObject.getString("consignorAddress"),
                                        jsonObject.getString("consignorPlace"),
                                        jsonObject.getString("consignorPincode"),
                                        jsonObject.getString("consigneeGst"),
                                        jsonObject.getString("consigneeName"),
                                        jsonObject.getString("consigneeAddress"),
                                        jsonObject.getString("consigneePlace"),
                                        jsonObject.getString("consigneePincode"),
                                        jsonObject.getString("ewayBillId"),
                                        jsonObject.getString("vehicleNo"),
                                        jsonObject.getString("ewayBillNo")

                                );
                                WayBillscanList.add(waybilllist);
                                RetainDatalist.clear();
                                RetainDatalist.add(WaybillType);
                                RetainDatalist.add(MovementType);
                                RetainDatalist.add(PartnerType);
                                RetainDatalist.add(BillType);
                                RetainDatalist.add(Weight_type);
                                RetainDatalist.add(Special_ratetype);
                                RetainDatalist.add(CodorDod);
                                RetainDatalist.add(Floor);
                                RetainDatalist.add(Apptcharge);
                                RetainDatalist.add(nosfloor.getText().toString());
                                RetainDatalist.add(apptdate.getText());
                                RetainDatalist.add(appttime.getText());
                                RetainDatalist.add(barcode_type);

                                System.out.println("Retiaindatalist === > " + RetainDatalist);

                            }
                        }else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Snackbar.make(scanner,"E-WayBill is doesn't Exists" , Snackbar.LENGTH_LONG).show();
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                        }
//                        pDialog.hide();
                    } catch (final JSONException e) {
                        System.out.println("JSONException=========" + e);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                Snackbar.make(scanner,"Invalid Data" , Snackbar.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);
//                                loaderDialog("0");
                            }
                        });
                    }

                    try {
                        if (WayBillscanList.size() > 0) {
                            listview.setVisibility(View.VISIBLE);
                            adapter = new WayBillScanListAdapter(WayBillScanActivity.this, WayBillscanList,Ewaybillnum,Customername,Contractfactor,WaybillType,Article_status,RetainDatalist,CustID,RequestID);
                            listview.setAdapter(adapter);
                            progressBar.setVisibility(View.GONE);
                        }
                        else{
                            progressBar.setVisibility(View.GONE);
                            Snackbar.make(listview,"Waybill not exists" , Snackbar.LENGTH_LONG).show();

                        }
                    } catch (Exception e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            loaderDialog("0");
                            progressBar.setVisibility(View.GONE);
                            Snackbar.make(listview,"Could't get data from the server" , Snackbar.LENGTH_LONG).show();
                        }
                    });

                }
            }
        }.execute();
    }


    public void getMannualNumber() {

//        pDialog = new ProgressDialog(SubmitOrder.this);
//        pDialog.setMessage("Connecting Server... \n Please Wait!!!");
//        pDialog.setCancelable(false);
//        pDialog.show();

        String obj = null;

        new BackgroundTask(this) {
            @Override
            public void doInBackground() {
                //put you background code
                //same like doingBackground
                //Background Thread
                jsonStr = webService.getManualnolist(branchId);
//                System.out.println("json_string_output====" + jsonStr);

                if (jsonStr != null) {
                    try {
                        JSONArray array = new JSONArray(jsonStr);
                        if (array.length() > 0) {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jsonObj = array.getJSONObject(i);
                                System.out.println(jsonObj);
                                manualnumlist.add(jsonObj.getString("PreNo"));
                            }

                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Manual number not Available", Toast.LENGTH_LONG).show();
                                }
                            });

                        }

                    } catch (final JSONException e) {
                        System.out.println("JSONException=========" + e);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Invalid Data", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Could't get data from the server", Toast.LENGTH_LONG).show();
                        }
                    });

                }


            }

            @Override
            public void onPostExecute() {
//                if (pDialog.isShowing()) {
//                    pDialog.dismiss();
//                }
                manualnum = findViewById(R.id.manualnum);
                displayVehList = manualnumlist;

                manualnum.setSelection(0);

                ArrayAdapter manualadapter = new ArrayAdapter<String>(WayBillScanActivity.this,R.layout.dropdown_list_view,R.id.list_item,displayVehList);
                manualnum.setAdapter(manualadapter);
                manualnum.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                        if (!displayVehList.get(position).equals("0") && !displayVehList.get(position).equals("Select Manual Number")) {
                            Mannualnum = Integer.parseInt(displayVehList.get(position));
                            RetainDatalist.add(13,Mannualnum);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
//                        tvp_dealer_edit.setText("");
                    }
                });

            }
        }.execute();
    }
    public void GetArticleStatus() {

        pDialog = new ProgressDialog(WayBillScanActivity.this);
        pDialog.setMessage("Connecting Server... \n Please Wait!!!");
        pDialog.setCancelable(false);
        pDialog.show();


        new BackgroundTask(this) {
            @Override
            public void doInBackground() {
                //put you background code
                //same like doingBackground
                //Background Thread
                String jsonStr = webService.GetArticleStatus(Ewaybillnum);
                System.out.println("json_string_output====" + jsonStr);
                if(jsonStr != null){
                try {
                    JSONArray jsonArray = new JSONArray(jsonStr);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        for (int n = 0; n < jsonArray.length(); n++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(n);
                            Article_status = jsonObject.getString("Article_Staus");
                        }
                        System.out.println("Article Status from Query ==> " + Article_status);
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pDialog.hide();
                                }
                            });

                        }

                    } catch (final JSONException e) {
                        System.out.println("JSONException=========" + e);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pDialog.hide();

                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialog.hide();
                        }
                    });

                }


            }

            @Override
            public void onPostExecute() {
                pDialog.hide();

            }
        }.execute();
    }

    public void Ewaybillinsert() {
                    pDialog = new ProgressDialog(WayBillScanActivity.this);
                    pDialog.setMessage("Connecting Server... \n Please Wait!!!");
                    pDialog.setCancelable(false);
                    pDialog.show();

        String obj = Ewaybillinsertlist.toString();

        new BackgroundTask(this) {
            @Override
            public void doInBackground() {
                //put you background code
                //same like doingBackground
                //Background Thread
                String jsonStr = webService.Ewaybillinsert(obj,WaybillIdintent ,userId);
                System.out.println("json_string_output====" + jsonStr);

                if (jsonStr != null) {
                    try {
                        JSONArray array = new JSONArray(jsonStr);
                        if (array.length() > 0) {
                            JSONArray resultArray = new JSONArray();
                            JSONObject jsonObj = array.getJSONObject(0);
                            String exceptionMsg = jsonObj.getString("Code");
                            if ("1".equalsIgnoreCase(exceptionMsg)) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        Toast.makeText(getApplicationContext(), "WayBill Submited Successfully", Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(WayBillScanActivity.this, DashBoard.class);
                                        startActivity(intent);
                                        btnlayout.setVisibility(View.GONE);
                                        Article_status = "2";
                                    }
                                });




                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }


                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "E-Waybill insert Server errror", Toast.LENGTH_LONG).show();
                                }
                            });

                        }

                    } catch (final JSONException e) {
                        System.out.println("JSONException=========" + e);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Could't connect to the server", Toast.LENGTH_LONG).show();
                        }
                    });

                }


            }

            @Override
            public void onPostExecute() {
                            if (pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
            }
        }.execute();
    }
    @Override
    public void onBackPressed() {

        if(Article_status.equals("1")){

            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.exit_alert_layout);
            dialog.setCancelable(false);
            Button yesButton = (Button) dialog.findViewById(R.id.yes);
            yesButton.setVisibility(View.GONE);
            Button noButton = (Button) dialog.findViewById(R.id.no);
            TextView title = (TextView) dialog.findViewById(R.id.title);
            TextView message = (TextView) dialog.findViewById(R.id.message);
            title.setText("Warning !");
            noButton.setText("close");
            message.setText("Without Submit the Waybills, can't Back !");

            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }else{
            Intent intent = new Intent(this, DashBoard.class);
            startActivity(intent);
        }


    }

}