package com.application.dexter_new;

public class WaybillListTO {
    String waybillId = null;
    String waybillNum = null;
    String customerName = null;
    String consignorName = null;
    String consignorAddress = null;
    String consignorphone = null;
    String consigneeName = null;
    String consigneeAddress = null;
    String consigneephone = null;
    String noofArticles = null;
    String totalWeight = null;

    public WaybillListTO(String waybillId, String waybillNum, String customerName, String consignorName, String consignorAddress, String consignorphone, String consigneeName, String consigneeAddress, String consigneephone, String noofArticles, String totalWeight) {
        this.waybillId = waybillId;
        this.waybillNum = waybillNum;
        this.customerName = customerName;
        this.consignorName = consignorName;
        this.consignorAddress = consignorAddress;
        this.consignorphone = consignorphone;
        this.consigneeName = consigneeName;
        this.consigneeAddress = consigneeAddress;
        this.consigneephone = consigneephone;
        this.noofArticles = noofArticles;
        this.totalWeight = totalWeight;
    }

    public String getWaybillId() {
        return waybillId;
    }

    public void setWaybillId(String waybillId) {
        this.waybillId = waybillId;
    }

    public String getWaybillNum() {
        return waybillNum;
    }

    public void setWaybillNum(String waybillNum) {
        this.waybillNum = waybillNum;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsignorphone() {
        return consignorphone;
    }

    public void setConsignorphone(String consignorphone) {
        this.consignorphone = consignorphone;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getConsigneephone() {
        return consigneephone;
    }

    public void setConsigneephone(String consigneephone) {
        this.consigneephone = consigneephone;
    }

    public String getNoofArticles() {
        return noofArticles;
    }

    public void setNoofArticles(String noofArticles) {
        this.noofArticles = noofArticles;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }
}
