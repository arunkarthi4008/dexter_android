package com.application.dexter_new;

public class InterimTripListTO {
    String point_id = null;
    String point_name = null;
    String in_date = null;
    String in_time = null;
    String out_date = null;
    String out_time = null;
    String file_name = null;
    String completed_flag = null;
    String route_order = null;


    public InterimTripListTO(String point_id, String point_name, String in_date, String in_time, String out_date, String out_time, String file_name, String completed_flag, String route_order) {
        this.point_id = point_id;
        this.point_name = point_name;
        this.in_date = in_date;
        this.in_time = in_time;
        this.out_date = out_date;
        this.out_time = out_time;
        this.file_name = file_name;
        this.completed_flag = completed_flag;
        this.route_order = route_order;
    }


    public String getPoint_id() {
        return point_id;
    }

    public void setPoint_id(String point_id) {
        this.point_id = point_id;
    }

    public String getPoint_name() {
        return point_name;
    }

    public void setPoint_name(String point_name) {
        this.point_name = point_name;
    }

    public String getIn_date() {
        return in_date;
    }

    public void setIn_date(String in_date) {
        this.in_date = in_date;
    }

    public String getIn_time() {
        return in_time;
    }

    public void setIn_time(String in_time) {
        this.in_time = in_time;
    }

    public String getOut_date() {
        return out_date;
    }

    public void setOut_date(String out_date) {
        this.out_date = out_date;
    }

    public String getOut_time() {
        return out_time;
    }

    public void setOut_time(String out_time) {
        this.out_time = out_time;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getCompleted_flag() {
        return completed_flag;
    }

    public void setCompleted_flag(String completed_flag) {
        this.completed_flag = completed_flag;
    }

    public String getRoute_order() {
        return route_order;
    }

    public void setRoute_order(String route_order) {
        this.route_order = route_order;
    }
}
